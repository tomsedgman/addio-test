
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">


            <div class="col-sm-6 offset-sm-3">

                <?= form_open_multipart('/client/update', 'class="form form-horizontal"') ?>

                <div id="" class="card">
                    <div class="title">
                        <h5>Edit client</h5>
                    </div>
                    <div class="body">

                        <?php if (isset($_REQUEST['saved'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="confirm small">
                                        The information has been saved
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>



                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $client['name'] ?>" id="name" type="text" name="name" class="form-control" required>
                                    <label for="name">Give your client a name</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $client['url'] ?>" id="url" type="text" name="url" class="form-control" required>
                                    <label for="url">Main website URL</label>
                                </div>
                            </div>
                        </div>






                    </div>
                </div>

                <div class="card">
                    <div class="title"><h5>Branding</h5></div>
                    <div class="body">
                        <div class="row">

                            <div class="col-sm-4">
                                <input class="hide" id="logo" name="logo_file" type="file"/>
                                <button id="logo_upload" type="button" class="btn btn-blue">Upload logo</button>
                            </div>
                            <div class="col-sm-8">
                                <img id="logo_preview" style="width: 200px; margin-bottom: 30px;" src="<?= $client['logo'] ?>"/>
                            </div>
                        </div>

                        <hr/>

                        <div class="row">

                            <div class="col-sm-6">
                                <p>Header color</p>
                                <input id="header_color_input" style="height: 50px; width: 50px;" type="color" value="<?= $client['header_color'] ?>" /><br/><br/>
                                <input id="header_color" name="header_color" style="width: 70%" class="form-control" value="<?= $client['header_color'] ?>"/>
                            </div>
                            <div class="col-sm-6">
                                <p>Sidebar colour</p>
                                <input id="side_color_input" style="height: 50px; width: 50px;" type="color" value="<?= $client['side_color'] ?>"/><br/><br/>
                                <input id="side_color" name="side_color" style="width: 70%" class="form-control" value="<?= $client['side_color'] ?>"/>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="form-group right">
                    <button type="button" onclick="window.location.href='/analytics'" class="btn btn-grey">Exit</button>
                    <button class="btn btn-green">Update</button>
                </div>

                <?= form_close(); ?>
            </div>

        </div>
    </div>
</div>



<script>
    $(document).ready(function() {

        $('#logo_upload').click(function() {
            $('#logo').click();
        })

        $("#logo").change(function() {

            readURL(this);
        });

        $('#header_color_input').change(function() {
            $('#header_color').val($(this).val());
        })

        $('#side_color_input').change(function() {
            $('#side_color').val($(this).val());
        })



    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logo_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    setTimeout(
        function()
        {
            $('#msgbox1').addClass('hide');
        }, 1500);


</script>
