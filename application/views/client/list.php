<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Addio</title>
</head>

<body>


<div class="split">

    <div class="col-sm-7 align-self-center split-left">

        <div class="splitContent">

            <div class="row">
                <div class="col-sm-10 offset-sm-1">

                    <img src="/uploads/addio-logo.png"/>

                    <h4>Choose a client</h4>

                    <table id="dtable" class="table table-hover">
                        <thead>
                        <th>Client</th>
                        </thead>
                        <tbody>
                        <?php foreach ($clients as $c) : ?>
                            <tr style="cursor: pointer;" onclick="window.location.href='/client/select/<?= $c['key'] ?>'">
                                <td><?= $c['name'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                    <div class="add_btn_icon" style="float: right;" onclick="window.location.href='/client/add'">
                        <span class="ion-plus"></span>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="split-right col-sm-5 d-none d-sm-block">



    </div>

</div>



<div class="floating_btn" onclick="window.location.href='/client/add'">
    <i class="ion ion-plus"/>
</div>



<script>
    $(document).ready( function () {
        $('#dtable').DataTable({
            "dom": 'Bfrtip',
            "pageLength": 10000,
            "paging": false,
        });

        $('#dtable_filter input').attr('placeholder', 'Search for a client');
    } );
</script>

</body>
</html>