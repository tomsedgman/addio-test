<div id="report_content">
    <div class="container-fluid">
        <div class="row" id="report_logo">
            <div class="col-sm-12">
                <img style="height: 90px;" src="<?= $logo ?>"/>
            </div>
        </div>

        <div class="row" id="report_title">
            <div class="col-sm-12 right">
                <h2>Lead Generation Report</h2>
                <h4><?= $site['name'] ?> - <?= $page['name'] ?></h4>
                <h6><?= date('d M Y', strtotime($start)) ?> - <?= date('d M Y', strtotime($end)) ?></h6>

            </div>
        </div>


        <div class="row" id="report_graph">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Visitors - <span class="green"><?= $total_visitors ?> people</span></h4>
                        <p>Number of people that have clicked an advert and have visited the landing page.</p>
                    </div>
                </div>

                <div class="" id="chart" style="margin-top: 15px; width: 800px; height: 309px"></div>
            </div>

        </div>

        <div class="row" id="report_conversions">

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Conversions - <span class="green"><?= $conversions ?> people (<?= round(($conversions / $total_visitors) * 100, 2) ?>%)</span></h4>
                        <p>Number of people that have completed the form, called or clicked to email.</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card" style="border-color: #555; padding: 18px 30px;">
                    <div class="report_stat">
                        <div class="report_title">
                            Completed forms
                        </div>
                        <div class="report_number">
                            <?= $formfills ?>
                        </div>
                        <div class="report_percentage">
                            <?= round(($formfills / $total_visitors) * 100, 2) ?>%
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card" style="border-color: #555; padding: 18px 30px;">
                    <div class="report_stat">
                        <div class="report_title">
                            Click to email
                        </div>
                        <div class="report_number">
                            <?= $clickemail ?>
                        </div>
                        <div class="report_percentage">
                            <?= round(($clickemail / $total_visitors) * 100, 2) ?>%
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card" style="border-color: #555; padding: 18px 30px;">
                    <div class="report_stat">
                        <div class="report_title">
                            Phone calls
                        </div>
                        <div class="report_number">
                            <?= $calls ?>
                        </div>
                        <div class="report_percentage">
                            <?= round(($calls / $total_visitors) * 100, 2) ?>%
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" id="report_summary">

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Cost per lead - <span class="green">£<?= round(($adspend / $conversions), 2) ?></span></h4>
                        <p>Based on £<?= $adspend ?> spent on advertising over the last <?= $days ?> days and there being <?= $conversions ?> leads generated.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        load_graph();

        $('#daterange').change(function() {

            window.location.href='/rankings/view/' + $('#daterange').val();

        })

    })


    function load_graph() {

        var d = <?= $graph ?>

        if (d.length === 0) {

            $('#chart').addClass('hide');
            $('.no_rankings_msg').removeClass('hide');


        } else {


            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);



            function drawChart() {
                var data = google.visualization.arrayToDataTable(d);

                var days = d.length;
                var labelcount = 1;
                if (days > 10) { labelcount = 4; }
                if (days > 40) { labelcount = 8; }

                var options = {
                    hAxis: {showTextEvery: labelcount, title: '',  textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                    vAxis: {minValue: 0, format: 0, viewWindow : { min: 0 }, textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                    title: '',
                    colors: ['#6491E6'],
                    areaOpacity: 0.8,
                    pointSize: 10,
                    lineWidth: 7,
                    theme: 'material',
                    legend: {position : 'none' },
                    width: '100%',
                    chartArea: {'width': '90%', 'height': '70%'},
                };

                var chart = new google.visualization.AreaChart(document.getElementById('chart'));

                chart.draw(data, options);




            }

        }




    }


    $(window).resize(function() {
        load_graph();
    });



</script>