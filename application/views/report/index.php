
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">

                <div class="card">
                    <div class="title"><h5>Manual report</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="confirm small">
                                        Your report has been submitted and will be emailed shortly
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?= form_open('report/submit_report'); ?>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" value="<?= $this->session->userdata('email') ?>" name="email" required class="form-control">
                                    <label for="email">Email address</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="start_date" value="<?= date('Y-m-d', strtotime('- 30 days')) ?>" name="start_date" type="date" required class="form-control">
                                    <label for="start_date">Report start date</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="end_date" value="<?= date('Y-m-d') ?>" name="end_date" type="date" required class="form-control">
                                    <label for="end_date">Report end date</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="budget" type="number" value="" name="budget" class="form-control" required>
                                    <label for="budget">Monthly budget</label>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-9">
                                <p id="sd_warning" class="warning small hide">

                                </p>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group right">
                                    <button id="submitbtn" class="btn btn-blue">Create report</button>
                                </div>
                            </div>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>



            </div>

            <div class="col-sm-6">

                <div class="card">
                    <div class="title"><h5>New Report Schedule</h5></div>
                    <div class="body">



                        <?= form_open('report/create_schedule'); ?>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" value="<?= $this->session->userdata('email') ?>" name="email" required class="form-control">
                                    <label for="email">Email address</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select id="frequency" name="frequency" class="form-control">
                                        <option>Monthly</option>
                                        <option>Weekly</option>
                                    </select>
                                    <label style="margin-top: -25px;" for="frequency">Frequency</label>
                                </div>
                            </div>
                        </div>

                        <div class="row hide" id="weekly">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="weekly" class="form-control">
                                        <option value="1">Monday</option>
                                        <option value="2">Tuesday</option>
                                        <option value="3">Wednesday</option>
                                        <option value="4">Thursday</option>
                                        <option value="5">Friday</option>
                                        <option value="6">Saturday</option>
                                        <option value="7">Sunday</option>
                                    </select>
                                    <label style="margin-top: -25px;" for="weekly">Day of week</label>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="monthly">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="monthly" class="form-control">
                                        <?php for ($i = 1; $i < 27; $i++) : ?>
                                            <option><?= $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <label style="margin-top: -25px;" for="monthly">Day of month</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="budget" type="number" value="" name="budget" class="form-control" required>
                                    <label for="budget">Monthly budget</label>
                                </div>
                            </div>
                        </div>

                        <input name="site_key" type="hidden" value="<?= $this->session->userdata('site_key') ?>"/>


                        <div class="row">
                            <div class="col-sm-9">
                                <p id="sd_warning" class="warning small hide">

                                </p>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group right">
                                    <button id="submitbtn" class="btn btn-blue">Create schedule</button>
                                </div>
                            </div>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>


                <div class="card">
                    <div class="title"><h5>Report Schedules</h5></div>
                    <div class="body">


                        <?php foreach ($crons as $c) : ?>

                            <div class="row">
                                <div class="col-sm-4">
                                    <?= $c['email'] ?>
                                </div>
                                <div class="col-sm-2">
                                    £<?= $c['budget'] ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $c['frequency'] ?>
                                </div>

                                <div class="col-sm-2">
                                    <?= $c['frequencyNo'] ?>
                                </div>

                                <div class="col-sm-2">
                                    <button onclick="window.location.href='/report/delete_schedule/<?= $c['key'] ?>'" class="btn btn-red">Delete schedule</button>
                                    <br/><br/>
                                </div>
                            </div>


                        <?php endforeach; ?>


                    </div>
                </div>



            </div>
        </div>
    </div>

</div>



<script>


    $(document).ready(function() {


        freq();

        $('#frequency').change(function() {
            freq()
        });



    })

    function freq() {

        if ($('#frequency').val() === "Weekly") {
            $('#weekly').removeClass('hide');
            $('#monthly').addClass('hide');
        }

        if ($('#frequency').val() === "Monthly") {
            $('#monthly').removeClass('hide');
            $('#weekly').addClass('hide');
        }

    }

    setTimeout(
        function()
        {
            $('#msgbox1').addClass('hide');
        }, 1500);

</script>
