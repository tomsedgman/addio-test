
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-9">
                                <h5>Visitors for page: <?= $page['name'] ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="" id="curve_chart_visitors" style="margin-top: 30px; width: 100%; height: 300px"></div>
                    </div>
                </div>

                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-9">
                                <h5>Completed forms</h5>
                            </div>
                            <div class="col-sm-3">

                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="" id="curve_chart_forms" style="margin-top: 30px; width: 100%; height: 300px"></div>
                    </div>
                </div>

                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-9">
                                <h5>Click to email</h5>
                            </div>
                            <div class="col-sm-3">

                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="" id="curve_chart_cemails" style="margin-top: 30px; width: 100%; height: 300px"></div>
                    </div>
                </div>

                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-9">
                                <h5>Calls</h5>
                            </div>
                            <div class="col-sm-3">

                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="" id="curve_chart_calls" style="margin-top: 30px; width: 100%; height: 300px"></div>
                    </div>
                </div>

                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-9">
                                <h5>Click to call</h5>
                            </div>
                            <div class="col-sm-3">

                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="" id="curve_chart_ccalls" style="margin-top: 30px; width: 100%; height: 300px"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="card">

                    <div class="body">
                        <div class="row">
                            <div class="col-sm-4">
                                <strong>Page name</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Visitors</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Conv</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Conv %</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>£ per Conv</strong>
                            </div>
                        </div>
                        <hr/>
                        <?php $p = $page_data  ?>
                            <?php if ($page['status'] == 'Deleted' && $p['total'] == 0) : else : ?>
                            <div class="pageline clickable" onclick="window.location.href='/analytics/viewpage/<?= $daterange ?>/<?= $page['key'] ?>'">
                                <div class="row" style="<?php if ($page['status'] == 'Deleted') { echo "color: #ccc;"; } ?>">
                                    <div class="col-sm-4">
                                        <?= $page['name'] ?> <?php if ($page['status'] == 'Deleted') { echo "(Deleted)"; } ?>
                                    </div>
                                    <div class="col-sm-2 right">
                                        <?= $p['total'] ?>
                                    </div>
                                    <div class="col-sm-2 right">
                                        <?= $p['forms'] + $p['calls'] + $p['ccalls'] + $p['cemails'] ?>
                                    </div>
                                    <div class="col-sm-2 right">
                                        <?php
                                        $pc = 0;
                                        $t = $p['forms'] + $p['calls'] + $p['ccalls'] + $p['cemails']; // add calls to this when tracking
                                        if ($t !== 0) {
                                            $pc = round(($t / $p['total']) * 100, 0);
                                        }
                                        ?>
                                        <?= $pc ?>%
                                    </div>
                                    <div class="col-sm-2 right">
                                        <?php if ($p['spend'] > 0 && ($p['forms'] + $p['calls'] + $p['ccalls'] + $p['cemails']) > 0) : ?>
                                            £<?= round(($p['spend'] / ($p['forms'] + $p['calls'] + $p['ccalls'] + $p['cemails'])), 2) ?>
                                        <?php else : ?>
                                            £0
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <hr/>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        load_graph_visitors();
        load_graph_forms();
        load_graph_calls();
        load_graph_ccalls();
        load_graph_cemails();

        $('#daterange').change(function() {

            window.location.href='/analytics/viewpage/' + $('#daterange').val() + '/<?= $page['key'] ?>';

        })

    })


    function load_graph_visitors() {

        var d = <?= $graph ?>

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var data = google.visualization.arrayToDataTable(d);

            var days = d.length;
            var labelcount = 1;
            if (days > 10) { labelcount = 4; }
            if (days > 40) { labelcount = 8; }

            var options = {
                hAxis: {showTextEvery: labelcount, title: '',  textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                vAxis: {minValue: 0, format: 0, viewWindow : { min: 0 }, textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                title: '',
                colors: ['#FF5252'],
                areaOpacity: 0.8,
                pointSize: 10,
                lineWidth: 7,
                theme: 'material',
                legend: {position : 'none' },
                width: '100%',
                chartArea: {'width': '90%', 'height': '80%'},
            };

            var chart = new google.visualization.AreaChart(document.getElementById('curve_chart_visitors'));

            chart.draw(data, options);

        }


    }

    function load_graph_forms() {

        var d = <?= $graph_form ?>

            google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var data = google.visualization.arrayToDataTable(d);

            var days = d.length;
            var labelcount = 1;
            if (days > 10) { labelcount = 4; }
            if (days > 40) { labelcount = 8; }

            var options = {
                hAxis: {showTextEvery: labelcount, title: '',  textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                vAxis: {minValue: 0, format: 0, viewWindow : { min: 0 }, textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                title: '',
                colors: ['#6491E6'],
                areaOpacity: 0.8,
                pointSize: 10,
                lineWidth: 7,
                theme: 'material',
                legend: {position : 'none' },
                width: '100%',
                chartArea: {'width': '90%', 'height': '80%'},
            };

            var chart = new google.visualization.AreaChart(document.getElementById('curve_chart_forms'));

            chart.draw(data, options);

        }


    }
    function load_graph_cemails() {

        var d = <?= $graph_cemail ?>

            google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var data = google.visualization.arrayToDataTable(d);

            var days = d.length;
            var labelcount = 1;
            if (days > 10) { labelcount = 4; }
            if (days > 40) { labelcount = 8; }

            var options = {
                hAxis: {showTextEvery: labelcount, title: '',  textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                vAxis: {minValue: 0, format: 0, viewWindow : { min: 0 }, textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                title: '',
                colors: ['#6491E6'],
                areaOpacity: 0.8,
                pointSize: 10,
                lineWidth: 7,
                theme: 'material',
                legend: {position : 'none' },
                width: '100%',
                chartArea: {'width': '90%', 'height': '80%'},
            };

            var chart = new google.visualization.AreaChart(document.getElementById('curve_chart_cemails'));

            chart.draw(data, options);

        }


    }

    function load_graph_calls() {

        var d = <?= $graph_call ?>

            google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var data = google.visualization.arrayToDataTable(d);

            var days = d.length;
            var labelcount = 1;
            if (days > 10) { labelcount = 4; }
            if (days > 40) { labelcount = 8; }

            var options = {
                hAxis: {showTextEvery: labelcount, title: '',  textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                vAxis: {minValue: 0, format: 0, viewWindow : { min: 0 }, textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                title: '',
                colors: ['#8CC36E'],
                areaOpacity: 0.8,
                pointSize: 10,
                lineWidth: 7,
                theme: 'material',
                legend: {position : 'none' },
                width: '100%',
                chartArea: {'width': '90%', 'height': '80%'},
            };

            var chart = new google.visualization.AreaChart(document.getElementById('curve_chart_calls'));

            chart.draw(data, options);

        }


    }

    function load_graph_ccalls() {

        var d = <?= $graph_ccall ?>

            google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var data = google.visualization.arrayToDataTable(d);

            var days = d.length;
            var labelcount = 1;
            if (days > 10) { labelcount = 4; }
            if (days > 40) { labelcount = 8; }

            var options = {
                hAxis: {showTextEvery: labelcount, title: '',  textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                vAxis: {minValue: 0, format: 0, viewWindow : { min: 0 }, textStyle: {color: '#333', fontName: 'Product Sans', fontSize: '12'}},
                title: '',
                colors: ['#8CC36E'],
                areaOpacity: 0.8,
                pointSize: 10,
                lineWidth: 7,
                theme: 'material',
                legend: {position : 'none' },
                width: '100%',
                chartArea: {'width': '90%', 'height': '80%'},
            };

            var chart = new google.visualization.AreaChart(document.getElementById('curve_chart_ccalls'));

            chart.draw(data, options);

        }


    }

</script>