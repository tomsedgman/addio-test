
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="title"><h5>Add user access for <?= $site['name'] ?></h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['hasaccess'])) : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="warning small">
                                        This email address already has access
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($_REQUEST['owner'])) : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="warning small">
                                        This email address is the owner of the site
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($_REQUEST['hasaccount'])) : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="warning small">
                                        This email address already has an account. Please enter a different email.
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?= form_open('settings/adduseraccess'); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" name="email" required class="form-control">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label style="margin-top: -25px;" for="access">Access</label>
                                    <select class="form-control" name="access">
                                        <option>User</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button onclick="window.location.href='/settings'" type="button" class="btn btn-grey">Back</button>
                            <button class="btn btn-blue">Add user</button>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#name').keyup(function() {

            url = $('#name').val().toLowerCase().split(' ').join('-');

            $('#url').val(url);
            $('#url2').val(url);

        })

    })
</script>
