
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">

                <div class="card">
                    <div class="title"><h5>Website details</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="confirm small">
                                        The information has been saved
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?= form_open('settings/update'); ?>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="name" value="<?= $site['name'] ?>" name="name" required class="form-control">
                                    <label for="name">Name</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input onkeyup="validSubdomain()" id="url" value="<?= str_replace('.addio.co.uk', '', $site['url']) ?>" name="url" required class="form-control">
                                    <label for="url" id="subdomain">Your subdomain</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="main_url" value="<?= $site['main_url'] ?>" name="main_url" required class="form-control">
                                    <label for="main_url">Main Website URL</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="privacy_policy" type="text" value="<?= $site['privacy_policy'] ?>" name="privacy_policy" class="form-control" required>
                                    <label for="privacy_policy">Privacy Policy</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="cookie_policy" type="text" value="<?= $site['cookie_policy'] ?>" name="cookie_policy" class="form-control" required>
                                    <label for="cookie_policy">Cookie Policy</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="address" type="text" value="<?= $site['address'] ?>" name="address" class="form-control" required>
                                    <label for="address">Address</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-9">
                                <p id="sd_warning" class="warning small hide">
                                    Invalid subdomain. Cannot use spaces or special characters
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group right">
                                    <button type="button" onclick="window.location.href='/analytics'" class="btn btn-grey">Exit Settings</button>
                                    <button id="submitbtn" class="btn btn-blue">Update</button>
                                </div>
                            </div>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>




            </div>

            <div class="col-sm-6">

                <?php if ($this->session->userdata('user_type') == "Admin") : ?>

                    <div class="card">
                        <div class="title"><h5>Client</h5></div>
                        <div class="body">

                            <?= form_open('settings/updateclient'); ?>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <select name="client_key" class="form-control">
                                            <?php foreach ($clients as $c) : ?>
                                                <option value="<?= $c['key'] ?>"><?= $c['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <label style="margin-top: -20px;" for="name">Client</label>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group right">
                                        <button id="submitbtn" class="btn btn-blue">Update</button>
                                    </div>
                                </div>
                            </div>

                            <?= form_close(); ?>
                        </div>
                    </div>

                <?php endif; ?>

                <div class="card">

                    <div class="title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Customer access</h5>
                            </div>
                            <div class="col-sm-6 right">
                                <button onclick="window.location.href='/settings/add_access'" class="btn btn-green">Add user</button>
                            </div>
                        </div>
                    </div>
                    <div class="body">

                        <hr/>
                        <?php foreach ($users as $u) : ?>
                            <div class="row">
                                <div class="col-sm-11">
                                    <?= $u['email'] ?>
                                </div>
                                <div class="col-sm-1 right">
                                    <i onclick="window.location.href='/settings/removeaccess/<?= $u['key'] ?>'" class="clickable red ion ion-close"></i>
                                </div>
                            </div>
                            <hr/>
                        <?php endforeach; ?>

                        <?php foreach ($invites as $i) : ?>
                            <div class="row" style="color: #ccc">
                                <div class="col-sm-8">
                                    Invited: <?= $i['email'] ?>
                                </div>
                                <div class="col-sm-3 right">
                                    <?= date('d M Y', strtotime($i['created'])) ?>
                                </div>
                                <div class="col-sm-1 right">
                                    <i onclick="window.location.href='/settings/removeinvitation/<?= $i['key'] ?>'" class="clickable red ion ion-close"></i>
                                </div>
                            </div>
                            <hr/>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($this->session->userdata('user_type') == "Admin") : ?>


        <div class="row">
            <div class="col-sm-12">
                <br/>
                <a style="font-weight: 300; text-decoration: underline;" class="red" href="#" onclick="delete_site()" id="delete_site_btn">Delete this site forever</a>

            </div>
        </div>

        <?php endif; ?>
    </div>

</div>


<!-- Modal -->
<div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-weight: 300">
                    Deleting this site will remove all data including groups, keywords and all previous ranking information. This action <strong>cannot</strong> be reversed.
                </p>
                <p style="font-weight: 300">
                    If you are sure then please type "<strong>please delete this site</strong>" in the box below to activate the delete button
                </p>

                <input class="form-control" id="delete_confirm_text" style="letter-spacing: 1px; border: 1px solid #ccc;" placeholder="Write the text above exactly as shown"/>

            </div>
            <?= form_open('site/delete/') ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button id="delete_btn" disabled class="btn btn-red">Delete site</button>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>


<script>

    function validSubdomain() {
        var re = /[^a-zA-Z0-9\-]/;
        var val = document.getElementById("url").value;
        if(re.test(val)) {
            $('#submitbtn').attr('disabled', 'disabled');
            $('#sd_warning').removeClass('hide');
        } else {
            $('#submitbtn').attr('disabled', false);
            $('#sd_warning').addClass('hide');
        }
    }

    $(document).ready(function() {


        $('#delete_confirm_text').keyup(function() {

            if ($(this).val() === "please delete this site") {

                $('#delete_btn').attr('disabled', false);

            } else {
                $('#delete_btn').attr('disabled', true);
            }

        })


        $('#subdomain').html('Your subdomain (' + $('#url').val() + '.addio.co.uk)');

        $('#url').keyup(function() {

            var str = this.value;
            str = str.replace(/\s+/g, '-').toLowerCase();
            str = str.replace(/\.+/g, '-').toLowerCase();
            this.value = str;

            $('#subdomain').html('Your subdomain (' + $('#url').val() + '.addio.co.uk)');
        })
    })

    function delete_site() {


        $('#myModalDelete').modal('toggle');


    }

    setTimeout(
        function()
        {
            $('#msgbox1').addClass('hide');
        }, 1500);



</script>
