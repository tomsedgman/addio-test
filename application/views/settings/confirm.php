<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Boilerplate App</title>
</head>

<body>

<div id="content" class="wide">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 offset-sm-4">
                <div id="login" class="card">
                    <?php if (isset($_REQUEST['err'])) : ?>
                        <div class="title"><h5>Opps!</h5></div>
                        <div class="body">
                            <p>We don't appear to have that email address in our system. Please <a href="/reset">try again</a></p>
                        </div>
                    <?php elseif (isset($_REQUEST['expired'])) : ?>
                        <div class="title"><h5>Opps!</h5></div>
                        <div class="body">
                            <p>This reset link has now expired please <a href="/reset">try again</a></p>
                        </div>
                    <?php else : ?>
                        <div class="title"><h5>Success!</h5></div>
                        <div class="body">
                            <p>We're sending you an email with instructions on how to change your password now.</p>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>