<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Boilerplate App</title>
</head>

<body>

<div id="content" class="wide">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 offset-sm-4">
                <div id="login" class="card">
                    <div class="title"><h5>Sign up</h5></div>
                    <div class="body">
                        <?= form_open('/signup/dosignup', 'id="signup_form" class="form form-horizontal"') ?>

                        <?php if (isset($_REQUEST['exists'])) : ?>
                        <div id="msgbox1" class="row">
                            <div class="col-sm-12">
                                <p id="msg1" class="warning small">
                                    The email address already exists in our system. Please log in or enter another email address.
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input id="fname" type="text" name="fname" required class="form-control" required>
                                    <label for="fname">First name</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input id="lname" type="text" name="lname" required class="form-control" required>
                                    <label for="lname">Last name</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" type="text" name="email" required class="form-control" required>
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="pwd" type="password" name="password" required class="form-control" required>
                                    <label for="pwd">Password</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="pwd2" type="password" name="password2" required class="form-control" required>
                                    <label for="pwd2">Confirm Password</label>
                                </div>
                            </div>
                        </div>

                        <div id="msgbox" class="row hide">
                            <div class="col-sm-12">
                                <p id="msg" class="warning small">

                                </p>
                            </div>
                        </div>

                        <div id="msgbox2" class="row hide">
                            <div class="col-sm-12">
                                <p id="msg2" class="warning small">

                                </p>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button id="signup_btn" type="button" class="btn btn-blue">Next</button>
                        </div>

                        <?= form_close(); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <p class="center small">Have an account? <a href="/login">Login here</a></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var pmErr = 0;
    var pwErr = 0;

    $(document).ready(function() {

        $('#pwd, #pwd2').keyup(function() {
            if ($('#pwd').val() === $('#pwd2').val()) {
                $('#msgbox').addClass('hide');
                $('#msg').html("");
                pmErr = 0;
            } else {
                $('#msgbox').removeClass('hide');
                $('#msg').html("Passwords do not match");
                pmErr = 1;
            }

            if ($('#pwd').val().length > 5) {
                $('#msgbox2').addClass('hide');
                $('#msg2').html("");
                pwErr = 0;
            } else {
                $('#msgbox2').removeClass('hide');
                $('#msg2').html("Password must be longer than 5 characters");
                pwErr = 1;
            }

        })



        $('#signup_btn').click(function() {


            if (pwErr === 0 && pmErr === 0) {
                $('#signup_form').submit();
            }



        })
    })
</script>

</body>
</html>