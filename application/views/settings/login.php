<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Boilerplate App</title>
</head>

<body>

<div id="content" class="wide">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 offset-sm-4">

                <div id="login" class="card">
                    <div class="title"><h5>Login</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['err'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="warning small">
                                        The email or password you entered is incorrect
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?= form_open('/login/dologin', 'class="form form-horizontal"') ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" type="text" name="email" required class="form-control" required>
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="password" type="password" name="password" required class="form-control" required>
                                    <label for="password">Password</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group right">
                            <button type="submit" class="btn btn-blue">Next</button>
                        </div>

                        <?= form_close(); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <p class="center small"><a href="/reset">Forgot password?</a></p>
                                <p class="center small">Don't have an account? <a href="/signup">sign up here</a></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>