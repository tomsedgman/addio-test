
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="title"><h5>Edit page</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="confirm small">
                                    Your details have been updated!
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?= form_open('page/edit/' . $page['key']); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                <input value="<?= $page['name'] ?>" id="name" value="" type="text" name="name" required class="form-control">
                                <label for="name">Name</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <p id="url2">
                                    URL:
                                </p>
                                <input id="url" value="" type="hidden"  name="url" required class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['main_keyword'] ?>" id="main_keyword" name="main_keyword" required class="form-control">
                                    <label for="main_keyword">Main Keyword</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['font'] ?>" id="font" name="font" required class="form-control">
                                    <label for="font">Font</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['fontfamily'] ?>" id="fontfamily" name="fontfamily" required class="form-control">
                                    <label for="fontfamily">Font Family</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group right">
                                    <button onclick="window.location.href='/page/configure/<?= $page['key'] ?>'" type="button" class="btn btn-grey">Back</button>
                                    <button type="submit" class="btn btn-blue">Update details</button>
                                </div>
                            </div>
                        </div>


                        <?= form_close(); ?>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#name').keyup(function() {

            url = $('#name').val().toLowerCase().split(' ').join('-');

            $('#url').val(url);
            $('#url2').html('URL: https://<?= $site['url'] ?>' + url);

        })

        url = $('#name').val().toLowerCase().split(' ').join('-');

        $('#url').val(url);
        $('#url2').html('URL: https://<?= $site['url'] ?>/' + url);

    })
</script>
