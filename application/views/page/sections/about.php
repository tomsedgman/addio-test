<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/about', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>About preview</h5></div>
                    <div class="body">
                        <div class="c_about" style="background-color: <?= $settings['about_bgcolor'] ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2 style="color: <?= $settings['about_fontcolor'] ?>"><?= $settings['about_titletext'] ?></h2>
                                        <p style="color: <?= $settings['about_fontcolor'] ?>"><?= nl2br($settings['about_description']) ?></p>
                                    </div>

                                    <div class="col-sm-6">
                                        <img style="width: 100%;" src="<?= $settings['about_image'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['about_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['about_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['about_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['about_fontcolor'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Image</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <img id="image_preview" style="width: 100%;" src="<?= $settings['about_image'] ?>"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="hide" id="image" name="image_file" type="file"/>
                                        <button id="image_upload" type="button" class="btn btn-blue">Upload image</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Content</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['about_titletext'] ?>" id="titletext" class="form-control" name="titletext"/>
                                            <label for="titletext">Header</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label style="margin-top: -15px" for="description">Text</label>
                                            <textarea rows="5" id="description" class="form-control" name="description"><?= $settings['about_description'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#image_upload').click(function() {
            $('#image').click();
        })

        $("#image").change(function() {

            readURL(this);
        });

        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

        $('#form_border_input').change(function() {
            $('#form_border').val($(this).val());
        })

        $('#form_headerx_input').change(function() {
            $('#form_headerx').val($(this).val());
        })


    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
