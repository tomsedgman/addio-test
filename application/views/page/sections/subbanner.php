<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/subbanner', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>Sub Banner preview</h5></div>
                    <div class="body">
                        <div class="c_subbanner" style="background-color: <?= $settings['subbanner_bgcolor'] ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h1 style="color: <?= $settings['subbanner_fontcolor'] ?>"><?= $settings['subbanner_bannertext'] ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['subbanner_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['subbanner_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['subbanner_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['subbanner_fontcolor'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Text</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['subbanner_bannertext'] ?>" id="bannertext" class="form-control" name="bannertext"/>
                                            <label for="bannertext">Banner Text</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#logo_upload').click(function() {
            $('#logo').click();
        })

        $("#logo").change(function() {

            readURL(this);
        });


        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logo_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
