<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/banner', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>Banner preview</h5></div>
                    <div class="body">
                        <div class="c_banner" style="border: none; background-size: cover; background-position: center; background-image: URL('<?= $settings['banner_bgimage'] ?>'); background-color: <?= $settings['banner_bgcolor'] ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php if ($settings['banner_boxbehindtext'] === 'Yes') : ?>
                                        <div class="bannertextarea">
                                            <?php else: ?>
                                            <div class="bannertextarea_blank">
                                                <?php endif; ?>
                                                <div class="bannertext" style="color: <?= $settings['banner_fontcolor'] ?>">
                                                    <p style="color: <?= $settings['banner_fontcolor'] ?>">
                                                        <?= $settings['banner_text'] ?>
                                                    </p>

                                                    <div class="row">
                                                        <?php if ($settings['banner_bp1_text'] !== '') : ?>
                                                            <div class="col-sm-12">
                                                                <div class="bulletpoint">
                                                                    <div style="color: <?= $settings['banner_fontcolor'] ?>"  class="bp_icon">
                                                                        <i class="ion ion-checkmark"></i>
                                                                    </div>
                                                                    <h3><?= $settings['banner_bp1_title'] ?></h3>
                                                                    <p><?= $settings['banner_bp1_text'] ?></p>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($settings['banner_bp2_text'] !== '') : ?>
                                                            <div class="col-sm-12">
                                                                <div class="bulletpoint">
                                                                    <div style="color: <?= $settings['banner_fontcolor'] ?>"  class="bp_icon">
                                                                        <i class="ion ion-checkmark"></i>
                                                                    </div>
                                                                    <h3><?= $settings['banner_bp2_title'] ?></h3>
                                                                    <p><?= $settings['banner_bp2_text'] ?></p>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($settings['banner_bp3_text'] !== '') : ?>
                                                            <div class="col-sm-12">
                                                                <div class="bulletpoint">
                                                                    <div style="color: <?= $settings['banner_fontcolor'] ?>"  class="bp_icon">
                                                                        <i class="ion ion-checkmark"></i>
                                                                    </div>
                                                                    <h3><?= $settings['banner_bp3_title'] ?></h3>
                                                                    <p><?= $settings['banner_bp3_text'] ?></p>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-sm-6">

                                            <div class="banner_form" style="border-color: <?= $settings['banner_form_border'] ?>">

                                                <div class="banner_form_header" style="background-color: <?= $settings['banner_form_header'] ?>;">

                                                    <h4>
                                                        <?= $settings['banner_form_headline'] ?>
                                                    </h4>
                                                    <h5>
                                                        <?= $settings['banner_form_headline2'] ?>
                                                    </h5>
                                                </div>

                                                <div class="banner_form_body">

                                                    <div class="title">
                                                        <?= $settings['banner_form_pretext'] ?>
                                                    </div>



                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label><?= $settings['banner_form_textbox1'] ?></label>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label><?= $settings['banner_form_textbox2'] ?></label>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label><?= $settings['banner_form_textbox3'] ?></label>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label><?= $settings['banner_form_textbox4'] ?></label>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>


                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <button style="background-color: <?= $settings['banner_form_header'] ?>;" type="button" class="btn"><?= $settings['banner_form_button'] ?></button>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['banner_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['banner_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['banner_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['banner_fontcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <p>Form Header / Button</p>
                                        <input id="form_headerx_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['banner_form_header'] ?>"/><br/><br/>
                                        <input id="form_headerx" name="form_header" style="width: 70%" class="form-control" value="<?= $settings['banner_form_header'] ?>"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <p>Form Border</p>
                                        <input id="form_border_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['banner_form_border'] ?>"/><br/><br/>
                                        <input id="form_border" name="form_border" style="width: 70%" class="form-control" value="<?= $settings['banner_form_border'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Background</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <img id="bgimage_preview" style="width: 100%;" src="<?= $settings['banner_bgimage'] ?>"/>
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="hide" id="bgimage" name="bgimage_file" type="file"/>
                                        <button id="bgimage_upload" type="button" class="btn btn-blue">Upload image</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Banner content</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_text'] ?>" id="bannertext" class="form-control" name="bannertext"/>
                                            <label for="bannertext">Text</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label style="margin-top: -15px;" for="bannertext">Show box behind text</label>
                                            <select id="bannerbox" class="form-control" name="bannerbox">
                                                <option <?php if ($settings['banner_boxbehindtext'] == "No") { echo "selected "; } ?>>No</option>
                                                <option <?php if ($settings['banner_boxbehindtext'] == "Yes") { echo "selected "; } ?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Bullet Points</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_bp1_title'] ?>" id="bp1_title" class="form-control" name="bp1_title"/>
                                            <label for="bp1_title">Title 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_bp1_text'] ?>" id="bp1_text" class="form-control" name="bp1_text"/>
                                            <label for="bp1_text">Text 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_bp2_title'] ?>" id="bp2_title" class="form-control" name="bp2_title"/>
                                            <label for="bp2_title">Title 2</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_bp2_text'] ?>" id="bp2_text" class="form-control" name="bp2_text"/>
                                            <label for="bp2_text">Text 2</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_bp3_title'] ?>" id="bp3_title" class="form-control" name="bp3_title"/>
                                            <label for="bp3_title">Title 3</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_bp3_text'] ?>" id="bp3_text" class="form-control" name="bp3_text"/>
                                            <label for="bp3_text">Text 3</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Form Text</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_headline'] ?>" id="form_headline" class="form-control" name="form_headline"/>
                                            <label for="form_headline">Form Headline 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_headline2'] ?>" id="form_headline2" class="form-control" name="form_headline2"/>
                                            <label for="form_headline1">Form Headline 2</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_pretext'] ?>" id="form_pretext" class="form-control" name="form_pretext"/>
                                            <label for="form_pretext">Pretext</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_button'] ?>" id="form_button" class="form-control" name="form_button"/>
                                            <label for="form_button">Button text</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Form Inputs</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_textbox1'] ?>" id="form_textbox_1" class="form-control" name="form_textbox_1"/>
                                            <label for="form_textbox_1">Input 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_textbox2'] ?>" id="form_textbox_2" class="form-control" name="form_textbox_2"/>
                                            <label for="form_textbox_2">Input 2</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_textbox3'] ?>" id="form_textbox_3" class="form-control" name="form_textbox_3"/>
                                            <label for="form_textbox_3">Input 3</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['banner_form_textbox4'] ?>" id="form_textbox_4" class="form-control" name="form_textbox_4"/>
                                            <label for="form_textbox_4">Input 4</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#bgimage_upload').click(function() {
            $('#bgimage').click();
        })

        $("#bgimage").change(function() {

            readURL(this);
        });

        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

        $('#form_border_input').change(function() {
            $('#form_border').val($(this).val());
        })

        $('#form_headerx_input').change(function() {
            $('#form_headerx').val($(this).val());
        })


    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#bgimage_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
