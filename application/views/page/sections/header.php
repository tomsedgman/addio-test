<?php

$settings = (Array) json_decode($page['settings']);

?>

<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/header', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>Header preview</h5></div>
                    <div class="body">
                        <div class="c_header" style="background-color: <?= $settings['head_bgcolor'] ?>">
                            <div class="container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="logo_preview">
                                        <?php if (isset($settings['head_logo'])) : ?>
                                            <img src="<?= $settings['head_logo'] ?>"/>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <?php if (isset($settings['head_link_3'])) : ?>
                                        <div class="menu_preview" style="color: <?= $settings['head_fontcolor'] ?>">
                                            <?= $settings['head_link_3'] ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (isset($settings['head_link_2'])) : ?>
                                        <div class="menu_preview" style="color: <?= $settings['head_fontcolor'] ?>">
                                            <?= $settings['head_link_2'] ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (isset($settings['head_link_1'])) : ?>
                                        <div class="menu_preview" style="color: <?= $settings['head_fontcolor'] ?>">
                                            <?= $settings['head_link_1'] ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="phone" style="color: <?= $settings['head_phonecolor'] ?>">
                                        <?= str_replace('+44', '0', $phone) ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['head_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['head_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['head_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['head_fontcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Phone colour</p>
                                        <input id="phonecolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['head_phonecolor'] ?>"/><br/><br/>
                                        <input id="phonecolor" name="phonecolor" style="width: 70%" class="form-control" value="<?= $settings['head_phonecolor'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Logo</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php if (isset($settings['head_logo'])) : ?>
                                            <img id="logo_preview" style="height: 100px;" src="<?= $settings['head_logo'] ?>"/>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="hide" id="logo" name="logo_file" type="file"/>
                                        <button id="logo_upload" type="button" class="btn btn-blue">Upload logo</button>
                                        <p>

                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>




                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Menu Links</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input value="<?= $settings['head_link_1'] ?>" id="link1" class="form-control" name="link1"/>
                                            <label for="link1">Link 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input value="<?= $settings['head_dest_1'] ?>" id="dest1" class="form-control" name="dest1"/>
                                            <label for="dest1">Destination URL</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input value="<?= $settings['head_link_2'] ?>" id="link2" class="form-control" name="link2"/>
                                            <label for="link2">Link 2</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input value="<?= $settings['head_dest_2'] ?>" id="dest2" class="form-control" name="dest2"/>
                                            <label for="dest2">Destination URL</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input value="<?= $settings['head_link_3'] ?>" id="link3" class="form-control" name="link3"/>
                                            <label for="link3">Link 3</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input value="<?= $settings['head_dest_3'] ?>" id="dest3" class="form-control" name="dest3"/>
                                            <label for="dest3">Destination URL</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#logo_upload').click(function() {
            $('#logo').click();
        })

        $("#logo").change(function() {

            readURL(this);
        });

        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

        $('#phonecolor_input').change(function() {
            $('#phonecolor').val($(this).val());
        })

    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logo_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
