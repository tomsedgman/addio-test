<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/cta', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>Call to action preview</h5></div>
                    <div class="body">
                        <div class="c_cta" style="background-color: <?= $settings['cta_bgcolor'] ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h1 style="color: <?= $settings['cta_fontcolor'] ?>"><?= $settings['cta_header1'] ?></h1>
                                    <h3 style="color: <?= $settings['cta_fontcolor'] ?>"><?= $settings['cta_header2'] ?></h3>
                                    <h1><a style="font-weight: 300; color: <?= $settings['cta_fontcolor'] ?>" href="mailto:<?= $page['email'] ?>"><strong>E.</strong> <?= $page['email'] ?></a> <a style="font-weight: 300; color: <?= $settings['cta_fontcolor'] ?>" href="tel:<?= str_replace(' ', '', $page['phone']) ?>"><strong>T.</strong> <?= $page['phone'] ?></a></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['cta_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['cta_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['cta_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['cta_fontcolor'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Text</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['cta_header1'] ?>" id="headertext1" class="form-control" name="headertext1"/>
                                            <label for="headertext1">Header text 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['cta_header2'] ?>" id="headertext2" class="form-control" name="headertext2"/>
                                            <label for="headertext2">Header text 2</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {



        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

    })




</script>
