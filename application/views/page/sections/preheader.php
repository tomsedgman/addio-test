<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/preheader', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>Preheader preview</h5></div>
                    <div class="body">
                        <div class="c_preheader" style="background-color: <?= $settings['prehead_bgcolor'] ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h1 style="color: <?= $settings['prehead_fontcolor'] ?>"><?= $settings['prehead_text1'] ?></h1>
                                    <h3 style="color: <?= $settings['prehead_fontcolor'] ?>"><?= $settings['prehead_text2'] ?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['prehead_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['prehead_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['prehead_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['prehead_fontcolor'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Text</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['prehead_text1'] ?>" id="headertext1" class="form-control" name="headertext1"/>
                                            <label for="headertext1">Header text 1</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input value="<?= $settings['prehead_text2'] ?>" id="headertext2" class="form-control" name="headertext2"/>
                                            <label for="headertext2">Header text 2</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#logo_upload').click(function() {
            $('#logo').click();
        })

        $("#logo").change(function() {

            readURL(this);
        });

        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logo_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
