<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content">
    <div class="container-fluid">
        <?= form_open_multipart('/page/saveconfigure/' . $page['key'] . '/testimonial', 'class="form form-horizontal"'); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title"><h5>Testimonial preview</h5></div>
                    <div class="body">
                        <div class="c_testimonial" style="background-color: <?= $settings['test_bgcolor'] ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="testimonial">
                                            <img class="quote-left" src="/uploads/quote-left.png"/>
                                            <span style="color: <?= $settings['test_fontcolor'] ?>"><?= $settings['test_text1'] ?></span>
                                        </div>
                                        <div class="author">
                                            <span style="color: <?= $settings['test_fontcolor'] ?>"><?= $settings['test_author1'] ?></span>
                                        </div>
                                        <img class="quote-right" src="/uploads/quote-right.png"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="testimonial">
                                            <img class="quote-left" src="/uploads/quote-left.png"/>
                                            <span style="color: <?= $settings['test_fontcolor'] ?>"><?= $settings['test_text2'] ?></span>
                                        </div>
                                        <div class="author">
                                            <span style="color: <?= $settings['test_fontcolor'] ?>"><?= $settings['test_author2'] ?></span>
                                        </div>
                                        <img class="quote-right" src="/uploads/quote-right.png"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Colours</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Background</p>
                                        <input id="bgcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['test_bgcolor'] ?>"/><br/><br/>
                                        <input id="bgcolor" name="bgcolor" style="width: 70%" class="form-control" value="<?= $settings['test_bgcolor'] ?>"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Font colour</p>
                                        <input id="fontcolor_input" style="height: 50px; width: 50px;" type="color" value="<?= $settings['test_fontcolor'] ?>"/><br/><br/>
                                        <input id="fontcolor" name="fontcolor" style="width: 70%" class="form-control" value="<?= $settings['test_fontcolor'] ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="card">
                            <div class="title"><h5>Text</h5></div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input required value="<?= $settings['test_text1'] ?>" id="test1_text" class="form-control" name="test1_text"/>
                                            <label for="test1_text">Testimonial 1</label>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <input required value="<?= $settings['test_author1'] ?>" id="test1_author" class="form-control" name="test1_author"/>
                                            <label for="test1_author">Author</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input required value="<?= $settings['test_text2'] ?>" id="test2_text" class="form-control" name="test2_text"/>
                                            <label for="test2_text">Testimonial 2</label>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <input required value="<?= $settings['test_author2'] ?>" id="test2_author" class="form-control" name="test2_author"/>
                                            <label for="test2_author">Author</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 right">
                <button class="btn btn-blue">Save changes</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#logo_upload').click(function() {
            $('#logo').click();
        })

        $("#logo").change(function() {

            readURL(this);
        });


        $('#bgcolor_input').change(function() {
            $('#bgcolor').val($(this).val());
        })

        $('#fontcolor_input').change(function() {
            $('#fontcolor').val($(this).val());
        })

    })

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logo_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
