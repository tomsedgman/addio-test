
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="title"><h5>Edit call tracking</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="confirm small">
                                    Your details have been updated!
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>


                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php if (!$page['trackable_phone'] > '0') : ?>
                                        <button type="button" onclick="window.location.href='/page/searchnumbers/<?= $page['key'] ?>'" class="btn btn-green">Get trackable number (UK Only)</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($page['trackable_phone'] > '0') : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <p style="margin-top: -25px;" for="">Tracking number: <?= $page['trackable_phone'] ?></p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="button" onclick="window.location.href='/page/deletenumber/<?= $page['key'] ?>/<?= $page['twillio_key'] ?>'" class="btn btn-red">Remove tracking number</button>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group right">
                                    <button onclick="window.location.href='/page/configure/<?= $page['key'] ?>'" type="button" class="btn btn-grey">Back</button>
                                </div>
                            </div>
                        </div>


                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#name').keyup(function() {

            url = $('#name').val().toLowerCase().split(' ').join('-');

            $('#url').val(url);
            $('#url2').html('URL: https://<?= $site['url'] ?>' + url);

        })

        url = $('#name').val().toLowerCase().split(' ').join('-');

        $('#url').val(url);
        $('#url2').html('URL: https://<?= $site['url'] ?>/' + url);

    })
</script>
