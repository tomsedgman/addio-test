<?php

$settings = (Array) json_decode($page['settings']);

?>
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-6">

                <div class="card">
                    <div class="title"><h5>Page details</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['saved'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="confirm small">
                                        The information has been saved
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?= form_open('page/edit/'); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['name'] ?>" id="name" value="" type="text" name="name" required class="form-control">

                                    <label for="name">Name</label>
                                </div>
                            </div>
                        </div>

                        <input id="url" value="" type="hidden"  name="url" required class="form-control">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['main_keyword'] ?>" id="main_keyword" name="main_keyword" required class="form-control">
                                    <label for="main_keyword">Main Keyword</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['email'] ?>" id="email" name="email" required class="form-control">
                                    <label for="email">Email to receive contact form enquiries</label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $page['phone'] ?>" id="phone" name="phone" required class="form-control">
                                    <label for="name">Phone number to call</label>
                                </div>
                            </div>
                        </div>
                        <input value="<?= $page['font'] ?>" id="font" name="font" type="hidden" class="form-control"/>
                        <input value="<?= $page['fontfamily'] ?>" id="fontfamily" type="hidden" name="fontfamily"  class="form-control"/>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" id="fontchoice">
                                        <?php foreach ($fonts as $f) : ?>
                                            <option <?php  if ($page['font'] == $f['name']) { echo " selected "; } ?> value="<?= $f['id'] ?>"><?= $f['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label for="fontchoice" style="margin-top: -20px;">Font</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group right">
                                    <button class="btn btn-blue">Update details</button>
                                </div>
                            </div>
                        </div>


                        <?= form_close(); ?>
                    </div>
                </div>



            </div>


            <div class="col-sm-6">
                <div class="card">
                    <div class="title"><h5>Page URLs</h5></div>
                    <div class="body">
                        <div class="row">

                            <div class="col-sm-12">
                                <p id="url2">
                                    Live URL:
                                </p>
                                <input id="url" value="" type="hidden"  name="url" required class="form-control">
                            </div>

                            <div class="col-sm-12">
                                <p id="url3">
                                    Test URL:
                                </p>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group right">
                                    <button onclick="window.location.href='/page/editor'" type="button" class="btn btn-blue">Open page editor</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="title"><h5>Edit call tracking</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="confirm small">
                                        Your details have been updated!
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>


                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php if (!$page['trackable_phone'] > '0') : ?>
                                        <button type="button" onclick="window.location.href='/page/searchnumbers'" class="btn btn-green">Get trackable number (UK Only)</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($page['trackable_phone'] > '0') : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <p style="margin-top: -25px;" for="">Tracking number: <?= $page['trackable_phone'] ?></p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="button" onclick="window.location.href='/page/deletenumber/<?= $page['twillio_key'] ?>'" class="btn btn-red">Remove tracking number</button>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<script>

    var fonts = <?= $fontsjson ?>;

    $(document).ready(function() {


        $('#font').val(fonts[$('#fontchoice').val()].name);
        $('#fontfamily').val(fonts[$('#fontchoice').val()].style);

    })


    $('#fontchoice').change(function() {

        $('#font').val(fonts[$(this).val()].name);
        $('#fontfamily').val(fonts[$(this).val()].style);


    })


    $('#name').keyup(function() {

        url = $('#name').val().toLowerCase().split(' ').join('-');

        $('#url').val(url);
        $('#url2').html('URL: https://<?= $site['url'] ?>' + url);

    })

    url = $('#name').val().toLowerCase().split(' ').join('-');

    $('#url').val(url);
    $('#url2').html('<a target="_blank" href="https://<?= $site['url'] ?>/' + url + '">Live URL: https://<?= $site['url'] ?>/' + url + "</a>");
    $('#url3').html('<a target="_blank" href="https://test.addio.co.uk/view/test/<?= $page['key'] ?>">Test URL: https://test.addio.co.uk/view/test/<?= $page['key'] ?></a>');

    setTimeout(
        function()
        {
            $('#msgbox1').addClass('hide');
        }, 1500);

</script>