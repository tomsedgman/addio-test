
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="title"><h5>New page</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="confirm small">
                                    Your details have been updated!
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?= form_open('page/add'); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="name" value="" type="text" name="name" required class="form-control">
                                    <label for="name">Name</label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label style="top: -15px;" for="url">URL</label>
                                    <input id="url2" value="" type="text" disabled required class="form-control">
                                    <input id="url" value="" type="hidden"  name="url" required class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="phone" name="phone" required class="form-control">
                                    <label for="name">PHONE NUMBER TO CALL</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" name="email" required class="form-control">
                                    <label for="email">EMAIL TO RECEIVE CONTACT FORM ENQUIRIES</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="main_keyword" name="main_keyword" required class="form-control">
                                    <label for="main_keyword">Main Keyword</label>
                                </div>
                            </div>
                        </div>

                            <div class="form-group right">
                            <button onclick="window.location.href=document.referrer" type="button" class="btn btn-grey">Back</button>
                            <button class="btn btn-blue">Add page</button>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#name').keyup(function() {

            url = $('#name').val().toLowerCase().split(' ').join('-');

            $('#url').val(url);
            $('#url2').val(url);

        })

    })
</script>
