
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <?php foreach ($pages as $p) : ?>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="title">
                            <h5><?= $p['name'] ?></h5>
                        </div>
                        <div class="body">
                            <p style="height: 50px;">
                                <strong>URL:</strong> https://<?= $p['site_url'] ?>/<?= $p['url'] ?>
                            </p>

                            <div class="row">
                                <div class="col-sm-12 right">
                                    <button onclick="duplicatePage('<?= $p['key'] ?>')" class="btn btn-blue">Duplicate page</button>
                                    <button onclick="window.location.href='/page/configure/<?= $p['key'] ?>'" class="btn btn-green">Edit page</button>
                                </div>
                                <div class="col-sm-12">
                                    <br/>
                                    <a class="red" style="font-size: 12px; text-decoration: underline" href="/page/delete/<?= $p['key'] ?>/check">delete page</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<div class="floating_btn" onclick="window.location.href='/page/add'">
    <i class="ion ion-plus"/>
</div>


<script>

    function duplicatePage(key) {

        window.location.href="/page/duplicate/" + key;

    }


</script>