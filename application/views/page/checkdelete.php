
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="title"><h5>Delete page</h5></div>
                    <div class="body">

                        <div class="row">
                            <div class="col-sm-12">
                                <p>
                                    Are you sure you want to delete the page "<strong><?= $page['name'] ?></strong>"? you will no longer be able to edit this page.
                                </p>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group right">
                                    <button onclick="window.location.href='/page'" type="button" class="btn btn-grey">Go back</button>
                                    <button onclick="window.location.href='/page/delete/<?= $page['key'] ?>/delete'" class="btn btn-red">Yes delete</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {


    })
</script>
