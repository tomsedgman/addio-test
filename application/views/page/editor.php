<?php

$settings = (Array) json_decode($page['settings']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <?php if ($page['font'] !== '') : ?>

        <link href='https://fonts.googleapis.com/css?family=<?= str_replace(' ', '+', $page['font']) ?>:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <?php else : ?>

        <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <?php endif; ?>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link href="/css/theme.css" rel="stylesheet">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script src='/js/spectrum.js'></script>
    <link rel='stylesheet' href='/js/spectrum.css' />

    <title>Addio</title>
</head>

<body>
<div id="content" class="wide" style="background-color: #FFF;">
    <div class="container-fluid">

        <?= form_open_multipart('/page/saveeditor'); ?>

        <div style="position: fixed; z-index: 10; border-bottom: 3px solid #CCC; background-color: #fff; padding: 15px; top: 0; width: 100%; left: 0;">
            <div class="row">
                <div class="col-sm-4">
                    <button onclick="window.location.href='/page/configure'" type="button" class="btn btn-blue">Exit</button>
                </div>
                <div class="col-sm-4">
                    <?php if (isset($_REQUEST['success'])) : ?>
                    <div id="save_popup" class="alert alert-success center">
                        <strong>Success!</strong> All changes saved
                    </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-4 right">
                    <button id="save_button" type="submit" class="btn btn-green">Save changes</button>
                </div>

            </div>
        </div>



        <!-- HEADER -->
        <div class="row" style="margin-top: 50px;">
            <div class="col-sm-12">

                <div class="card" >

                    <div class="body">
                        <div class="c_header clickable2" data-section="Header" style="background-color: <?= $settings['head_bgcolor'] ?>; position: relative;">

                            <div class="color_picker">
                                <input id="head_bgcolor_input"      name="head_bgcolor"     class="color_item" type="color" value="<?= $settings['head_bgcolor'] ?>"/><br/>
                                <input id="head_fontcolor_input"    name="head_fontcolor"   class="color_item" type="color" value="<?= $settings['head_fontcolor'] ?>"/>
                                <div class="image_item" id="head_logo_choose">
                                    <i class="ion ion-image"></i>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="logo_preview" style="position: relative;">
                                            <input style="display: none;" id="head_logo_upload" name="head_logo_upload" type="file"/>
                                            <img style="cursor: pointer;" id="head_logo_display" src="<?= $settings['head_logo'] ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">



                                    </div>
                                    <div class="col-sm-3">
                                        <div id="head_phone_preview" class="head_fc phone" style="color: <?= $settings['head_fontcolor'] ?>">
                                            <?= str_replace('+44', '0', $phone) ?>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                        <div class="c_preheader" data-section="Preheader" style="<?= $page['fontfamily']; ?>; background-color: <?= $settings['prehead_bgcolor'] ?>; position: relative">
                            <div class="color_picker">
                                <input name="prehead_bgcolor" id="prehead_bgcolor_input" class="color_item" type="color" value="<?= $settings['prehead_bgcolor'] ?>"/><br/>
                                <input name="prehead_fontcolor" id="prehead_fontcolor_input" class="color_item" type="color" value="<?= $settings['prehead_fontcolor'] ?>"/>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="prehead_text1" class="prehead_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(255, 255, 255, 0.3); background: transparent; color: <?= $settings['prehead_fontcolor'] ?>; font-size: 32px; width: 100%; text-align: center;" ><?= $settings['prehead_text1'] ?></textarea>
                                        <textarea name="prehead_text2" class="prehead_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['prehead_fontcolor'] ?>; font-size: 19px; font-weight: 300; letter-spacing: 1px; width: 100%; text-align: center;" ><?= $settings['prehead_text2'] ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="c_banner" data-section="Banner" style="<?= $page['fontfamily']; ?> border: none; background-size: cover; background-position: center; background-image: URL('<?= $settings['banner_bgimage'] ?>'); background-color: <?= $settings['banner_bgcolor'] ?>; position: relative;">

                            <div class="color_picker">
                                <input name="banner_bgcolor" id="banner_bgcolor_input" class="color_item" type="color" value="<?= $settings['banner_bgcolor'] ?>"/><br/>
                                <input name="banner_fontcolor" id="banner_fontcolor_input"   class="color_item" type="color" value="<?= $settings['banner_fontcolor'] ?>"/>
                                <div class="image_item" id="banner_image_choose">
                                    <i class="ion ion-image"></i>
                                </div>
                                <div onclick="window.location.href='/page/remove_banner_bg/<?= $page['key'] ?>'" class="image_item center" id="banner_image_choose" style="margin-top: 5px;">
                                    X
                                </div>
                                <input style="display: none;" id="banner_image_upload" name="banner_image_upload" type="file"/>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php if ($settings['banner_boxbehindtext'] === 'Yes') : ?>
                                        <div class="bannertextarea">
                                            <?php else: ?>
                                            <div class="bannertextarea_blank">
                                                <?php endif; ?>
                                                <div class="bannertext" style="color: <?= $settings['banner_fontcolor'] ?>">
                                                    <p style="color: <?= $settings['banner_fontcolor'] ?>">
                                                        <textarea name="banner_text" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; font-weight: 300; letter-spacing: 1px; font-size: 18px; width: 100%;" ><?= $settings['banner_text'] ?></textarea>
                                                    </p>

                                                    <div class="row">
                                                        <?php if ($settings['banner_bp1_text'] !== '') : ?>
                                                            <div class="col-sm-12">
                                                                <div class="bulletpoint">
                                                                    <div style="color: <?= $settings['banner_fontcolor'] ?>"  class="bp_icon">
                                                                        <i class="ion ion-checkmark"></i>
                                                                    </div>
                                                                    <textarea name="banner_bp1_title" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; letter-spacing: 1px; font-weight: 500; line-height: 33px;font-size: 28px; width: 100%;" ><?= $settings['banner_bp1_title'] ?></textarea>
                                                                    <textarea name="banner_bp1_text" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 18px; width: 100%;" ><?= $settings['banner_bp1_text'] ?></textarea>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($settings['banner_bp2_text'] !== '') : ?>
                                                            <div class="col-sm-12">
                                                                <div class="bulletpoint">
                                                                    <div style="color: <?= $settings['banner_fontcolor'] ?>"  class="bp_icon">
                                                                        <i class="ion ion-checkmark"></i>
                                                                    </div>
                                                                    <textarea name="banner_bp2_title" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; letter-spacing: 1px; font-weight: 500; line-height: 33px;font-size: 28px; width: 100%;" ><?= $settings['banner_bp2_title'] ?></textarea>
                                                                    <textarea name="banner_bp2_text" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 18px; width: 100%;" ><?= $settings['banner_bp2_text'] ?></textarea>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($settings['banner_bp3_text'] !== '') : ?>
                                                            <div class="col-sm-12">
                                                                <div class="bulletpoint">
                                                                    <div style="color: <?= $settings['banner_fontcolor'] ?>"  class="bp_icon">
                                                                        <i class="ion ion-checkmark"></i>
                                                                    </div>
                                                                    <textarea name="banner_bp3_title" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; letter-spacing: 1px; font-weight: 500; line-height: 33px;font-size: 28px; width: 100%;" ><?= $settings['banner_bp3_title'] ?></textarea>
                                                                    <textarea name="banner_bp3_text" class="banner_fc" rows="2" style="border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 18px; width: 100%;" ><?= $settings['banner_bp3_text'] ?></textarea>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-sm-6">

                                            <div class="banner_form" style="<?= $page['fontfamily']; ?> border-color: <?= $settings['banner_form_border'] ?>; position: relative;">

                                                <div class="color_picker">
                                                    <input id="form_bordercolor_input" name="banner_form_border" class="color_item" type="color" value="<?= $settings['banner_form_border'] ?>"/><br/>
                                                    <input id="form_bgcolor_input"     name="banner_form_header" class="color_item" type="color" value="<?= $settings['banner_form_header'] ?>"/>
                                                </div>

                                                <div class="banner_form_header" style="background-color: <?= $settings['banner_form_header'] ?>; position: relative;">


                                                    <textarea name="banner_form_headline" class="banner_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 500; font-size: 24px; width: 100%;" ><?= $settings['banner_form_headline'] ?></textarea>
                                                    <textarea name="banner_form_headline2" class="banner_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['banner_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 300; font-size: 20px; width: 100%;" ><?= $settings['banner_form_headline2'] ?></textarea>
                                                </div>

                                                <div class="banner_form_body">

                                                    <div class="title">
                                                        <textarea name="banner_form_pretext" rows="2" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3); text-align: center; background: transparent; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 16px; width: 100%;" ><?= $settings['banner_form_pretext'] ?></textarea>
                                                    </div>



                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea name="banner_form_textbox1" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3); background: transparent; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 16px; width: 100%;" ><?= $settings['banner_form_textbox1'] ?></textarea>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea name="banner_form_textbox2" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3); background: transparent; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 16px; width: 100%;" ><?= $settings['banner_form_textbox2'] ?></textarea>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea name="banner_form_textbox3" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3); background: transparent; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 16px; width: 100%;" ><?= $settings['banner_form_textbox3'] ?></textarea>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea name="banner_form_textbox4" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3); background: transparent; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 16px; width: 100%;" ><?= $settings['banner_form_textbox4'] ?></textarea>
                                                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                                                        </div>
                                                    </div>


                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <textarea class="banner_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3); color: #FFFFFF; text-align: center; text-transform: uppercase; padding: 10px; background-color: <?= $settings['banner_form_header'] ?>; letter-spacing: 1px; font-weight: 300; line-height: 25px;font-size: 20px; width: 100%;" ><?= $settings['banner_form_button'] ?></textarea>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="c_subbanner" style="<?= $page['fontfamily']; ?>; background-color: <?= $settings['subbanner_bgcolor'] ?>; position: relative;">

                                <div class="color_picker">
                                    <input name="subbanner_bgcolor"     id="subbanner_bgcolor_input"        class="color_item" type="color" value="<?= $settings['subbanner_bgcolor'] ?>"/><br/>
                                    <input name="subbanner_fontcolor"   id="subbanner_fontcolor_input"      class="color_item" type="color" value="<?= $settings['subbanner_fontcolor'] ?>"/>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="subbanner_bannertext" class="subbanner_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(255, 255, 255, 0.3);  background: transparent; color: <?= $settings['subbanner_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 400; font-size: 26px; width: 100%;" ><?= $settings['subbanner_bannertext'] ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="c_testimonial" style="<?= $page['fontfamily']; ?>; background-color: <?= $settings['test_bgcolor'] ?>; position: relative;">

                                <div class="color_picker">
                                    <input name="test_bgcolor" id="test_bgcolor_input"      class="color_item" type="color" value="<?= $settings['test_bgcolor'] ?>"/><br/>
                                    <input name="test_fontcolor" id="test_fontcolor_input"    class="color_item" type="color" value="<?= $settings['test_fontcolor'] ?>"/>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="testimonial">
                                                <img class="quote-left" src="/uploads/quote-left.png"/>
                                                <textarea name="test_text1" class="test_fc" rows="2" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['test_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 300; font-size: 20px; width: 100%;" ><?= $settings['test_text1'] ?></textarea>
                                            </div>
                                            <div class="author">
                                                <textarea name="test_author1" class="test_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['test_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 500; font-size: 18px; width: 100%;" ><?= $settings['test_author1'] ?></textarea>
                                            </div>
                                            <img class="quote-right" src="/uploads/quote-right.png"/>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="testimonial">
                                                <img class="quote-left" src="/uploads/quote-left.png"/>
                                                <textarea name="test_text2" class="test_fc" rows="2" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['test_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 300; font-size: 20px; width: 100%;" ><?= $settings['test_text2'] ?></textarea>
                                            </div>
                                            <div class="author">
                                                <textarea name="test_author2" class="test_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['test_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 500; font-size: 18px; width: 100%;" ><?= $settings['test_author2'] ?></textarea>
                                            </div>
                                            <img class="quote-right" src="/uploads/quote-right.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="c_about" style="<?= $page['fontfamily']; ?>; background-color: <?= $settings['about_bgcolor'] ?>; position: relative;">

                                <div class="color_picker">
                                    <input id="about_bgcolor_input" name="about_bgcolor"class="color_item" type="color" value="<?= $settings['about_bgcolor'] ?>"/><br/>
                                    <input id="about_fontcolor_input" name="about_fontcolor"   class="color_item" type="color" value="<?= $settings['about_fontcolor'] ?>"/>

                                    <div class="image_item" id="about_image_choose">
                                        <i class="ion ion-image"></i>
                                    </div>
                                    <input style="display: none;" id="about_image_upload" name="about_image_upload" type="file"/>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <textarea name="about_titletext" class="about_fc" rows="2" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['about_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 500; font-size: 30px; width: 100%;" ><?= $settings['about_titletext'] ?></textarea>
                                            <textarea name="about_description" class="about_fc" rows="10" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['about_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 300; font-size: 16px; width: 100%;" ><?= $settings['about_description'] ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <img id="about_image_display" style="width: 100%;" src="<?= $settings['about_image'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="c_cta" style="<?= $page['fontfamily']; ?>; background-color: <?= $settings['cta_bgcolor'] ?>; position: relative;">

                                <div class="color_picker">
                                    <input id="cta_bgcolor_input" name="cta_bgcolor" class="color_item" type="color" value="<?= $settings['cta_bgcolor'] ?>"/><br/>
                                    <input id="cta_fontcolor_input" name="cta_fontcolor"   class="color_item" type="color" value="<?= $settings['cta_fontcolor'] ?>"/>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <textarea name="cta_header1" class="cta_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['cta_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 400; font-size: 30px; width: 100%;" ><?= $settings['cta_header1'] ?></textarea>
                                            <textarea name="cta_header2" class="cta_fc" rows="1" style="<?= $page['fontfamily']; ?> border: 1px dotted rgba(0, 0, 0, 0.3);  background: transparent; color: <?= $settings['cta_fontcolor'] ?>; text-align: center; letter-spacing: 1px; font-weight: 300; font-size: 18px; width: 100%;" ><?= $settings['cta_header2'] ?></textarea>
                                            <h1>
                                                <a class="cta_fc" style="<?= $page['fontfamily']; ?> font-weight: 300; color: <?= $settings['cta_fontcolor'] ?>" href="mailto:<?= $page['email'] ?>"><strong>E.</strong> <?= $page['email'] ?></a>
                                                <a class="cta_fc" style="<?= $page['fontfamily']; ?> font-weight: 300; color: <?= $settings['cta_fontcolor'] ?>" href="tel:<?= str_replace(' ', '', $page['phone']) ?>"><strong>T.</strong> <?= $page['phone'] ?></a></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>




                    </div>
                </div>
            </div>
        </div>

<?php form_close(); ?>



        </div>
    </div>
</div>

<script>

    var settings = <?= $page['settings'] ?>;

    $(document).ready(function() {


        $("#head_bgcolor_input, #head_fontcolor_input, #prehead_bgcolor_input, #prehead_fontcolor_input, #banner_bgcolor_input, #banner_fontcolor_input, #form_bordercolor_input, #form_bgcolor_input, #subbanner_bgcolor_input, #subbanner_fontcolor_input, #test_bgcolor_input, #test_fontcolor_input, #about_bgcolor_input, #about_fontcolor_input, #cta_bgcolor_input, #cta_fontcolor_input").spectrum({
            preferredFormat: "hex",
            showInput: true,
            showPalette: false,
        });






        $(':input').change(function() {

            $('#save_button').addClass('btn-red');
            $('#save_button').text('You have unsaved changes. Click here to save now');

        })

        //$('#save_popup').addClass('hide');

        $("#save_popup").removeClass("hide").delay(2000).queue(function(next){
            $(this).addClass("hide");
            next();
        });

        //alert(settings['head_bgcolor']);

        $('#head_bgcolor_input').change(function() {

            $('.c_header').css('backgroundColor', $(this).val());
        })

        $('#head_fontcolor_input').change(function() {

            $('.head_fc').css('color', $(this).val());
        })

        $('#prehead_bgcolor_input').change(function() {
            $('.c_preheader').css('backgroundColor', $(this).val());
        })

        $('#prehead_fontcolor_input').change(function() {

            $('.prehead_fc').css('color', $(this).val());
        })

        $('#banner_bgcolor_input').change(function() {

            $('.c_banner').css('backgroundColor', $(this).val());
        })

        $('#banner_fontcolor_input').change(function() {

            $('.banner_fc').css('color', $(this).val());
        })

        $('#form_bordercolor_input').change(function() {

            $('.banner_form').css('borderColor', $(this).val());
        })

        $('#form_bgcolor_input').change(function() {

            $('.banner_form_header').css('backgroundColor', $(this).val());
        })

        $('#subbanner_bgcolor_input').change(function() {

            $('.c_subbanner').css('backgroundColor', $(this).val());
        })

        $('#subbanner_fontcolor_input').change(function() {

            $('.subbanner_fc').css('color', $(this).val());
        })

        $('#test_bgcolor_input').change(function() {

            $('.c_testimonial').css('backgroundColor', $(this).val());
        })

        $('#test_fontcolor_input').change(function() {

            $('.test_fc').css('color', $(this).val());
        })

        $('#about_bgcolor_input').change(function() {

            $('.c_about').css('backgroundColor', $(this).val());
        })

        $('#about_fontcolor_input').change(function() {

            $('.about_fc').css('color', $(this).val());
        })

        $('#cta_bgcolor_input').change(function() {

            $('.c_cta').css('backgroundColor', $(this).val());
        })

        $('#cta_fontcolor_input').change(function() {

            $('.cta_fc').css('color', $(this).val());
        })

        $('#head_logo_choose').click(function() {

            $('#head_logo_upload').click();

        })

        $("#head_logo_upload").change(function() {

            readURL(this, '#head_logo_display');
        });

        $('#about_image_choose').click(function() {

            $('#about_image_upload').click();

        })

        $("#about_image_upload").change(function() {

            readURL(this, '#about_image_display');
        });

        $('#banner_image_choose').click(function() {

            $('#banner_image_upload').click();

        })

        $("#banner_image_upload").change(function() {

            readURLBanner(this);
        });

    })


    function readURL(input, targetid) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(targetid).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLBanner(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.c_banner').css('background-image', "url(" + e.target.result + ")");
            }



            reader.readAsDataURL(input.files[0]);
        }
    }



</script>

</body>
</html>