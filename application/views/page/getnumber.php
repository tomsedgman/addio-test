
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="title"><h5>Dialling code</h5></div>
                    <div class="body">

                        <?= form_open('/page/searchnumbers') ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input value="<?= $code ?>" id="code" name="code" required class="form-control">
                                    <label for="code">Code</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group right">
                                    <button onclick="window.location.href='/page/configure'" type="button" class="btn btn-grey">Back</button>
                                    <button type="submit" class="btn btn-blue">Search</button>
                                </div>
                            </div>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>


            </div>
            <div class="col-sm-6">
                <?php if (count($numbers) > 0) : ?>
                <div class="card">
                    <div class="title"><h5>Available numbers</h5><p>Click a number to purchase</p></div>
                    <div class="body">
                        <div class="row">

                            <?php foreach ($numbers as $n) : ?>
                                <div class="col-sm-4">
                                    <div class="number_selection" onclick="window.location.href='/page/purchasenumber/<?= urlencode($n->phoneNumber) ?>'">
                                        <?= str_replace('+44', '0', $n->phoneNumber); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#name').keyup(function() {

            url = $('#name').val().toLowerCase().split(' ').join('-');

            $('#url').val(url);
            $('#url2').val(url);

        })

        url = $('#name').val().toLowerCase().split(' ').join('-');

        $('#url').val(url);
        $('#url2').val(url);

    })
</script>
