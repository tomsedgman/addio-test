
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <?= form_open('advertising/create'); ?>

                <div class="card">
                    <div class="title"><h5>Target page</h5></div>
                    <div class="body">

                        <div class="row">
                            <div class="col-sm-12">
                                <select id="page_key" name="page_key" class="form-control">
                                    <?php foreach ($pages as $p) : ?>
                                        <option value="<?= $p['key'] ?>"><?= $p['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <select id="package" name="package" class="form-control">
                                    <option>Starter</option>
                                    <option>Advanced</option>
                                    <option>Premium</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="card">
                    <div class="title"><h5>Advert</h5></div>
                    <div class="body">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input maxlength="30" id="headline1" name="headline1" required class="form-control">
                                    <label for="headline1">Headline 1</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input maxlength="30" id="headline2" name="headline2" required class="form-control">
                                    <label for="headline2">Headline 2</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input maxlength="80" id="description" name="description" required class="form-control">
                                    <label for="description">Description</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input maxlength="15" id="url1" name="url1" required class="form-control">
                                    <label for="url1">URL Extender 1</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input maxlength="15" id="url2" name="url2" required class="form-control">
                                    <label for="url2">URL Extender 2</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="card">
                    <div class="title"><h5>Keywords</h5></div>
                    <div class="body">


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <br/>
                                    <textarea style="border: 1px solid #ccc;" rows="6" id="keywords" name="keywords" required class="form-control"></textarea>
                                    <label style="margin-top: -20px;" for="keywords">(1 keyword per line)</label>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

                <div class="card">
                    <div class="title"><h5>Targeting</h5></div>
                    <div class="body">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label style="margin-top: -25px;" for="criteriaId">Location targeting</label>
                                    <select id="criteriaId" name="criteriaId" class="form-control">
                                        <?php foreach ($locations as $l) : ?>
                                            <option value="<?= $l['criteriaId'] ?>"><?= $l['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label style="margin-top: -25px;" for="radius">Radius</label>
                                    <select id="radius" name="radius" class="form-control">
                                        <option>5 miles</option>
                                        <option>10 miles</option>
                                        <option>25 miles</option>
                                        <option>50 miles</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button onclick="window.location.href='/advertising'" type="button" class="btn btn-grey">Back</button>
                            <button class="btn btn-blue">Add advert</button>
                        </div>


                    </div>
                </div>

                <?= form_close(); ?>

            </div>

            <div class="col-sm-6">

                <div class="card">
                    <div class="title"><h5>Advert preview</h5></div>
                    <div class="body">

                        <div class="row">
                            <div class="col-sm-12">
                                <div style="font-family: arial,sans-serif; color: #1a0dab; line-height: 18px; font-size: 18px; font-weight: normal; letter-spacing: 0px;" class="ad_h1" id="ad_h1"></div>
                            </div>
                            <div class="col-sm-12">
                                <div style="font-family: arial,sans-serif; color: #006621; line-height: 18px; font-size: 14px; font-weight: normal; letter-spacing: 0px;" class="ad_url" id="ad_url"></div>
                            </div>

                            <div class="col-sm-12">
                                <div style="font-family: arial,sans-serif; color: #545454; line-height: 18px; font-size: small; font-weight: normal; letter-spacing: 0px;" class="ad_desc" id="ad_desc"></div>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#headline1, #headline2').keyup(function() {

            $('#ad_h1').html($('#headline1').val() + ' | ' + $('#headline2').val());

        })

        $('#url1, #url2').keyup(function() {

            $('#ad_url').html('https://yourname.addio.co.uk/' + $('#url1').val() + '/' + $('#url2').val());

        })

        $('#description').keyup(function() {

            $('#ad_desc').html($('#description').val());

        })

    })
</script>
