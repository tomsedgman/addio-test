
<div id="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-9">
                                <h5>Advertising Spend</h5>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-4">
                                <strong>Page name</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Created</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Start date</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Budget</strong>
                            </div>
                            <div class="col-sm-2 right">
                                <strong>Balance</strong>
                            </div>
                        </div>
                        <hr/>
                        <?php foreach ($ads as $s) : ?>

                                <div class="pageline">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $s['name'] ?>
                                        </div>
                                        <div class="col-sm-2 right">
                                            <?= date('d M Y', strtotime($s['created'])) ?>
                                        </div>
                                        <div class="col-sm-2 right">
                                            <?= date('d M Y', strtotime($s['fromdate'])) ?>
                                        </div>
                                        <div class="col-sm-2 right">
                                            £<?= $s['budget'] ?>
                                        </div>
                                        <div class="col-sm-2 right">
                                            <?= $s['balance'] ?>
                                        </div>

                                    </div>
                                </div>

                                <hr/>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<div class="floating_btn" onclick="window.location.href='/advertising/add'">
    <i class="ion ion-plus"/>
</div>



<script>



</script>






<div class="hide" id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="card clickable">
                    <div class="title">
                        <h5 class="center">Google Text Ads</h5>
                    </div>
                    <div class="divider"></div>
                    <div class="body">

                        <div class="price_item">
                            1 Campaign
                        </div>
                        <div class="price_item">
                            1 Ad Group
                        </div>
                        <div class="price_item">
                            10 Keywords
                        </div>
                        <div class="price">
                            £395
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card clickable">
                    <div class="title">
                        <h5 class="center">Facebook Ads</h5>
                    </div>
                    <div class="divider"></div>
                    <div class="body">

                        <div class="price_item">
                            1 Advert
                        </div>
                        <div class="price_item">
                            Full design
                        </div>
                        <div class="price_item">
                            Advert copy
                        </div>
                        <div class="price">
                            £195
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card clickable">
                    <div class="title">
                        <h5 class="center">Instagram Ads</h5>
                    </div>
                    <div class="divider"></div>
                    <div class="body">

                        <div class="price_item">
                            1 Advert
                        </div>
                        <div class="price_item">
                            Full design
                        </div>
                        <div class="price_item">
                            Advert copy
                        </div>
                        <div class="price">
                            £245
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


