
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="title"><h5>Add spend</h5></div>
                    <div class="body">


                        <?= form_open('advertising/add'); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label style="margin-top: -25px;" for="page_key">Page</label>
                                    <select id="page_key" name="page_key" class="form-control">
                                        <?php foreach ($pages as $p) : ?>
                                            <option value="<?= $p['key'] ?>"><?= $p['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="date" name="date" type="date" required class="form-control" value="<?= date('Y-m-d') ?>">
                                    <label for="date">Start Date</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="package" class="form-control">
                                        <option value="Starter">Starter (£198 budget)</option>
                                        <option value="Advanced">Advanced (£281 budget)</option>
                                        <option value="Premium">Premium (£358 budget)</option>
                                    </select>
                                    <label style="margin-top: -25px;" for="package">Package</label>
                                </div>
                            </div>
                        </div>


                            <div class="form-group right">
                            <button onclick="window.location.href='/advertising'" type="button" class="btn btn-grey">Back</button>
                            <button type="submit" class="btn btn-blue">Add spend</button>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function() {

        $('#name').keyup(function() {

            url = $('#name').val().toLowerCase().split(' ').join('-');

            $('#url').val(url);
            $('#url2').val(url);

        })

    })
</script>
