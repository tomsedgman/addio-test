<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link href="/css/theme.css" rel="stylesheet">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <title>Addio</title>
</head>

<body>



<div id="sidebar" style="background-color: <?= $this->session->userdata('client_scolor') ?>">
    <div class="header">
        <?php
        if ($this->session->userdata('client_logo') !== null) {

            $logo = $this->session->userdata('client_logo');
        } else {
            $logo = LOGO;
        }
        ?>

        <img src="<?= $logo ?>"/>
    </div>

    <?php if ($this->session->userdata('user_type') == "Admin") : ?>
    <div class="site" onclick="window.location.href='/client'">
        Client: <strong><?= $this->session->userdata('client_name') ?></strong>
        <i class="ion ion-arrow-swap"></i>
    </div>
    <?php endif; ?>
    <?php if ($this->session->userdata('user_type') !== "User") : ?>
    <div class="site" onclick="window.location.href='/site'">
        Site: <strong><?= $this->session->userdata('site_name') ?></strong>
        <i class="ion ion-arrow-swap"></i>
    </div>
    <?php endif; ?>
    <?php if ($this->session->userdata('user_type') == "User") : ?>
        <div class="site">
            <strong><?= $this->session->userdata('site_name') ?></strong>

        </div>
    <?php endif; ?>

    <div class="menu">
        <div id="menu_analytics" class="item" onclick="redirectTo('analytics')">
            Analytics
        </div>
        <div id="menu_leads" class="item" onclick="redirectTo('leads')">
            Email Leads
        </div>
        <div id="menu_calls" class="item" onclick="redirectTo('calls')">
            Calls
        </div>

        <!--
        <div id="menu_advertising" class="item" onclick="redirectTo('advertising')">
            Advertising
        </div>
        -->


            <div id="menu_report" class="item" onclick="redirectTo('report')">
                Report
            </div>

        <div id="menu_page" class="item" onclick="redirectTo('page')">
            Page setup
        </div>
        <div id="menu_settings" class="item" onclick="redirectTo('settings')">
            Site settings
        </div>

    </div>
</div>



<div id="header" style="background-color: <?= $this->session->userdata('client_hcolor') ?>">
    <div class="row">
        <div class="col-sm-8">
            <div class="page_title">
                <?php if (isset($page_name)) { echo $page_name; } ?>
                <?php $i = 1; if (isset($header_title)) : foreach ($header_title as $ht) : ?>

                    <?php $ht['url'] ? print "<a href='" . $ht['url'] . "'>" : null; ?><?= $ht['title'] ?><?php $ht['url'] ? print "</a>" : null; ?>
                    <?php if ($i < count($header_title)) : ?>
                        <?= " <i class='menu_chevron ion ion-ios-arrow-forward'></i> " ?>
                    <?php endif; ?>
                    <?php $i++; endforeach; endif; ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div id="user_menu" class="user">
                Hi <?= $this->session->userdata('fname') ?> <i id="user_menu_arrow_down" class="ion ion-chevron-down"></i><i id="user_menu_arrow_up" class="ion ion-chevron-up hide"></i>
                <div id="user_menu_popup" class="popup hide">
                    <div class="menu_item" onclick="window.location.href='/account'">
                        Account Settings
                    </div>
                    <div class="menu_item" onclick="window.location.href='/login'">
                        Log out
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#<?= $menu_item ?>').addClass('active');

        $('#user_menu').click(function() {
            $('#user_menu_popup').toggleClass('hide');

            $('#user_menu_arrow_down').toggleClass('hide');
            $('#user_menu_arrow_up').toggleClass('hide');


        })
    })

    function redirectTo(page) {
        window.location.href="/" + page;
    }
</script>

</body>

</html>