<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Addio</title>
</head>

<body>

<div class="signin">

    <div class="col-sm-7 align-self-center signin-left">

        <div class="signinContent">

            <div class="row">
                <div class="col-sm-8 offset-sm-2">

                    <h2>Create your new password</h2>

                    <?= form_open('/reset/changepassword', 'id="reset_form" class="form form-horizontal"') ?>

                    <input type="hidden" name="key" value="<?= $key ?>"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input id="pwd" type="password" name="password" required class="form-control" required>
                                <label for="pwd">Password</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input id="pwd2" type="password" name="password2" required class="form-control" required>
                                <label for="pwd2">Confirm Password</label>
                            </div>
                        </div>
                    </div>

                    <div id="msgbox" class="row hide">
                        <div class="col-sm-12">
                            <p id="msg" class="warning small">

                            </p>
                        </div>
                    </div>

                    <div id="msgbox2" class="row hide">
                        <div class="col-sm-12">
                            <p id="msg2" class="warning small">

                            </p>
                        </div>
                    </div>

                    <div class="form-group right">
                        <button id="reset_btn" type="button" class="btn btn-blue">Next</button>
                    </div>

                    <?= form_close(); ?>

                    <div class="row">
                        <div class="col-sm-12">
                            <p class="center small">Remember now? <a href="/login">Login here</a></p>
                        </div>
                    </div>

                </div>


            </div>
        </div>

    </div>

    <div class="signin-right col-sm-5">



    </div>

</div>



<script>

    var pmErr = 0;
    var pwErr = 0;

    $(document).ready(function() {

        $('#pwd, #pwd2').keyup(function() {
            if ($('#pwd').val() === $('#pwd2').val()) {
                $('#msgbox').addClass('hide');
                $('#msg').html("");
                pmErr = 0;
            } else {
                $('#msgbox').removeClass('hide');
                $('#msg').html("Passwords do not match");
                pmErr = 1;
            }

            if ($('#pwd').val().length > 5) {
                $('#msgbox2').addClass('hide');
                $('#msg2').html("");
                pwErr = 0;
            } else {
                $('#msgbox2').removeClass('hide');
                $('#msg2').html("Password must be longer than 5 characters");
                pwErr = 1;
            }

        })



        $('#reset_btn').click(function() {


            if (pwErr === 0 && pmErr === 0) {
                $('#reset_form').submit();
            }



        })
    })
</script>

</body>
</html>