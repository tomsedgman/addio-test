
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="title"><h5>Account details</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['details'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="confirm small">
                                    Your details have been updated!
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?= form_open('account/update'); ?>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <input id="fname" value="<?= $user['fname'] ?>" type="text" name="fname" required class="form-control">
                                <label for="fname">First name</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input id="lname" value="<?= $user['lname'] ?>" type="text" name="lname" required class="form-control">
                                    <label for="lname">Last name</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="email" value="<?= $user['email'] ?>" type="text" name="email" required class="form-control">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="phone" value="<?= $user['phone'] ?>" name="phone" required class="form-control">
                                    <label for="phone">Phone</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button type="submit" class="btn btn-blue">Update</button>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>



                <div class="card">
                    <div class="title"><h5>Change password</h5></div>
                    <div class="body">

                        <?php if (isset($_REQUEST['pwd'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="confirm small">
                                    Your password has been updated!
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?= form_open('account/updatepassword', 'id="reset_form"'); ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="password" type="password" name="password" required class="form-control">
                                    <label for="password">New password</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button id="reset_btn" type="button" class="btn btn-blue">Update</button>
                        </div>

                        <div id="msgbox" class="row hide">
                            <div class="col-sm-12">
                                <p id="msg" class="warning small">

                                </p>
                            </div>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>

    var pwErr = 0;

    $(document).ready(function() {

        $('#password').keyup(function() {


            if ($('#password').val().length > 5) {
                $('#msgbox').addClass('hide');
                $('#msg').html("");
                pwErr = 0;
            } else {
                $('#msgbox').removeClass('hide');
                $('#msg').html("Password must be longer than 5 characters");
                pwErr = 1;
            }

        })



        $('#reset_btn').click(function() {

            if ($('#password').val().length < 6) {
                $('#msgbox').removeClass('hide');
                $('#msg').html("You must enter a password");
                pwErr = 1;
            }
            if (pwErr === 0) {
                $('#reset_form').submit();
            }



        })
    })
</script>
