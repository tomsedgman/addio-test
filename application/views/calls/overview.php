
<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5><?= $page_name ?></h5>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group right">
                                    <input style="width: 250px; text-align: right; float: right;" class="form-control" name="daterange" id="daterange"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-2">
                                <strong>Date</strong>
                            </div>
                            <div class="col-sm-2">
                                <strong>Start time</strong>
                            </div>
                            <div class="col-sm-2">
                                <strong>End time</strong>
                            </div>
                            <div class="col-sm-3">
                                <strong>Caller Number</strong>
                            </div>
                            <div class="col-sm-3">
                                <strong>Page</strong>
                            </div>

                        </div>
                        <hr/>
                        <?php foreach ($calls as $cc) : foreach ($cc as $c) : ?>

                            <div class="pageline clickable">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <?= date('d M Y', strtotime($c['start'])) ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?= date('H:i:s', strtotime($c['start'])) ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?= date('H:i:s', strtotime($c['end'])) ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?= $c['number'] ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?= $c['pagename'] ?>
                                    </div>
                                </div>
                            </div>

                        <hr/>
                        <?php endforeach; endforeach; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<script>

    $(document).ready(function() {




    })


    function cb(start, end) {

        //$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('MMMM D, YYYY'));

        console.log(("<?= date('Y-m-d', strtotime($start)) ?>"));


    }


    $('#daterange').daterangepicker({
        autoApply: false,
        startDate: moment("<?= date('Y-m-d', strtotime($start)) ?>", "YYYY-MM-DD"),
        endDate: moment("<?= date('Y-m-d', strtotime($end)) ?>", "YYYY-MM-DD"),
        locale: {
            format: 'DD/MM/YYYY'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    //cb(start, end);


    $('#daterange').on('apply.daterangepicker', function(ev, picker) {
        var s = picker.startDate.format('YYYY-MM-DD');
        var e = picker.endDate.format('YYYY-MM-DD');

        window.location.href='/calls/view/' + s + '/' + e;
        //console.log(s);
    })



</script>