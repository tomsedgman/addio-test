<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Website Landing Page</title>
</head>

<body>
<div class="c_header" style="border: none; background-color: <?= $section_header['bgcolor'] ?>">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="logo_preview">
                    <?php if (isset($section_header['logo'])) : ?>
                        <img src="<?= $section_header['logo'] ?>"/>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-5">
                <?php if (isset($section_header['link3'])) : ?>
                    <div class="menu_preview" style="color: <?= $section_header['fontcolor'] ?>">
                        <a style="color: <?= $section_header['fontcolor'] ?>" href="<?= $section_header['dest3'] ?>"><?= $section_header['link3'] ?></a>
                    </div>
                <?php endif; ?>
                <?php if (isset($section_header['link2'])) : ?>
                    <div class="menu_preview" style="color: <?= $section_header['fontcolor'] ?>">
                        <a style="color: <?= $section_header['fontcolor'] ?>" href="<?= $section_header['dest2'] ?>"><?= $section_header['link2'] ?></a>
                    </div>
                <?php endif; ?>
                <?php if (isset($section_header['link1'])) : ?>
                    <div class="menu_preview" style="color: <?= $section_header['fontcolor'] ?>">
                        <a style="color: <?= $section_header['fontcolor'] ?>" href="<?= $section_header['dest1'] ?>"><?= $section_header['link1'] ?></a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-sm-3">
                <div class="phone" style="color: <?= $section_header['phonecolor'] ?>">
                    0800 093 9933
                </div>
            </div>
        </div>
    </div>
</div>

<div class="c_preheader" style="border: none; background-color: <?= $section_preheader['bgcolor'] ?>">
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 style="color: <?= $section_preheader['fontcolor'] ?>"><?= $section_preheader['headertext1'] ?></h1>
            <h3 style="color: <?= $section_preheader['fontcolor'] ?>"><?= $section_preheader['headertext2'] ?></h3>
        </div>
    </div>
    </div>
</div>

<div class="c_banner" style="border: none; background-size: cover; background-position: center; background-image: URL('<?= $section_banner['bgimage'] ?>'); background-color: <?= $section_banner['bgcolor'] ?>">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <?php if ($section_banner['boxbehindtext'] === 'Yes') : ?>
                <div class="bannertextarea">
                    <?php else: ?>
                    <div class="bannertextarea_blank">
                    <?php endif; ?>
                <div class="bannertext" style="color: <?= $section_banner['fontcolor'] ?>">
                    <p style="color: <?= $section_banner['fontcolor'] ?>">
                        <?= $section_banner['bannertext'] ?>
                    </p>

                    <div class="row">
                        <?php if ($section_banner['bp1_text'] !== '') : ?>
                        <div class="col-sm-12">
                            <div class="bulletpoint">
                            <div style="color: <?= $section_banner['fontcolor'] ?>"  class="bp_icon">
                                <i class="ion ion-checkmark"></i>
                            </div>
                            <h3><?= $section_banner['bp1_title'] ?></h3>
                            <p><?= $section_banner['bp1_text'] ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if ($section_banner['bp2_text'] !== '') : ?>
                        <div class="col-sm-12">
                            <div class="bulletpoint">
                                <div style="color: <?= $section_banner['fontcolor'] ?>"  class="bp_icon">
                                    <i class="ion ion-checkmark"></i>
                                </div>
                                <h3><?= $section_banner['bp2_title'] ?></h3>
                                <p><?= $section_banner['bp2_text'] ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if ($section_banner['bp3_text'] !== '') : ?>
                        <div class="col-sm-12">
                            <div class="bulletpoint">
                                <div style="color: <?= $section_banner['fontcolor'] ?>"  class="bp_icon">
                                    <i class="ion ion-checkmark"></i>
                                </div>
                                <h3><?= $section_banner['bp3_title'] ?></h3>
                                <p><?= $section_banner['bp3_text'] ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>

                </div>

                </div>

            </div>
            <div class="col-sm-6">

                <div class="banner_form" style="border-color: <?= $section_banner['form_border'] ?>">

                    <div class="banner_form_header" style="background-color: <?= $section_banner['form_header'] ?>;">

                        <h4>
                            <?= $section_banner['form_headline'] ?>
                        </h4>
                        <h5>
                            <?= $section_banner['form_headline2'] ?>
                        </h5>
                    </div>

                    <div class="banner_form_body">

                        <div class="title">
                            <?= $section_banner['form_pretext'] ?>
                        </div>

                    <?= form_open(); ?>

                    <div class="row">
                        <div class="col-sm-12">
                            <label>Name</label>
                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Email</label>
                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Company</label>
                            <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Phone number</label>
                                <input style="height: 40px; padding-left: 10px;" class="form-control" name="name" placeholder=""/>
                            </div>
                        </div>


                    <br/>
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn" style="background-color: <?= $section_banner['form_header'] ?>;"><?= $section_banner['form_button'] ?></button>
                        </div>
                    </div>

                    <?= form_close(); ?>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

    <div class="c_subbanner" style="border: none; background-color: <?= $section_subbanner['bgcolor'] ?>">
        <div class="row">
            <div class="col-sm-12">
                <h1 style="color: <?= $section_subbanner['fontcolor'] ?>"><?= $section_subbanner['bannertext'] ?></h1>
            </div>
        </div>
    </div>