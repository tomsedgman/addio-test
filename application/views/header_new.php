<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


    <link href="/css/theme.css" rel="stylesheet">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <title>Addio</title>
</head>

<body>

<?php
$this->db->where('client_key', $this->session->userdata('client_key'));
$this->db->join('page', 'page.site_key = site.key', 'left');
$this->db->select('site.key as key, site.name, site.main_url, COUNT(page.name) as pages');
$this->db->group_by('key');
$sites                    =       $this->db->get('site')->result_array();

$this->db->select('client.key as key, client.name, client.url');
$clients                  =       $this->db->get('client')->result_array();

$this->db->where('status', 'Active');
$this->db->where('page.site_key', $this->session->userdata('site_key'));
$this->db->select('page.key as key, page.name');
$pages                    =       $this->db->get('page')->result_array();
?>


<div id="header_new" style="background-color: <?= $this->session->userdata('client_hcolor') ?>">
    <div class="row">
        <div class="col-sm-8">

            <?php $currpage = substr($_SERVER['REQUEST_URI'], 0, 9) ?>


            <?php if ($this->session->userdata('user_type') == "Admin") : ?>
            <div class="breadcrumb">
                <i id="" class="b_menu_arrow_down ion ion-chevron-down"></i><i id="" class="b_menu_arrow_up ion ion-chevron-up hide"></i>
                <div class="b-title">Client<br/></div>
                <div class="b-name"><?= $this->session->userdata('client_name') ?></div>
                <div class="b-popup hide">
                    <?php foreach ($clients as $c) : ?>
                        <div class="menu_item">
                            <div class="menu_item_name" onclick="window.location.href='/client/select/<?= $c['key'] ?>'">
                                <?= $c['name'] ?>
                            </div>
                            <div class="menu_item_settings">
                                <i onclick="window.location.href='/client/select/<?= $c['key'] ?>/client_settings'" class="menu_settings ion ion-ios-gear"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="menu_item" onclick="window.location.href='/client/add/'">
                        <span class="green">+ Add new client</span>
                    </div>
                </div>

            </div>
            <?php endif; ?>

            <?php if ($currpage !==  '/client/s') : ?>


            <?php if ($this->session->userdata('user_type') !== "User") : ?>
            <div class="breadcrumb">
                <i id="" class="b_menu_arrow_down ion ion-chevron-down"></i><i id="" class="b_menu_arrow_up ion ion-chevron-up hide"></i>
                <div class="b-title">Site<br/></div>
                <div class="b-name"><?= $this->session->userdata('site_name') ?></div>
                <div class="b-popup hide">
                    <?php foreach ($sites as $s) : ?>
                    <div class="menu_item">
                        <div class="menu_item_name" onclick="window.location.href='/site/select/<?= $s['key'] ?>'">
                            <?= $s['name'] ?>
                        </div>
                        <div class="menu_item_settings">
                            <i onclick="window.location.href='/site/select/<?= $s['key'] ?>/settings'" class="menu_settings ion ion-ios-gear"></i>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <div class="menu_item" onclick="window.location.href='/site/add/'">
                        <span class="green">+ Add new site</span>
                    </div>
                </div>
            </div>
            <?php endif; ?>


            <?php if ($currpage !==  '/settings' && $currpage !== '/page/add') : ?>
            <div class="breadcrumb">
                <i id="" class="b_menu_arrow_down ion ion-chevron-down"></i><i id="" class="b_menu_arrow_up ion ion-chevron-up hide"></i>
                <div class="b-title">Page<br/></div>
                <div class="b-name"><?= $this->session->userdata('page_name') ?></div>
                <div class="b-popup hide">
                    <?php if (count($pages) > 1) : ?>
                        <div class="menu_item" onclick="window.location.href='/page/select/all'">
                            All pages
                        </div>
                    <?php endif; ?>
                    <?php foreach ($pages as $p) : ?>
                        <div class="menu_item" onclick="window.location.href='/page/select/<?= $p['key'] ?>'">
                            <?= $p['name'] ?>
                        </div>
                    <?php endforeach; ?>
                    <div class="menu_item" onclick="window.location.href='/page/add/'">
                        <span class="green">+ Add new page</span>
                    </div>
                </div>
            </div>
            <div class="breadcrumb">
                <i id="" class="b_menu_arrow_down ion ion-chevron-down"></i><i id="" class="b_menu_arrow_up ion ion-chevron-up hide"></i>
                <div class="b-title">View<br/></div>
                <div class="b-name"><?= $pageview ?></div>
                <div class="b-popup hide">
                    <div class="menu_item" onclick="window.location.href='/analytics'">
                        Analytics
                    </div>
                    <div class="menu_item" onclick="window.location.href='/leads/view'">
                        Email Leads
                    </div>
                    <div class="menu_item" onclick="window.location.href='/calls/view'">
                        Phone Leads
                    </div>
                    <?php if ($this->session->userdata('page_key') !== 'all') : ?>
                    <div class="menu_item" onclick="window.location.href='/report'">
                        Report
                    </div>
                    <div class="menu_item" onclick="window.location.href='/page'">
                        Page setup
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php endif; ?>

        </div>
        <div class="col-sm-4">
            <div id="user_menu" class="user">
                Hi <?= $this->session->userdata('fname') ?> <i id="user_menu_arrow_down" class="ion ion-chevron-down"></i><i id="user_menu_arrow_up" class="ion ion-chevron-up hide"></i>
                <div id="user_menu_popup" class="popup hide">
                    <div class="menu_item" onclick="window.location.href='/account'">
                        Account Settings
                    </div>
                    <div class="menu_item" onclick="window.location.href='/login'">
                        Log out
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {


        $('#user_menu').click(function() {
            $('#user_menu_popup').toggleClass('hide');

            $('#user_menu_arrow_down').toggleClass('hide');
            $('#user_menu_arrow_up').toggleClass('hide');


        })

        $('.breadcrumb').click(function() {

            if ($(this).find('.b-popup').hasClass('hide')){

                $('.b-popup').addClass('hide');
                $('.b_menu_arrow_down').removeClass('hide');
                $('.b_menu_arrow_up').addClass('hide');
                $(this).find('.b-popup').removeClass('hide');
                $(this).find('.b_menu_arrow_down').toggleClass('hide');
                $(this).find('.b_menu_arrow_up').toggleClass('hide');

            } else {
                $(this).find('.b-popup').addClass('hide');
                $(this).find('.b_menu_arrow_down').toggleClass('hide');
                $(this).find('.b_menu_arrow_up').toggleClass('hide');
            }


        })

    })

    function redirectTo(page) {
        window.location.href="/" + page;
    }
</script>

</body>

</html>