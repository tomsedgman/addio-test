

<div id="content" class="full">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">

                <div id="" class="card">
                    <div class="title">
                        <h5>Add a new site</h5>
                    </div>
                    <div class="body">

                        <?php if (isset($_REQUEST['err'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="warning small">
                                        The website already exists
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?= form_open('/site/addnew', 'class="form form-horizontal"') ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="name" type="text" name="name" class="form-control" required>
                                    <label for="name">Give your site a name</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="url" type="text" name="url" class="form-control" required>
                                    <label for="url">Main website URL</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="privacy_policy" type="text" name="privacy_policy" class="form-control" required>
                                    <label for="privacy_policy">Privacy Policy</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="cookie_policy" type="text" name="cookie_policy" class="form-control" required>
                                    <label for="cookie_policy">Cookie Policy</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="address" type="text" name="address" class="form-control" required>
                                    <label for="address">Address</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button onclick="window.location.href=document.referrer" type="button" class="btn btn-grey">Cancel</button>
                            <button class="btn btn-blue">Next</button>
                        </div>

                        <?= form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
