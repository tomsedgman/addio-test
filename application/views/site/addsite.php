<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Product+Sans:100,200,300,400,500,600' rel='stylesheet' type='text/css'>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="/css/theme.css" rel="stylesheet">

    <title>Addio</title>
</head>

<body>

<div id="content" class="wide">
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-sm-4">

                <div id="login" class="card">
                    <div class="title">
                        <h5>Add a website</h5>
                        <p style="font-weight: 300;">
                            In order to get started enter the details of your website below.
                        </p>
                    </div>
                    <div class="body">

                        <?php if (isset($_REQUEST['err'])) : ?>
                            <div id="msgbox1" class="row">
                                <div class="col-sm-12">
                                    <p id="msg1" class="warning small">
                                        The website already exists
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?= form_open('/site/addnew', 'class="form form-horizontal"') ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="name" type="text" name="name" required class="form-control" required>
                                    <label for="name">Give your site a name</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="url" type="text" name="url" required class="form-control" required>
                                    <label for="url">Main website URL</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="privacy_policy" type="text" name="privacy_policy" class="form-control" required>
                                    <label for="privacy_policy">Privacy Policy</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="cookie_policy" type="text" name="cookie_policy" class="form-control" required>
                                    <label for="cookie_policy">Cookie Policy</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="address" type="text" name="address" class="form-control" required>
                                    <label for="address">Address</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group right">
                            <button type="submit" class="btn btn-blue">Next</button>
                        </div>

                        <?= form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>