<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 06/06/2017
 * Time: 11:40
 */

namespace TrakiCharts;

class Api {

	/**
	 * @var
	 */
	public $server;

	/**
	 * @var
	 */
	public $port;

	/**
	 * @var
	 */
	public $protocol;

	/**
	 * Api constructor.
	 *
	 * @param $endpoint
	 * @param $port
	 */
	public function __construct( $protocol, $server, $port ) {
		$this->protocol = $protocol;
		$this->server   = $server;
		$this->port     = $port;
	}

	/**
	 * @param array $requestData
	 *
	 * @return string $svg
	 */
	public function getSVG( array $requestData ) {

		// make request
		$svg = $this->makeRequest( $requestData );

		// return response (should be an svg)
		return (string) $svg;
	}

	/**
	 * @param array $requestData
	 *
	 * @return string $svg
	 */
	public function makeRequest( array $requestData ) {

		// build URI
		$uri = $this->protocol . '://' . $this->server . ':' . $this->port;

		// encode JSON
		$data_string = json_encode( array( 'chart_data' => $requestData ) );

		// make request
		try {
			$ch = curl_init( $uri );
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_string );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen( $data_string )
				)
			);
			$result = curl_exec( $ch );
			sleep(1);
		} catch ( \ErrorException $e ) {
			echo $e;
		}

		// return result
		return (string) $result;
	}
}
