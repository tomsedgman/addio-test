<?php
// Create file application/core/MY_Controller.php
class Auth_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
	    if ( ! $this->session->userdata('loggedin'))
		{
			// Allow some methods?
			$allowed = array(
				'login'
			);
			if (!in_array($this->router->fetch_method(), $allowed))
			{
				$redirect = $_SERVER['REQUEST_URI'];
				redirect('/login?redirect=' . $redirect);
			}



		} else if ($this->router->fetch_method() !== 'addsite' && $this->router->fetch_method() !== 'addnew') {

            $sites = $this->db->get('site')->result_array();
            if (count($sites) == 0) {

                redirect('/site/addsite');

            }

        }

        if ($this->router->fetch_class() == 'client') {

	        if ($this->session->userdata('user_type') !== 'Admin') {

	            redirect('analytics');

            }

        }

        if ($this->router->fetch_class() == 'site') {

            //echo $this->session->userdata('user_type');
            if ($this->session->userdata('user_type') == 'User') {

                redirect('analytics');

            }

        }


       // echo $this->router->fetch_method();
    }
}
?>