<?php
/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//namespace Google\AdsApi\Examples\AdWords\v201710\AccountManagement;

//require __DIR__ . '/../../../../vendor/autoload.php';
require APPPATH . 'vendor/autoload.php';

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201710\cm\Operator;
use Google\AdsApi\AdWords\v201710\mcm\ManagedCustomer;
use Google\AdsApi\AdWords\v201710\mcm\ManagedCustomerOperation;
use Google\AdsApi\AdWords\v201710\mcm\ManagedCustomerService;
use Google\AdsApi\Common\OAuth2TokenBuilder;


use Google\AdsApi\AdWords\v201710\cm\AdvertisingChannelType;
use Google\AdsApi\AdWords\v201710\cm\BiddingStrategyConfiguration;
use Google\AdsApi\AdWords\v201710\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201710\cm\Budget;
use Google\AdsApi\AdWords\v201710\cm\BudgetBudgetDeliveryMethod;
use Google\AdsApi\AdWords\v201710\cm\BudgetOperation;
use Google\AdsApi\AdWords\v201710\cm\BudgetService;
use Google\AdsApi\AdWords\v201710\cm\Campaign;
use Google\AdsApi\AdWords\v201710\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201710\cm\CampaignService;
use Google\AdsApi\AdWords\v201710\cm\CampaignStatus;
use Google\AdsApi\AdWords\v201710\cm\FrequencyCap;
use Google\AdsApi\AdWords\v201710\cm\GeoTargetTypeSetting;
use Google\AdsApi\AdWords\v201710\cm\GeoTargetTypeSettingNegativeGeoTargetType;
use Google\AdsApi\AdWords\v201710\cm\GeoTargetTypeSettingPositiveGeoTargetType;
use Google\AdsApi\AdWords\v201710\cm\Level;
use Google\AdsApi\AdWords\v201710\cm\ManualCpcBiddingScheme;
use Google\AdsApi\AdWords\v201710\cm\Money;
use Google\AdsApi\AdWords\v201710\cm\NetworkSetting;
use Google\AdsApi\AdWords\v201710\cm\TimeUnit;

use Google\AdsApi\AdWords\v201710\cm\AdGroup;
use Google\AdsApi\AdWords\v201710\cm\AdGroupAdRotationMode;
use Google\AdsApi\AdWords\v201710\cm\AdGroupOperation;
use Google\AdsApi\AdWords\v201710\cm\AdGroupService;
use Google\AdsApi\AdWords\v201710\cm\AdGroupStatus;
use Google\AdsApi\AdWords\v201710\cm\AdRotationMode;
use Google\AdsApi\AdWords\v201710\cm\CpcBid;
use Google\AdsApi\AdWords\v201710\cm\CriterionTypeGroup;
use Google\AdsApi\AdWords\v201710\cm\TargetingSetting;
use Google\AdsApi\AdWords\v201710\cm\TargetingSettingDetail;
use Google\AdsApi\AdWords\v201710\cm\AdGroupCriterionOperation;
use Google\AdsApi\AdWords\v201710\cm\AdGroupCriterionService;
use Google\AdsApi\AdWords\v201710\cm\BiddableAdGroupCriterion;
use Google\AdsApi\AdWords\v201710\cm\Keyword;
use Google\AdsApi\AdWords\v201710\cm\KeywordMatchType;
use Google\AdsApi\AdWords\v201710\cm\NegativeAdGroupCriterion;
use Google\AdsApi\AdWords\v201710\cm\UrlList;
use Google\AdsApi\AdWords\v201710\cm\UserStatus;
use Google\AdsApi\AdWords\v201710\cm\AdGroupAd;
use Google\AdsApi\AdWords\v201710\cm\AdGroupAdOperation;
use Google\AdsApi\AdWords\v201710\cm\AdGroupAdService;
use Google\AdsApi\AdWords\v201710\cm\AdGroupAdStatus;
use Google\AdsApi\AdWords\v201710\cm\ExpandedTextAd;

use Google\AdsApi\AdWords\v201710\cm\CampaignCriterion;
use Google\AdsApi\AdWords\v201710\cm\CampaignCriterionOperation;
use Google\AdsApi\AdWords\v201710\cm\CampaignCriterionService;
use Google\AdsApi\AdWords\v201710\cm\ConstantOperand;
use Google\AdsApi\AdWords\v201710\cm\ConstantOperandConstantType;
use Google\AdsApi\AdWords\v201710\cm\ConstantOperandUnit;
use Google\AdsApi\AdWords\v201710\cm\FunctionOperator;
use Google\AdsApi\AdWords\v201710\cm\Language;
use Google\AdsApi\AdWords\v201710\cm\Location;
use Google\AdsApi\AdWords\v201710\cm\LocationExtensionOperand;
use Google\AdsApi\AdWords\v201710\cm\LocationGroups;
use Google\AdsApi\AdWords\v201710\cm\MatchingFunction;
use Google\AdsApi\AdWords\v201710\cm\NegativeCampaignCriterion;


use Google\AdsApi\AdWords\Reporting\v201710\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201710\ReportDownloader;
use Google\AdsApi\AdWords\ReportSettingsBuilder;



class Googleads
{

    const PAGE_LIMIT = 500;

    public function createCamapaign($name, $daily_budget) {


        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();
        $adWordsServices = new AdWordsServices();


        $budgetService = $adWordsServices->get($session, BudgetService::class);

        // Create the shared budget (required).
        $budget = new Budget();
        $budget->setName("Budget: " . $name . ' (' . date('Ymd') . ')');
        $money = new Money();
        $money->setMicroAmount($daily_budget . '000000');
        $budget->setAmount($money);
        $budget->setDeliveryMethod(BudgetBudgetDeliveryMethod::STANDARD);

        $operations = [];

        // Create a budget operation.
        $operation = new BudgetOperation();
        $operation->setOperand($budget);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;

        // Create the budget on the server.
        $result = $budgetService->mutate($operations);
        $budget = $result->getValue()[0];

        $campaignService = $adWordsServices->get($session, CampaignService::class);

        $operations = [];

        // Create a campaign with required and optional settings.
        $campaign = new Campaign();
        $campaign->setName("Campaign: " . $name);
        $campaign->setAdvertisingChannelType(AdvertisingChannelType::SEARCH);

        // Set shared budget (required).
        $campaign->setBudget(new Budget());
        $campaign->getBudget()->setBudgetId($budget->getBudgetId());

        // Set bidding strategy (required).
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(
            BiddingStrategyType::MANUAL_CPC
        );

        // You can optionally provide a bidding scheme in place of the type.
        $biddingScheme = new ManualCpcBiddingScheme();
        $biddingScheme->setEnhancedCpcEnabled(false);
        $biddingStrategyConfiguration->setBiddingScheme($biddingScheme);

        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        // Set network targeting (optional).
        $networkSetting = new NetworkSetting();
        $networkSetting->setTargetGoogleSearch(true);
        $networkSetting->setTargetSearchNetwork(false);
        $networkSetting->setTargetContentNetwork(false);
        $campaign->setNetworkSetting($networkSetting);

        // Set additional settings (optional).
        // Recommendation: Set the campaign to PAUSED when creating it to stop
        // the ads from immediately serving. Set to ENABLED once you've added
        // targeting and the ads are ready to serve.
        $campaign->setStatus(CampaignStatus::PAUSED);
        $campaign->setStartDate(date('Ymd', strtotime('+1 day')));

        // Set frequency cap (optional).
        $frequencyCap = new FrequencyCap();
        $frequencyCap->setImpressions(5);
        $frequencyCap->setTimeUnit(TimeUnit::DAY);
        $frequencyCap->setLevel(Level::ADGROUP);
        $campaign->setFrequencyCap($frequencyCap);

        // Set advanced location targeting settings (optional).
        $geoTargetTypeSetting = new GeoTargetTypeSetting();
        $geoTargetTypeSetting->setPositiveGeoTargetType(
            GeoTargetTypeSettingPositiveGeoTargetType::DONT_CARE
        );
        $geoTargetTypeSetting->setNegativeGeoTargetType(
            GeoTargetTypeSettingNegativeGeoTargetType::DONT_CARE
        );
        $campaign->setSettings([$geoTargetTypeSetting]);

        // Create a campaign operation and add it to the operations list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;



        // Create the campaigns on the server and print out some information for
        // each created campaign.
        $result = $campaignService->mutate($operations);

        return $result->getValue()[0]->getId();

    }

    public function createAdGroup($name, $campaignId, $bidAmount) {

        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();
        $adWordsServices = new AdWordsServices();


        $adGroupService = $adWordsServices->get($session, AdGroupService::class);

        $operations = [];

        // Create an ad group with required and optional settings.
        $adGroup = new AdGroup();
        $adGroup->setCampaignId($campaignId);
        $adGroup->setName("Group: " . $name);

        // Set bids (required).
        $bid = new CpcBid();
        $money = new Money();
        $money->setMicroAmount(($bidAmount * 10) . '00000');
        $bid->setBid($money);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBids([$bid]);
        $adGroup->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        // Set additional settings (optional).
        $adGroup->setStatus(AdGroupStatus::ENABLED);

        // Targeting restriction settings. Depending on the criterionTypeGroup
        // value, most TargetingSettingDetail only affect Display campaigns.
        // However, the USER_INTEREST_AND_LIST value works for RLSA campaigns -
        // Search campaigns targeting using a remarketing list.
        $targetingSetting = new TargetingSetting();
        $details = [];
        // Restricting to serve ads that match your ad group placements.
        // This is equivalent to choosing "Target and bid" in the UI.
        $details[] = new TargetingSettingDetail(CriterionTypeGroup::PLACEMENT, false);
        // Using your ad group verticals only for bidding. This is equivalent
        // to choosing "Bid only" in the UI.
        $details[] = new TargetingSettingDetail(CriterionTypeGroup::VERTICAL, true);
        $targetingSetting->setDetails($details);
        $adGroup->setSettings([$targetingSetting]);

        // Set the rotation mode.
        $rotationMode = new AdGroupAdRotationMode(AdRotationMode::OPTIMIZE);
        $adGroup->setAdGroupAdRotationMode($rotationMode);

        // Create an ad group operation and add it to the operations list.
        $operation = new AdGroupOperation();
        $operation->setOperand($adGroup);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;


        // Create the ad groups on the server and print out some information for
        // each created ad group.
        $result = $adGroupService->mutate($operations);

        return $result->getValue()[0]->getId();

    }

    public function createKeywords($adGroupId, $bidAmount, $keyword_phrase) {


        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();
        $adWordsServices = new AdWordsServices();


        $adGroupCriterionService = $adWordsServices->get($session, AdGroupCriterionService::class);

        $operations = [];

        // Create the first keyword criterion.
        $keyword = new Keyword();
        $keyword->setText($keyword_phrase);
        $keyword->setMatchType(KeywordMatchType::PHRASE);

        // Create biddable ad group criterion.
        $adGroupCriterion = new BiddableAdGroupCriterion();
        $adGroupCriterion->setAdGroupId($adGroupId);
        $adGroupCriterion->setCriterion($keyword);


        // Set bids (optional).
        $bid = new CpcBid();
        $money = new Money();
        $money->setMicroAmount(($bidAmount * 10) . '00000');
        $bid->setBid($money);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBids([$bid]);
        $adGroupCriterion->setBiddingStrategyConfiguration(
            $biddingStrategyConfiguration
        );

        // Create an ad group criterion operation and add it to the list.
        $operation = new AdGroupCriterionOperation();
        $operation->setOperand($adGroupCriterion);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;


        // Create the ad group criteria on the server and print out some information
        // for each created ad group criterion.
        $result = $adGroupCriterionService->mutate($operations);

        return "success";
    }

    public function createAdvert($adGroupId, $headlinePart1, $headlinePart2, $description, $finalUrl, $path1, $path2) {

        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();
        $adWordsServices = new AdWordsServices();


        $adGroupAdService = $adWordsServices->get($session, AdGroupAdService::class);

        $operations = [];
        // Create an expanded text ad.
        $expandedTextAd = new ExpandedTextAd();
        $expandedTextAd->setHeadlinePart1($headlinePart1);
        $expandedTextAd->setHeadlinePart2($headlinePart2);
        $expandedTextAd->setDescription($description);
        $expandedTextAd->setFinalUrls([$finalUrl]);
        $expandedTextAd->setPath1($path1);
        $expandedTextAd->setPath2($path2);

        // Create ad group ad.
        $adGroupAd = new AdGroupAd();
        $adGroupAd->setAdGroupId($adGroupId);
        $adGroupAd->setAd($expandedTextAd);

        // Optional: Set additional settings.
        $adGroupAd->setStatus(AdGroupAdStatus::PAUSED);

        // Create ad group ad operation and add it to the list.
        $operation = new AdGroupAdOperation();
        $operation->setOperand($adGroupAd);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;



        // Add expanded text ads on the server.
        $result = $adGroupAdService->mutate($operations);

        return $result->getValue()[0]->getAd()->getId();
    }

    public function setTargetting($campaignId, $criteriaId) {


        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();
        $adWordsServices = new AdWordsServices();

        $campaignCriterionService = $adWordsServices->get($session, CampaignCriterionService::class);

        $campaignCriteria = [];

        $mylocation = new Location();
        $mylocation->setId($criteriaId);
        $campaignCriteria[] = new CampaignCriterion($campaignId, null, $mylocation);

        $operations = [];
        foreach ($campaignCriteria as $campaignCriterion) {
            $operation = new CampaignCriterionOperation();
            $operation->setOperator(Operator::ADD);
            $operation->setOperand($campaignCriterion);
            $operations[] = $operation;
        }

        $result = $campaignCriterionService->mutate($operations);

        return $result->getValue();

    }

    public function getCampaigns() {

        $reportFormat = DownloadFormat::CSV;

        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();
        //$adWordsServices = new AdWordsServices();

        // Create report query to get the data for last 7 days.
        $reportQuery = 'SELECT CampaignId, AdGroupId, Id, Criteria, CriteriaType, '
            . 'Impressions, Clicks, Cost FROM CRITERIA_PERFORMANCE_REPORT '
            . 'WHERE Status IN [ENABLED, PAUSED] DURING LAST_7_DAYS';

        // Download report as a string.
        $reportDownloader = new ReportDownloader($session);
        // Optional: If you need to adjust report settings just for this one
        // request, you can create and supply the settings override here. Otherwise,
        // default values from the configuration file (adsapi_php.ini) are used.
        $reportSettingsOverride = (new ReportSettingsBuilder())->includeZeroImpressions(false)->build();
        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            $reportQuery,
            $reportFormat,
            $reportSettingsOverride
        );
        print "Report was downloaded and printed below:\n";
        print $reportDownloadResult->getAsString();

    }

}

