<?php

include_once(APPPATH . 'vendor/Twilio/autoload.php');
use Twilio\Rest\Client;
use Twilio\Twiml;

class Twillio {


    function index() {

        $sid = "ACXXXXXX"; // Your Account SID from www.twilio.com/console
        $token = "YYYYYY"; // Your Auth Token from www.twilio.com/console

        $client = new Client($sid, $token);



    }

    function searchNumbers($code) {

        $sid = "ACe1dddb02fba951dacb756630e4379311"; // Your Account SID from www.twilio.com/console
        $token = "a84d297005113aa2bdee0d244c94d1c0"; // Your Auth Token from www.twilio.com/console

        $client = new Client($sid, $token);

        if ($code == '0800') {

            $numbers = $client->availablePhoneNumbers('GB')->tollFree->read();

        } else {

            $numbers = $client->availablePhoneNumbers('GB')->local->read(
                array("contains" => "+44" . ltrim($code, '0'))
            );

        }



        return $numbers;
    }

    function purchaseNumber($n, $page_key) {

        $sid = "ACe1dddb02fba951dacb756630e4379311"; // Your Account SID from www.twilio.com/console
        $token = "a84d297005113aa2bdee0d244c94d1c0"; // Your Auth Token from www.twilio.com/console

        $client = new Client($sid, $token);

        $number = $client->incomingPhoneNumbers
            ->create(
                array(
                    "phoneNumber" => $n,
                    "voiceUrl"      =>  'https://app.addio.co.uk/receivecall/track/' . $page_key,
                    "statusCallback" => 'https://app.addio.co.uk/receivecall/callstatus',
                )
            );

        return $number->sid;

    }

    function deleteNumber($id) {

        $sid = "ACe1dddb02fba951dacb756630e4379311"; // Your Account SID from www.twilio.com/console
        $token = "a84d297005113aa2bdee0d244c94d1c0"; // Your Auth Token from www.twilio.com/console

        $client = new Client($sid, $token);

        $number = $client->incomingPhoneNumbers($id)->delete();


    }

    function call($callnumber) {

        $response = new Twiml();
        $dial = $response->dial();
        $dial->number($callnumber);

        echo $response;

    }


}