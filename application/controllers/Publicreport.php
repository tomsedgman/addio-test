<?php

class Publicreport extends CI_Controller {

    public function viewpage($days, $page_key) {

        $data = array();

        $this->db->where('key', $page_key);
        $pages                  =       $this->db->get('page')->result_array();
        $page = $pages[0];

        $pages_array            =       array();

        // GET THE DATE RANGE

        if ($days == 30) {

            $start              =           date('Y-m-d 00:00:00', strtotime('- 29 days'));

        }
        if ($days == 7) {

            $start              =           date('Y-m-d 00:00:00', strtotime('- 6 days'));

        }

        $p = array();

        $this->db->where('created >= ', $start);
        $this->db->where('page_key', $page_key);
        $this->db->select('COUNT(ipaddress) as total');
        $total = $this->db->get('page_view')->result_array();
        $total = $total[0]['total'];

        $p['total'] = $total;

        $this->db->where('created >= ', $start);
        $this->db->where('page_key', $page_key);
        $this->db->select('COUNT(name) as total');
        $forms = $this->db->get('formfill')->result_array();
        $forms = $forms[0]['total'];

        $p['forms'] = $forms;


        $this->db->where('created >= ', $start);
        $this->db->where('page_key', $page_key);
        $this->db->select('COUNT(page_key) as total');
        $calls = $this->db->get('call')->result_array();
        $calls = $calls[0]['total'];

        $p['calls'] = $calls;

        $this->db->where('created >= ', $start);
        $this->db->where('page_key', $page_key);
        $this->db->select('COUNT(page_key) as total');
        $ccalls = $this->db->get('click_phone')->result_array();
        $ccalls = $ccalls[0]['total'];

        $p['ccalls'] = $ccalls;

        $this->db->where('created >= ', $start);
        $this->db->where('page_key', $page_key);
        $this->db->select('COUNT(page_key) as total');
        $cemail = $this->db->get('click_email')->result_array();
        $cemail = $cemail[0]['total'];

        $p['cemails'] = $cemail;

        $this->db->where('fromdate >= ', $start);
        $this->db->where('page_key', $page_key);
        $this->db->select('SUM(amount) as total');
        $spend = $this->db->get('advertising_spend')->result_array();
        $spend = $spend[0]['total'];

        $p['spend'] = $spend;

        array_push($pages_array, $p);

        // GRAPH

        $graph = array(array('Date', 'Total'));

        for ($i = $days; $i > -1; $i--) {

            $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime('-' . $i . ' days')));
            $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime('-' . $i . ' days')));
            $this->db->where('page_key', $page_key);
            $this->db->select('COUNT(ipaddress) as total, MIN(created) as date');
            $total = $this->db->get('page_view')->result_array();
            $graph[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


        }

        $graph_form = array(array('Date', 'Total'));

        for ($i = $days; $i > -1; $i--) {

            $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime('-' . $i . ' days')));
            $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime('-' . $i . ' days')));
            $this->db->where('page_key', $page_key);
            $this->db->select('COUNT(name) as total, MIN(created) as date');
            $total = $this->db->get('formfill')->result_array();
            $graph_form[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


        }

        $graph_cemail = array(array('Date', 'Total'));

        for ($i = $days; $i > -1; $i--) {

            $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime('-' . $i . ' days')));
            $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime('-' . $i . ' days')));
            $this->db->where('page_key', $page_key);
            $this->db->select('COUNT(page_key) as total, MIN(created) as date');
            $total = $this->db->get('click_email')->result_array();
            $graph_cemail[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


        }

        $graph_call = array(array('Date', 'Total'));

        for ($i = $days; $i > -1; $i--) {

            $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime('-' . $i . ' days')));
            $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime('-' . $i . ' days')));
            $this->db->where('page_key', $page_key);
            $this->db->select('COUNT(page_key) as total, MIN(created) as date');
            $total = $this->db->get('call')->result_array();
            $graph_call[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


        }

        $graph_ccall = array(array('Date', 'Total'));

        for ($i = $days; $i > -1; $i--) {

            $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime('-' . $i . ' days')));
            $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime('-' . $i . ' days')));
            $this->db->where('page_key', $page_key);
            $this->db->select('COUNT(page_key) as total, MIN(created) as date');
            $total = $this->db->get('click_phone')->result_array();
            $graph_ccall[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


        }

        $data['graph'] = json_encode($graph);
        $data['graph_form'] = json_encode($graph_form);
        $data['graph_call'] = json_encode($graph_call);
        $data['graph_ccall'] = json_encode($graph_ccall);
        $data['graph_cemail'] = json_encode($graph_cemail);
        $data['page']          =       $page;
        $data['page_data']     =       $p;


        $this->parser->parse('/analytics/publicpage.php', $data);

    }


}