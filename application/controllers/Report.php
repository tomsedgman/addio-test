<?php

require_once APPPATH . 'third_party/gc/autoload.php';

class Report extends CI_Controller {


    function index() {

        $data = array();

        $data['page_name']      =       "Page report";
        $data['menu_item']      =       "menu_report";
        $data['pageview']       =       "Report";

        $this->db->where('page_key', $this->session->userdata('page_key'));
        $crons = $this->db->get('report_cron')->result_array();
        $data['crons'] = $crons;


        $this->db->where('page.site_key', $this->session->userdata('site_key'));
        $this->db->where('status', 'Active');
        $this->db->join('site', 'site.key = page.site_key');
        $this->db->select('site.url as site_url, page.url as url, page.name as name, page.key as key');

        $pages                  =       $this->db->get('page')->result_array();

        $data['pages']          =       $pages;



        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/report/index.php', $data);

    }


    function submit_report() {

        $page_key                   =           $this->session->userdata('page_key');
        $start                      =           $this->input->post('start_date');
        $end                        =           $this->input->post('end_date');
        $budget                     =           $this->input->post('budget');
        $email                      =           $this->input->post('email');
        $client_key                 =           $this->session->userdata('client_key');



        $g                          =           new Grab();
        $url                        =           'https://app.addio.co.uk/report/overview/' . $page_key . '/' . $start . '/' . $end . '/' . $budget;
        $g->runpdfsave                          ($url, urlencode($email), $client_key);

        redirect("/report/?details");

    }


    function savepdf($email, $client_key) {

        $id = $_GET["id"];
        $filename = $_GET["filename"];

        $g = new Grab();
        $g->savepdf($id, $filename);

        // Custom id can be used to store user ids or whatever is needed for the later processing of the
        // resulting screenshot

        $this->db->where('key', $client_key);
        $clients = $this->db->get('client')->result_array();
        $logo = 'https://app.addio.co.uk' . $clients[0]['logo'];
        $from_email = $clients[0]['email'];


        $this->load->library('email');
        $this->email->from($from_email);
        $this->email->to(urldecode($email));

        $this->email->subject('Report attached');
        $this->email->message($this->emailtemplate($logo));
        $this->email->attach('./uploads/' . $filename);

        $this->email->send();


    }

    function overview($key, $from, $to, $budget) {

        $data = array();

        $start                  =           date('Y-m-d 00:00:00', strtotime($from));
        $end                    =           date('Y-m-d 23:59:59', strtotime($to));

        $data['start']          =       $start;
        $data['end']            =       $end;

        $this->db->where                ('key', $key);
        $pages                  =       $this->db->get('page')->result_array();

        $data['page']           =       $pages[0];


        $this->db->where('key', $pages[0]['site_key']);
        $sites = $this->db->get('site')->result_array();
        $data['site'] = $sites[0];

        // get the ranking overview data by group


        $this->db->order_by                 ('DATE(created)', 'asc');
        $this->db->where                    ('created >= ', $start);
        $this->db->where                    ('created <= ', $end);
        $this->db->where                    ('page_key', $key);
        $this->db->group_by                 ('DATE(created)');
        $this->db->select                   ('MIN(DATE(created)) as date, COUNT(ipaddress) as total');
        $visitors               =           $this->db->get('page_view')->result_array();

        $total_visitors = 0;

        if (count($visitors) > 0) {

            $graph                  =           array(array('Date', 'Total'));

            foreach ($visitors as $v) {

                $graph[] = array(date('d M Y', strtotime($v['date'])), (int) $v['total']);
                $total_visitors += $v['total'];

            }

            $data['graph']          =           json_encode($graph);


        } else {

            //$graph                  =           array(array('Date', 'Total'));
            //$graph[] = array(date('d M Y'), 0);
            $graph = array();
            $data['graph']          =           json_encode($graph);
        }

        //var_dump($graph);

        $data['total_visitors'] = $total_visitors;



        $this->db->where                    ('created >= ', $start);
        $this->db->where                    ('created <= ', $end);
        $this->db->where                    ('page_key', $key);
        $this->db->select                   ('COUNT(page_key) as total');
        $formfills                  =       $this->db->get('formfill')->result_array();


        $formfills_total = $formfills[0]['total'];
        $data['formfills'] = $formfills_total;

        $this->db->where                    ('created >= ', $start);
        $this->db->where                    ('created <= ', $end);
        $this->db->where                    ('page_key', $key);
        $this->db->select                   ('COUNT(page_key) as total');
        $clickemail                 =       $this->db->get('click_email')->result_array();


        $clickemail_total = $clickemail[0]['total'];
        $data['clickemail'] = $clickemail_total;

        $this->db->where                    ('created >= ', $start);
        $this->db->where                    ('created <= ', $end);
        $this->db->where                    ('page_key', $key);
        $this->db->select                   ('COUNT(page_key) as total');
        $calls                      =       $this->db->get('call')->result_array();

        $calls_total = $calls[0]['total'];

        $this->db->where                    ('created >= ', $start);
        $this->db->where                    ('created <= ', $end);
        $this->db->where                    ('page_key', $key);
        $this->db->select                   ('COUNT(page_key) as total');
        $click_phone                =       $this->db->get('click_phone')->result_array();

        $callclick_total = $click_phone[0]['total'];


        $data['calls'] = $callclick_total + $calls_total;

        $data['conversions'] = $formfills_total + $clickemail_total + $calls_total + $callclick_total;


        $end_date = strtotime($end);
        $start_date = strtotime($start);
        $datediff = $end_date - $start_date;

        $daterange = round($datediff / (60 * 60 * 24));

        $adspend = ($budget / 30) * $daterange;
        $data['adspend'] = round($adspend, 2);

        $data['days'] = $daterange;


        $this->db->where('key', $sites[0]['client_key']);
        $clients = $this->db->get('client')->result_array();
        $logo = 'https://app.addio.co.uk' . $clients[0]['logo'];
        $data['logo'] = $logo;


        $this->parser->parse('/report/header.php', $data);
        $this->parser->parse('/report/overview.php', $data);
    }

    function delete_schedule($key) {

        $this->db->where('key', $key);
        $this->db->delete('report_cron');

        redirect('/report');
    }
    function create_schedule() {

        $email                          =           $this->input->post('email');
        $frequency                      =           $this->input->post('frequency');
        $weekly                         =           $this->input->post('weekly');
        $monthly                        =           $this->input->post('monthly');

        if ($frequency == "Weekly") {
            $fn = $weekly;
        } else {
            $fn = $monthly;
        }

        $data = array(

            'key'               =>      $this->key_model->generate(),
            'email'             =>      $email,
            'frequency'         =>      $frequency,
            'frequencyNo'       =>      $fn,
            'created'           =>      date('Y-m-d'),
            'page_key'          =>      $this->session->userdata('page_key'),
            'client_key'        =>      $this->session->userdata('client_key'),
            'budget'            =>      $this->input->post('budget'),

        );

        $this->db->insert('report_cron', $data);

        redirect ('/report');

    }
    function run_cron() {

        $dayOfMonth = date('j');
        $dayOfWeek = date('N');

        // SELECT THE MONTHLY CRONS

        $this->db->where('frequency', "Monthly");
        $this->db->where('frequencyNo', $dayOfMonth);
        $crons = $this->db->get('report_cron')->result_array();

        $start = date('Y-m-d', strtotime('- 1 month - 1 day'));
        $end = date('Y-m-d');


        foreach ($crons as $c) {

            $page_key                   =           $c['page_key'];
            $email                      =           $c['email'];

            $g                          =           new Grab();
            $url                        =           'https://app.addio.co.uk/report/overview/' . $page_key . '/' . $start . '/' . $end . '/' . $c['budget'];
            $g->runpdfsave                          ($url, urlencode($email), $c['client_key']);

        }

        echo "Monthly crons<br/>";
        print_r($crons);


        // SELECT THE WEEKLY CRONS

        $this->db->where('frequency', "Weekly");
        $this->db->where('frequencyNo', $dayOfWeek);
        $crons = $this->db->get('report_cron')->result_array();

        $start = date('Y-m-d', strtotime('- 1 week - 1 day'));
        $end = date('Y-m-d');

        foreach ($crons as $c) {

            $page_key                   =           $c['page_key'];
            $email                      =           $c['email'];

            $g                          =           new Grab();
            $url                        =           'https://app.addio.co.uk/report/overview/' . $page_key . '/' . $start . '/' . $end . '/' . $c['budget'];
            $g->runpdfsave                          ($url, urlencode($email), $c['client_key']);

        }

        echo "<br/><br/>Weekly crons<br/>";
        print_r($crons);


    }


    function emailtemplate($logo) {

        $html = '<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Report</title>
    
    ';

        $html .= $this->email_style();

        $html .= '
    
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Your report is attached</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
            ';

        $html .= $this->email_content($logo);

        $html .= '

              
            </table>

';

        $html .= $this->email_footer();

        $html .= '
            

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>';

        return $html;

    }
    function email_content($logo) {

        return '<!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="logo" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                    <img width="100px" src="' . $logo . '"/>
                </td>
              </tr>
              <tr>
             
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Hi there,
                        </p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Your new Addio report is attached to this email.
                        </p>
                        
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                       If you have any problems please contact our support team who will be happy to help.</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Good luck!</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->';
    }
    function email_footer() {

        return '<!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"></span>
                   
                  </td>
                </tr>
                
              </table>
            </div>
            <!-- END FOOTER -->';

    }
    function email_style() {

        return '<style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .logo {
        padding: 10px !important;
        background: transparent;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>';
    }

}
