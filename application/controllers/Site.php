<?php

class Site extends Auth_Controller {

    function index() {

        $data = array();

        $data['page_name']      =       "Sites";
        $data['menu_item']      =       "menu_site";


        $this->db->where('client_key', $this->session->userdata('client_key'));
        $this->db->join('page', 'page.site_key = site.key');
        $this->db->select('site.key as key, site.name, site.main_url, COUNT(page.name) as pages');
        $this->db->group_by('site_key');
        $sites                  =       $this->db->get('site')->result_array();
        $data['sites']          =       $sites;



        if (count($sites) == 0) {
            redirect('/site/add');
        } else {

            $this->parser->parse('/site/list.php', $data);

        }

    }
    function select($key, $redirect = null) {

        $this->db->where('key', $key);
        $sites = $this->db->get('site')->result_array();

        $this->db->where('status', 'Active');
        $this->db->where('site_key', $sites[0]['key']);
        $pages = $this->db->get('page')->result_array();

        $this->session->set_userdata('site_key', $key);
        $this->session->set_userdata('site_name', $sites[0]['name']);


        if (count($pages) > 0) {

            $this->session->set_userdata('page_key', $pages[0]['key']);
            $this->session->set_userdata('page_name', $pages[0]['name']);

            $this->session->set_userdata('site_access', 'Admin');

            if ($redirect) {

                redirect($redirect);

            } else {

                redirect($_SERVER['HTTP_REFERER']);

            }

        } else {

            redirect('page/add');

        }




    }
    function add() {

        $data = array();

        $data['pageview'] = "Site";

        $title = array(

            array(
                'title'         =>      'Sites',
                'url'           =>      '/site'
            ),
            array(
                'title'         =>      'Add site',
                'url'           =>      null,
            ),

        );

        $data['header_title']   =       $title;

        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/site/add.php', $data);
    }
    function addsite() {

        $data = array();
        $this->parser->parse('/site/addsite.php', $data);
    }
    function addnew() {

        $url                    =               strtolower(str_replace(' ', '-', $this->input->post('name')));
        $key                    =               $this->key_model->generate();

        $data = array(
            'name'              =>              $this->input->post('name'),
            'main_url'          =>              $this->input->post('url'),
            'privacy_policy'    =>              $this->input->post('privacy_policy'),
            'cookie_policy'     =>              $this->input->post('cookie_policy'),
            'address'           =>              $this->input->post('address'),
            'created'           =>              date('Y-m-d H:i:s'),
            'url'               =>              $url . '.addio.co.uk',
            'user_key'          =>              $this->session->userdata('key'),
            'key'               =>              $key,
            'client_key'        =>              $this->session->userdata('client_key'),
        );

        $this->db->insert('site', $data);

        $this->session->set_userdata('site_key', $key);
        $this->session->set_userdata('site_name', $this->input->post('name'));
        $this->session->set_userdata('site_access', 'Admin');

        redirect('/analytics');

    }
    function delete() {

        $site_key = $this->session->userdata('site_key');

        // 1. Delete advertising spend

        $this->db->where('site_key', $site_key);
        $this->db->delete('advertising_spend');

        $this->db->where('site_key', $site_key);
        $this->db->delete('advertising_spend_detail');

        $this->db->where('site_key', $site_key);
        $this->db->delete('adwords');

        // 2. Remove all the invites

        $this->db->where('site_key', $site_key);
        $this->db->delete('invites');

        // 3. Remove all the user access

        $this->db->where('site_key', $site_key);
        $this->db->delete('user_access');

        // 4. Get all the pages

        $this->db->where('site_key', $site_key);
        $pages = $this->db->get('page')->result_array();

        foreach ($pages as $p) {

            $page_key = $p['key'];

            // 5. Delete the calls
            $this->db->where('page_key', $page_key);
            $this->db->delete('call');

            // 6. Delete the email clicks
            $this->db->where('page_key', $page_key);
            $this->db->delete('click_email');

            // 7. Delete the call clicks
            $this->db->where('page_key', $page_key);
            $this->db->delete('click_phone');

            // 8. Delete the form fills
            $this->db->where('page_key', $page_key);
            $this->db->delete('formfill');

            // 9. Delete the page views
            $this->db->where('page_key', $page_key);
            $this->db->delete('page_view');

            // 10. Delete the sections
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_about');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_banner');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_cta');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_header');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_preheader');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_subbanner');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_testimonial');

            // 11. Delete the page
            $this->db->where('key', $page_key);
            $this->db->delete('page');

        }

        // 12. Delete this site
        $this->db->where('key', $site_key);
        $this->db->delete('site');


        redirect('/client/select/' . $this->session->userdata('client_key'));


    }

}