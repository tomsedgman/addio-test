<?php
class Leads extends Auth_Controller {


    public function index() {

        redirect('/leads/view/7');

    }

    public function view($start = null, $end = null) {

        $data = array();


        $data['menu_item']      =       "menu_leads";
        $data['pageview']       =       "Email Leads";

        if (!$start) {

            $start              =           date('Y-m-d', strtotime('- 1 month'));
        } else {
            $start = date('Y-m-d 00:00:00', strtotime($start));
        }

        if (!$end) {

            $end                =           date('Y-m-d');
        } else {
            $end = date('Y-m-d 23:59:59', strtotime($end));
        }

        $data['start'] = $start;
        $data['end'] = $end;

        if ($this->session->userdata('page_key') !== 'all') {
            $this->db->where('key', $this->session->userdata('page_key'));

            $data['page_name']      =       "Email Leads for " . $this->session->userdata('page_name');
        } else {
            $data['page_name']      =       "Email Leads for all pages";
        }

        $this->db->where('site_key', $this->session->userdata('site_key'));

        $pages                  =       $this->db->get('page')->result_array();

        $return_data = array();

        $this->db->where('formfill.created >=', date('Y-m-d 00:00:00', strtotime($start)));
        $this->db->where('formfill.created <=', date('Y-m-d 23:59:59', strtotime($end)));
        $this->db->where('site_key', $pages[0]['site_key']);
        $this->db->join('page', 'page.key = formfill.page_key');
        $this->db->join('site', 'site.key = page.site_key');
        $this->db->order_by('formfill.created', 'desc');
        $this->db->select('page.name as pagename, formfill.created as created, formfill.key as key, formfill.name as name, formfill.email as email, formfill.company as company, formfill.phone as phone');
        $formfills = $this->db->get('formfill')->result_array();
        $return_data[] = $formfills;

        $data['formfills'] = $return_data;

        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/leads/overview.php', $data);



    }


}
