<?php
class Advertising extends Auth_Controller {


    public function index() {

        $data = array();

        $data['page_name']      =       "Advertising";
        $data['menu_item']      =       "menu_advertising";

        /*
        $this->db->where('adwords.site_key', $this->session->userdata('site_key'));
        $this->db->join('page', 'page.key = adwords.page_key');
        $this->db->select('adwords.key as key, adwords.created as created, page.name as name, adwords.adspend as adspend, adwords.status as status');
        $this->db->order_by('created', 'desc');
        $adwords                 =       $this->db->get('adwords')->result_array();
        $data['adwords']          =       $adwords;
        */

        $this->db->where        ('advertising_spend.site_key', $this->session->userdata('site_key'));
        $this->db->join         ('page', 'page.key = advertising_spend.page_key', 'left');
        $this->db->select       ('advertising_spend.key as key, advertising_spend.created as created, page.name as name, advertising_spend.budget as budget, advertising_spend.balance as balance, advertising_spend.fromdate');
        $this->db->order_by     ('created', 'desc');
        $ads                    =       $this->db->get('advertising_spend')->result_array();
        $data['ads']            =       $ads;

        $this->parser->parse('header.php', $data);
        $this->parser->parse('/advertising/overview.php', $data);

    }

    public function campaigns() {

        $g = new Googleads();
        $g->getCampaigns();


    }

    function add() {

        if ($_POST) {

            if ($this->input->post('package') == "Starter") {
                $budget = 198.00;
            } else if ($this->input->post('package') == "Advanced") {
                $budget = 281.00;
            } else  {
                $budget = 358.00;
            }

            $data = array(
                'key'           =>      $this->key_model->generate(),
                'page_key'      =>      $this->input->post('page_key'),
                'fromdate'      =>      $this->input->post('date'),
                'budget'        =>      $budget,
                'package'       =>      $this->input->post('package'),
                'created'       =>      date('Y-m-d H:i:s'),
                'site_key'      =>      $this->session->userdata('site_key'),
                'balance'       =>      $budget,
            );

            $this->db->insert('advertising_spend', $data);
            redirect('/advertising');


        } else {

            $this->db->order_by('name');
            $data['locations']                      =               $this->db->get('google_locations')->result_array();

            $this->db->where('site_key', $this->session->userdata('site_key'));
            $data['pages']                          =               $this->db->get('page')->result_array();


            $title = array(

                array(
                    'title'         =>      'Advertising',
                    'url'           =>      '/advertising'
                ),
                array(
                    'title'         =>      'Add advert',
                    'url'           =>      null
                ),

            );

            $data['menu_item']      =       "menu_advertising";
            $data['header_title']   =       $title;


            $this->parser->parse('header.php', $data);
            $this->parser->parse('/advertising/add.php', $data);


        }


    }

    function add_NEW() {

        $this->db->order_by('name');
        $data['locations']                      =               $this->db->get('google_locations')->result_array();

        $this->db->where('site_key', $this->session->userdata('site_key'));
        $data['pages']                          =               $this->db->get('page')->result_array();


        $title = array(

            array(
                'title'         =>      'Advertising',
                'url'           =>      '/advertising'
            ),
            array(
                'title'         =>      'Add advert',
                'url'           =>      null
            ),

        );

        $data['menu_item']      =       "menu_advertising";
        $data['header_title']   =       $title;


        $this->parser->parse('header.php', $data);
        $this->parser->parse('/advertising/create_advert.php', $data);


    }

    function create() {

        $name                                   =               "My test campaign";

        $page_key                               =               $this->input->post('page_key');

        $this->db->where('key', $page_key);
        $pages                                  =               $this->db->get('page')->result_array();

        $site_key                               =               $this->session->userdata('site_key');
        $site_name                              =               $this->session->userdata('site_name');

        $this->db->where('key', $site_key);
        $sites                                  =               $this->db->get('site')->result_array();

        $criteriaId                             =               $this->input->post('criteriaId');
        $radius                                 =               $this->input->post('radius');
        $criteriaId                             =               $this->input->post('criteriaId');
        $headline1                              =               $this->input->post('headline1');
        $headline2                              =               $this->input->post('headline2');
        $description                            =               $this->input->post('description');
        $finalUrl                               =               'https://' . $sites[0]['url'] . '/' . $pages[0]['url'];
        $url1                                   =               $this->input->post('url1');
        $url2                                   =               $this->input->post('url2');

        $package                                =               $this->input->post('package');

        $budget = 0;
        if ($package == "Starter") { $budget = 6.60; }
        if ($package == "Advanced") { $budget = 9.30; }
        if ($package == "Premium") { $budget = 11.90; }


        $keywords                               =               explode("\r\n", $this->input->post('keywords'));


        $g = new Googleads();
        $campaignId                             =               $g->createCamapaign($site_name . ' (' . $site_key . ')', round(($budget), 0));

        $criteriaId                             =               $g->setTargetting($campaignId, $criteriaId);

        $adGroupId                              =               $g->createAdGroup($pages[0]['name'] . ' - ' . date('Ymd'), $campaignId, 0.50);

        foreach ($keywords as $k) {

            $keyword                            =               $g->createKeywords($adGroupId, 0.50, $k);

        }

        $advert                                 =               $g->createAdvert($adGroupId, $headline1, $headline2, $description, $finalUrl, $url1, $url2);


        $data = array(
            'key'                               =>              $this->key_model->generate(),
            'ga_campaignId'                     =>              $campaignId,
            'site_key'                          =>              $this->session->userdata('site_key'),
            'page_key'                          =>              $page_key,
            'created'                           =>              date('Y-m-d H:i:s'),
            'adspend'                           =>              198,
            'status'                            =>              'Live',
            'package'                           =>              $package,
        );

        $this->db->insert('adwords', $data);


        redirect('/advertising');


    }


}
