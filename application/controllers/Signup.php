<?php
class Signup extends CI_Controller {


    public function index() {


        redirect('/signup/go/noinvite');

    }

    public function go($client = null)
    {

        $data = array();

        if ($client) {

            $this->db->where('key', $client);
            $clients = $this->db->get('client')->result_array();
            $logo = $clients[0]['logo'];
            $data['logo'] = $logo;
            $data['client'] = $client;

            $this->parser->parse('/account/signup.php', $data);

        } else {

            redirect('/signup/go/49N4qnYsXv/noinvite');
        }


    }

    public function dosignup() {


        $this->db->where                ('email', $this->input->post('email'));
        $invites            =           $this->db->get('invites')->result_array();

        if (count($invites) == 0) {

            redirect('/signup/go/49N4qnYsXv/noinvite');

        } else {

            $this->db->where('email', $this->input->post('email'));
            $users = $this->db->get('users')->result();
            if (count($users) > 0) {

                redirect('/signup/go/49N4qnYsXv/exists');

            } else {

                $this->load->library('encrypt');

                $fname = $this->input->post('fname');
                $lname = $this->input->post('lname');
                $email = $this->input->post('email');
                $pwd = $this->input->post('password');
                $e_pwd = $this->encrypt->encode($pwd);

                $user_key       =       $this->key_model->generate();

                $data = array(
                    'key' => $user_key,
                    'fname' => $fname,
                    'lname' => $lname,
                    'email' => $email,
                    'password' => $e_pwd,
                    'created' => date('Y-m-d H:i:s'),
                    'site_key'  =>  $invites[0]['site_key'],
                    'client_key'    =>  $this->input->post('client'),
                    'user_type'        =>          $invites[0]['access'],
                );

                $this->db->insert('users', $data);


                $this->db->where('email', $this->input->post('email'));
                $users = $this->db->get('users')->result();

                $this->db->where('key', $this->input->post('client'));
                $clients = $this->db->get('client')->result_array();
                $logo = $clients[0]['logo'];
                $fromemail = $clients[0]['email'];


                $this->load->library('email');
                $this->email->from($fromemail);
                $this->email->to($users[0]->email);

                // TODO
                // Need to set the logo on the account created email

                $this->email->subject('Account created');
                $this->email->message($this->emailtemplate($logo));

                $this->email->send();

                // Delete the invite

                $this->db->where('key', $invites[0]['key']);
                $this->db->delete('invites');

                redirect('/login?created');

            }

        }

    }

    function emailtemplate($logo) {

        $html = '<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Your new account</title>
    
    ';

        $html .= $this->email_style();

        $html .= '
    
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Your account has been created</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
            ';

        $html .= $this->email_content($logo);

        $html .= '

              
            </table>

';

        $html .= $this->email_footer();

        $html .= '
            

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>';

        return $html;

    }
    function email_content($logo) {

        return '<!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="logo" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                    <img width="100px" src="https://app.addio.co.uk' . $logo . '"/>
                </td>
              </tr>
              <tr>
             
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Hi there,
                        </p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Congratulations, your account has now been created. You can login by clicking the button below.
                        </p>
                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
                          <tbody>
                            <tr>
                              <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> 
                                      <a href="' . site_url() . 'login" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">
                                      Login now
                                      </a> 
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                       If you have any problems please contact our support team who will be happy to help.</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Good luck!</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->';
    }
    function email_footer() {

        return '<!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"></span>
                   
                  </td>
                </tr>
                
              </table>
            </div>
            <!-- END FOOTER -->';

    }
    function email_style() {

        return '<style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .logo {
        padding: 10px !important;
        background: transparent;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>';
    }

}
