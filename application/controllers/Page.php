<?php
class Page extends Auth_Controller {


    public function index() {

        $data = array();

        $data['page_name']      =       "Pages";
        $data['menu_item']      =       "menu_page";
        $data['pageview']      =       "Page setup";

        $this->db->order_by('name');
        $this->db->where('page.site_key', $this->session->userdata('site_key'));
        $this->db->where('status', 'Active');
        $this->db->join('site', 'site.key = page.site_key');
        $this->db->select('site.url as site_url, page.url as url, page.name as name, page.key as key');

        $pages                  =       $this->db->get('page')->result_array();

        if (count($pages) > 0) {

            //$data['pages']          =       $pages;

            //$this->parser->parse('header_new.php', $data);
            //$this->parser->parse('/page/list.php', $data);

            redirect('/page/configure/');


        } else {

            redirect('page/add');

        }


    }

    function select($key) {

        if ($key == 'all') {

            $this->session->set_userdata('page_key', 'all');
            $this->session->set_userdata('page_name', 'All pages');

        } else {

            $this->db->where('key', $key);
            $pages = $this->db->get('page')->result_array();

            $this->session->set_userdata('page_key', $pages[0]['key']);
            $this->session->set_userdata('page_name', $pages[0]['name']);

        }

        $r = $_SERVER['HTTP_REFERER'];

        if (strpos($r, '/report') || strpos($r, '/page')) {

            redirect('/analytics');

        } else {

            redirect($_SERVER['HTTP_REFERER']);
        }



    }

    public function purchasenumber($number) {

        $page_key = $this->session->userdata('page_key');

        $number                 =       urldecode($number);

        $t = new Twillio();
        $sid                    =       $t->purchaseNumber($number, $page_key);


        $data = array(
            'trackable_phone'   =>      $number,
            'twillio_key'       =>      $sid,
        );

        $this->db->where('key', $page_key);
        $this->db->update('page', $data);

        redirect('/page/configure/' . $page_key);


    }
    function remove_banner_bg($page_key) {

        $this->db->where('key', $page_key);
        $pages = $this->db->get('page')->result_array();
        $page = $pages[0];


        $settings = (Array) json_decode($page['settings']);


        $settings['banner_bgimage'] = '';

        $new_settings = json_encode($settings);

        $data = array(

            'settings'      =>      $new_settings,
        );


        $this->db->where('key', $page_key);
        $this->db->update('page', $data);

        redirect('/page/editor/' . $page_key . '/?success');


    }
    function saveeditor() {

        $page_key = $this->session->userdata('page_key');

        $config['upload_path']          = './uploads/pages/' . $page_key;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|svg';
        $config['max_size']             = 1000;
        $config['max_width']            = 3024;
        $config['max_height']           = 1768;
        $this->load->library('upload', $config);

        $this->db->where('key', $page_key);
        $pages = $this->db->get('page')->result_array();
        $page = $pages[0];


        $settings = (Array) json_decode($page['settings']);


        if ($this->input->post('banner_image_upload') !== '') {


            if (!file_exists('./uploads/pages/' . $page_key)) {
                mkdir('./uploads/pages/' . $page_key);
            }

            if ( ! $this->upload->do_upload('banner_image_upload'))
            {
                //$error = array('error' => $this->upload->display_errors());
                //print_r($error);
                //var_dump("file: " . $this->input->post('banner_image_upload'));
            }
            else
            {
                echo $this->input->post('banner_image_upload');

                $data2 = array('upload_data' => $this->upload->data());
                //print_r($data2);
                $data['logo'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                $settings['banner_bgimage'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

            }


        }


        // HEADER SECTION

        $settings['head_bgcolor']                =      $this->input->post('head_bgcolor');
        $settings['head_fontcolor']              =      $this->input->post('head_fontcolor');

        if ($this->input->post('head_logo_upload') !== '') {

            if (!file_exists('./uploads/pages/' . $page_key)) {
                mkdir('./uploads/pages/' . $page_key);
            }





            if ( ! $this->upload->do_upload('head_logo_upload'))
            {
                //$error = array('error' => $this->upload->display_errors());
                //print_r($error);


            }
            else
            {
                $data2 = array('upload_data' => $this->upload->data());
                //print_r($data2);
                $data['logo'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                $settings['head_logo'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

            }


        }

        // PREHEADER SECTION

        $settings['prehead_bgcolor']                =      $this->input->post('prehead_bgcolor');
        $settings['prehead_fontcolor']                =      $this->input->post('prehead_fontcolor');
        $settings['prehead_text1']                =      $this->input->post('prehead_text1');
        $settings['prehead_text2']                =      $this->input->post('prehead_text2');


        // BANNER SECTION
        $settings['banner_bgcolor']                =      $this->input->post('banner_bgcolor');
        $settings['banner_fontcolor']              =      $this->input->post('banner_fontcolor');
        $settings['banner_text']              =      $this->input->post('banner_text');
        $settings['banner_bp1_title']              =      $this->input->post('banner_bp1_title');
        $settings['banner_bp1_text']              =      $this->input->post('banner_bp1_text');
        $settings['banner_bp2_title']              =      $this->input->post('banner_bp2_title');
        $settings['banner_bp2_text']              =      $this->input->post('banner_bp2_text');
        $settings['banner_bp3_title']              =      $this->input->post('banner_bp3_title');
        $settings['banner_bp3_text']              =      $this->input->post('banner_bp3_text');
        $settings['banner_form_header']                =      $this->input->post('banner_form_header');
        $settings['banner_form_border']              =      $this->input->post('banner_form_border');
        $settings['banner_form_headline']              =      $this->input->post('banner_form_headline');
        $settings['banner_form_headline2']              =      $this->input->post('banner_form_headline2');
        $settings['banner_form_pretext']              =      $this->input->post('banner_form_pretext');
        $settings['banner_form_textbox1']              =      $this->input->post('banner_form_textbox1');
        $settings['banner_form_textbox2']              =      $this->input->post('banner_form_textbox2');
        $settings['banner_form_textbox3']              =      $this->input->post('banner_form_textbox3');
        $settings['banner_form_textbox4']              =      $this->input->post('banner_form_textbox4');




        // SUBBANNER SECTION

        $settings['subbanner_bgcolor']                =      $this->input->post('subbanner_bgcolor');
        $settings['subbanner_fontcolor']                =      $this->input->post('subbanner_fontcolor');
        $settings['subbanner_bannertext']                =      $this->input->post('subbanner_bannertext');

        // TESTIMONIAL SECTION

        $settings['test_bgcolor']                =      $this->input->post('test_bgcolor');
        $settings['test_fontcolor']                =      $this->input->post('test_fontcolor');
        $settings['test_text1']                =      $this->input->post('test_text1');
        $settings['test_text2']                =      $this->input->post('test_text2');
        $settings['test_author1']                =      $this->input->post('test_author1');
        $settings['test_author2']                =      $this->input->post('test_author2');

        // ABOUT SECTION

        $settings['about_bgcolor']                =      $this->input->post('about_bgcolor');
        $settings['about_fontcolor']                =      $this->input->post('about_fontcolor');
        $settings['about_titletext']                =      $this->input->post('about_titletext');
        $settings['about_description']                =      $this->input->post('about_description');


        if ($this->input->post('about_image_upload') !== '') {

            if (!file_exists('./uploads/pages/' . $page_key)) {
                mkdir('./uploads/pages/' . $page_key);
            }




            if ( ! $this->upload->do_upload('about_image_upload'))
            {
                //$error = array('error' => $this->upload->display_errors());
                //print_r($error);


            }
            else
            {
                $data2 = array('upload_data' => $this->upload->data());
                //print_r($data2);
                $data['logo'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                $settings['about_image'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

            }


        }


        // CTA SECTION

        $settings['cta_bgcolor']                =      $this->input->post('cta_bgcolor');
        $settings['cta_fontcolor']              =      $this->input->post('cta_fontcolor');
        $settings['cta_header1']                =      $this->input->post('cta_header1');
        $settings['cta_header2']                =      $this->input->post('cta_header2');




        $new_settings = json_encode($settings);

        $data = array(

            'settings'      =>      $new_settings,
        );


        $this->db->where('key', $page_key);
        $this->db->update('page', $data);

        redirect('/page/editor/' . $page_key . '/?success');


    }
    public function deletenumber($sid) {

        $page_key = $this->session->userdata('page_key');

        $t = new Twillio();
        $sid                    =       $t->deleteNumber($sid);


        $data = array(
            'trackable_phone'   =>      '',
            'twillio_key'       =>      '',
        );

        $this->db->where('key', $page_key);
        $this->db->update('page', $data);

        redirect('/page/edit/' . $page_key);


    }
    public function searchnumbers() {

        $page_key = $this->session->userdata('page_key');

        if ($_POST) {

            $t = new Twillio();
            $numbers = $t->searchNumbers($this->input->post('code'));

            $data = array();

            $data['page_name']      =       "Edit page";
            $data['pageview']      =       "Page setup";
            $data['menu_item']      =       "menu_page";
            $data['numbers']        =       $numbers;
            $data['code']           =       $this->input->post('code');

            $this->db->where('key', $page_key);
            $page = $this->db->get('page')->result_array();
            $page = $page[0];
            $data['page'] = $page;

            $this->db->order_by('name');
            $this->db->where('page.site_key', $this->session->userdata('site_key'));
            $this->db->where('status', 'Active');
            $this->db->join('site', 'site.key = page.site_key');
            $this->db->select('site.url as site_url, page.url as url, page.name as name, page.key as key');

            $pages                  =       $this->db->get('page')->result_array();

            $breadcrumbs = array();
            foreach ($pages as $p) {

                $b = array(
                    'title'             =>      $p['name'],
                    'url'               =>      '/page/configure/' . $p['key']
                );
                array_push($breadcrumbs, $b);

            }

            $data['breadcrumb'] = array('name' => $page['name'], 'title' => 'Page');
            $data['breadcrumbs'] = $breadcrumbs;


            $this->parser->parse('header_new.php', $data);
            $this->parser->parse('/page/getnumber.php', $data);


        } else {

            $data = array();

            $data['page_name']      =       "Edit page";
            $data['menu_item']      =       "menu_page";
            $data['numbers']        =       array();
            $data['pageview']      =       "Page setup";
            $data['code']           =       '';

            $this->db->where('key', $page_key);
            $page = $this->db->get('page')->result_array();
            $page = $page[0];
            $data['page'] = $page;

            $this->parser->parse('header_new.php', $data);
            $this->parser->parse('/page/getnumber.php', $data);
        }


    }
    public function saveconfigure($page_key, $section) {

        // GET THE CURRENT PAGE SETTINGS

        $this->db->where('key', $page_key);
        $page = $this->db->get('page')->result_array();
        $settings = (Array) json_decode($page[0]['settings']);


        if ($section == 'header') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $link1              =       $this->input->post('link1');
            $dest1              =       $this->input->post('dest1');
            $link2              =       $this->input->post('link2');
            $dest2              =       $this->input->post('dest2');
            $link3              =       $this->input->post('link3');
            $dest3              =       $this->input->post('dest3');
            $phonecolor         =       $this->input->post('phonecolor');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'page_key'      =>      $page_key,
                'link1'         =>      $link1,
                'dest1'         =>      $dest1,
                'link2'         =>      $link2,
                'dest2'         =>      $dest2,
                'link3'         =>      $link3,
                'dest3'         =>      $dest3,
                'phonecolor'    =>      $phonecolor,

            );

            $settings['head_bgcolor'] = $bgcolor;
            $settings['head_fontcolor'] = $fontcolor;
            $settings['head_link_1'] = $link1;
            $settings['head_dest_1'] = $dest1;
            $settings['head_link_2'] = $link2;
            $settings['head_dest_2'] = $dest2;
            $settings['head_link_3'] = $link3;
            $settings['head_dest_3'] = $dest3;
            $settings['head_phonecolor'] = $phonecolor;


            if ($this->input->post('logo_file') !== '') {

                if (!file_exists('./uploads/pages/' . $page_key)) {
                    mkdir('./uploads/pages/' . $page_key);
                }

                $config['upload_path']          = './uploads/pages/' . $page_key;
                $config['allowed_types']        = 'gif|jpg|png|jpeg|svg';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo_file'))
                {
                    //$error = array('error' => $this->upload->display_errors());
                    //print_r($error);
                }
                else
                {
                    $data2 = array('upload_data' => $this->upload->data());
                    //print_r($data2);
                    $data['logo'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                    $settings['head_logo'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                }


            }




            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_header')->result_array();


            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_header', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_header', $data);


                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);


            }


        }
        if ($section == 'banner') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $bannertext         =       $this->input->post('bannertext');
            $form_header        =       $this->input->post('form_header');
            $form_border        =       $this->input->post('form_border');

            $bp1_title          =       $this->input->post('bp1_title');
            $bp1_text           =       $this->input->post('bp1_text');
            $bp2_title          =       $this->input->post('bp2_title');
            $bp2_text           =       $this->input->post('bp2_text');
            $bp3_title          =       $this->input->post('bp3_title');
            $bp3_text           =       $this->input->post('bp3_text');

            $bannerbox          =       $this->input->post('bannerbox');

            $form_headline      =       $this->input->post('form_headline');
            $form_headline2     =       $this->input->post('form_headline2');
            $form_pretext       =       $this->input->post('form_pretext');
            $form_button        =       $this->input->post('form_button');

            $form_textbox_1     =      $this->input->post('form_textbox_1');
            $form_textbox_2     =      $this->input->post('form_textbox_2');
            $form_textbox_3     =      $this->input->post('form_textbox_3');
            $form_textbox_4     =      $this->input->post('form_textbox_4');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'form_header'   =>      $form_header,
                'form_border'   =>      $form_border,
                'page_key'      =>      $page_key,
                'bannertext'    =>      $bannertext,

                'bp1_title'     =>      $bp1_title,
                'bp1_text'      =>      $bp1_text,
                'bp2_title'     =>      $bp2_title,
                'bp2_text'      =>      $bp2_text,
                'bp3_title'     =>      $bp3_title,
                'bp3_text'      =>      $bp3_text,

                'boxbehindtext' =>      $bannerbox,

                'form_headline' =>      $form_headline,
                'form_headline2'=>      $form_headline2,
                'form_pretext'  =>      $form_pretext,
                'form_button'   =>      $form_button,

                'form_textbox_1'    =>      $form_textbox_1,
                'form_textbox_2'    =>      $form_textbox_2,
                'form_textbox_3'    =>      $form_textbox_3,
                'form_textbox_4'    =>      $form_textbox_4,

            );


            $settings['banner_bgcolor']            =              $bgcolor;
            $settings['banner_text']               =              $bannertext;
            $settings['banner_fontcolor']          =              $fontcolor;
            $settings['banner_bp1_title']          =              $bp1_title;
            $settings['banner_bp1_text']           =              $bp1_text;
            $settings['banner_bp2_title']          =              $bp2_title;
            $settings['banner_bp2_text']           =              $bp2_text;
            $settings['banner_bp3_title']          =              $bp3_title;
            $settings['banner_bp3_text']           =              $bp3_text;
            $settings['banner_boxbehindtext']      =              $bannerbox;
            $settings['banner_form_header']        =              $form_header;
            $settings['banner_form_border']        =              $form_border;
            $settings['banner_form_headline']      =              $form_headline;
            $settings['banner_form_headline2']     =              $form_headline2;
            $settings['banner_form_pretext']       =              $form_pretext;
            $settings['banner_form_button']        =              $form_button;
            $settings['banner_form_textbox1']      =              $form_textbox_1;
            $settings['banner_form_textbox2']      =              $form_textbox_2;
            $settings['banner_form_textbox3']      =              $form_textbox_3;
            $settings['banner_form_textbox4']      =              $form_textbox_4;


            if ($this->input->post('bgimage_file') !== '') {

                if (!file_exists('./uploads/pages/' . $page_key)) {
                    mkdir('./uploads/pages/' . $page_key);
                }

                $config['upload_path']          = './uploads/pages/' . $page_key;
                $config['allowed_types']        = 'gif|jpg|png|jpeg|svg';
                $config['max_size']             = 10000;
                $config['max_width']            = 3024;
                $config['max_height']           = 1768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('bgimage_file'))
                {
                    //$error = array('error' => $this->upload->display_errors());
                    //print_r($error);
                }
                else
                {
                    $data2 = array('upload_data' => $this->upload->data());
                    $data['bgimage'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                    $settings['banner_bgimage']            =              '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                }
            }




            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_banner')->result_array();

            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_banner', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_banner', $data);

                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);

            }


        }
        if ($section == 'about') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $titletext         =       $this->input->post('titletext');
            $description        =       $this->input->post('description');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'titletext'   =>      $titletext,
                'description'   =>      $description,
                'page_key'      =>      $page_key,

            );

            $settings['about_bgcolor']             =              $bgcolor;
            $settings['about_fontcolor']           =              $fontcolor;
            $settings['about_titletext']           =              $titletext;
            $settings['about_description']         =              $description;



            if ($this->input->post('image_file') !== '') {

                if (!file_exists('./uploads/pages/' . $page_key)) {
                    mkdir('./uploads/pages/' . $page_key);
                }

                $config['upload_path']          = './uploads/pages/' . $page_key;
                $config['allowed_types']        = 'gif|jpg|png|jpeg|svg';
                $config['max_size']             = 10000;
                $config['max_width']            = 3024;
                $config['max_height']           = 1768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image_file'))
                {
                    //$error = array('error' => $this->upload->display_errors());
                    //print_r($error);
                }
                else
                {
                    $data2 = array('upload_data' => $this->upload->data());
                    $data['image'] = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                    $settings['about_image']               = '/uploads/pages/' . $page_key . '/' . $data2['upload_data']['file_name'];

                }
            }




            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_about')->result_array();

            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_about', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_about', $data);

                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);

            }


        }
        if ($section == 'preheader') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $headertext1        =       $this->input->post('headertext1');
            $headertext2        =       $this->input->post('headertext2');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'page_key'      =>      $page_key,
                'headertext1'   =>      $headertext1,
                'headertext2'   =>      $headertext2,

            );

            $settings['prehead_bgcolor']           =              $bgcolor;
            $settings['prehead_fontcolor']         =              $fontcolor;
            $settings['prehead_text1']             =              $headertext1;
            $settings['prehead_text2']             =              $headertext2;

            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_preheader')->result_array();

            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_preheader', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_preheader', $data);

                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);

            }


        }
        if ($section == 'cta') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $headertext1        =       $this->input->post('headertext1');
            $headertext2        =       $this->input->post('headertext2');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'page_key'      =>      $page_key,
                'headertext1'   =>      $headertext1,
                'headertext2'   =>      $headertext2,

            );

            $settings['cta_bgcolor']               =              $bgcolor;
            $settings['cta_fontcolor']             =              $fontcolor;
            $settings['cta_header1']               =              $headertext1;
            $settings['cta_header2']               =              $headertext2;

            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_cta')->result_array();

            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_cta', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_cta', $data);

                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);

            }


        }
        if ($section == 'subbanner') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $bannertext         =       $this->input->post('bannertext');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'page_key'      =>      $page_key,
                'bannertext'    =>      $bannertext,

            );

            $settings['subbanner_bgcolor']         =              $bgcolor;
            $settings['subbanner_bannertext']      =              $bannertext;
            $settings['subbanner_fontcolor']       =              $fontcolor;

            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_subbanner')->result_array();

            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_subbanner', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_subbanner', $data);

                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);

            }


        }
        if ($section == 'testimonial') {

            $bgcolor            =       $this->input->post('bgcolor');
            $fontcolor          =       $this->input->post('fontcolor');
            $test1_text         =       $this->input->post('test1_text');
            $test1_author       =       $this->input->post('test1_author');
            $test2_text         =       $this->input->post('test2_text');
            $test2_author       =       $this->input->post('test2_author');

            $data = array(

                'bgcolor'       =>      $bgcolor,
                'fontcolor'     =>      $fontcolor,
                'page_key'      =>      $page_key,
                'test1_text'    =>      $test1_text,
                'test1_author'  =>      $test1_author,
                'test2_text'    =>      $test2_text,
                'test2_author'  =>      $test2_author,

            );

            $settings['test_text1']                =              $test1_text;
            $settings['test_author1']              =              $test1_author;
            $settings['test_text2']                =              $test2_text;
            $settings['test_author2']              =              $test2_author;
            $settings['test_bgcolor']              =              $bgcolor;
            $settings['test_fontcolor']            =              $fontcolor;

            $this->db->where('page_key', $page_key);
            $exists             =       $this->db->get('section_testimonial')->result_array();

            if (count($exists) == 0) {

                $data['key']    =       $this->key_model->generate();
                $this->db->insert('section_testimonial', $data);

            } else {

                $this->db->where('page_key', $page_key);
                $this->db->update('section_testimonial', $data);

                $settings_data['settings'] = json_encode($settings);

                $this->db->where('key', $page_key);
                $this->db->update('page', $settings_data);

            }


        }


        redirect('/page/configure/' . $page_key);

    }
    public function convertpages() {


        $pages = $this->db->get('page')->result_array();

        foreach ($pages as $page) {

            $page_key = $page['key'];

            $settings = (Array) json_decode($page['settings']);

            $header = $this->getHeaderSection($page_key);

            $settings['head_bgcolor'] = $header['bgcolor'];
            $settings['head_fontcolor'] = $header['fontcolor'];
            $settings['head_link_1'] = $header['link1'];
            $settings['head_dest_1'] = $header['dest1'];
            $settings['head_link_2'] = $header['link2'];
            $settings['head_dest_2'] = $header['dest2'];
            $settings['head_link_3'] = $header['link3'];
            $settings['head_dest_3'] = $header['dest3'];
            $settings['head_phonecolor'] = $header['phonecolor'];
            $settings['head_logo'] = $header['logo'];



            $prehead = (Object) $this->getPreHeaderSection($page_key);

            $settings['prehead_bgcolor']           =              $prehead->bgcolor;
            $settings['prehead_fontcolor']         =              $prehead->fontcolor;
            $settings['prehead_text1']             =              $prehead->headertext1;
            $settings['prehead_text2']             =              $prehead->headertext2;


            $banner = (Object) $this->getBannerSection($page_key);

            $settings['banner_bgcolor']            =              $banner->bgcolor;
            $settings['banner_text']               =              $banner->bannertext;
            $settings['banner_fontcolor']          =              $banner->fontcolor;
            $settings['banner_bp1_title']          =              $banner->bp1_title;
            $settings['banner_bp1_text']           =              $banner->bp1_text;
            $settings['banner_bp2_title']          =              $banner->bp2_title;
            $settings['banner_bp2_text']           =              $banner->bp2_text;
            $settings['banner_bp3_title']          =              $banner->bp3_title;
            $settings['banner_bp3_text']           =              $banner->bp3_text;
            $settings['banner_boxbehindtext']      =              $banner->boxbehindtext;
            $settings['banner_form_header']        =              $banner->form_header;
            $settings['banner_form_border']        =              $banner->form_border;
            $settings['banner_form_headline']      =              $banner->form_headline;
            $settings['banner_form_headline2']     =              $banner->form_headline2;
            $settings['banner_form_pretext']       =              $banner->form_pretext;
            $settings['banner_form_button']        =              $banner->form_button;
            $settings['banner_form_textbox1']      =              $banner->form_textbox_1;
            $settings['banner_form_textbox2']      =              $banner->form_textbox_2;
            $settings['banner_form_textbox3']      =              $banner->form_textbox_3;
            $settings['banner_form_textbox4']      =              $banner->form_textbox_4;
            $settings['banner_bgimage']            =              $banner->bgimage;


            $sub = (Object) $this->getSubBannerSection($page_key);

            $settings['subbanner_bgcolor']         =              $sub->bgcolor;
            $settings['subbanner_bannertext']      =              $sub->bannertext;
            $settings['subbanner_fontcolor']       =              $sub->fontcolor;


            $test = (Object) $this->getTestimonialSection($page_key);

            $settings['test_text1']                =              $test->test1_text;
            $settings['test_author1']              =              $test->test1_author;
            $settings['test_text2']                =              $test->test2_text;
            $settings['test_author2']              =              $test->test2_author;
            $settings['test_bgcolor']              =              $test->bgcolor;
            $settings['test_fontcolor']            =              $test->fontcolor;


            $about = (Object) $this->getAboutSection($page_key);

            $settings['about_bgcolor']             =              $about->bgcolor;
            $settings['about_fontcolor']           =              $about->fontcolor;
            $settings['about_titletext']           =              $about->titletext;
            $settings['about_description']         =              $about->description;
            $settings['about_image']               =              $about->image;


            $cta = (Object) $this->getCtaSection($page_key);

            $settings['cta_bgcolor']               =              $cta->bgcolor;
            $settings['cta_fontcolor']             =              $cta->fontcolor;
            $settings['cta_header1']               =              $cta->headertext1;
            $settings['cta_header2']               =              $cta->headertext2;



            $settings_data['settings'] = json_encode($settings);

            $this->db->where('key', $page_key);
            $this->db->update('page', $settings_data);

            echo "The page named -- " . $page['name'] . " -- Has been updated<br/>";

        }


        echo "All pages updated!!";


    }
    public function configure() {

        $page_key = $this->session->userdata('page_key');
        $data = array();

        $this->db->where('key', $page_key);


        $pages                          =       $this->db->get('page')->result_array();
        $page                           =       $pages[0];
        $data['page']                   =       $page;
        $data['pageview']               =       "Page setup";

        $this->db->where('key', $this->session->userdata('site_key'));
        $sites = $this->db->get('site')->result_array();
        $data['site'] = $sites[0];

        $data['menu_item']              =       "menu_page";

        $data['fonts'] = $this->getFonts();

        $data['fontsjson'] = json_encode($this->getfonts());


        if ($page['trackable_phone'] > '0') {

            $data['phone'] = $page['trackable_phone'];
        } else {
            $data['phone'] = $page['phone'];
        }


        $title = array(

            array(
                'title'             =>      'Pages',
                'url'               =>      '/page'
            ),
            array(
                'title'             =>      $page['name'],
                'url'               =>      null
            ),

        );




        $data['header_title']       =       $title;

        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/page/configure.php', $data);

    }
    public function add() {

        if ($_POST) {

            $name                   =       $this->input->post('name');
            $url                    =       $this->input->post('url');
            $phone                  =       $this->input->post('phone');
            $email                  =       $this->input->post('email');
            $main_keyword           =       $this->input->post('main_keyword');
            $key                    =       $this->key_model->generate();

            $data = array(

                'key'               =>      $key,
                'name'              =>      $name,
                'phone'             =>      $phone,
                'email'             =>      $email,
                'main_keyword'      =>      $main_keyword,
                'created'           =>      date('Y-m-d H:i:s'),
                'site_key'          =>      $this->session->userdata('site_key'),
                'url'               =>      $url,
                'status'            =>      'Active',
                'template'          =>      'template_1',
                'settings'          =>      $this->getInitalPageJson(),
                'font'              =>      'Product Sans',
                'fontfamily'        =>      "font-family: 'Product Sans', Arial, sans-serif",

            );

            $this->db->insert('page', $data);

            $this->session->set_userdata('page_key', $key);
            $this->session->set_userdata('page_name', $name);


            redirect('/page/configure/');

        } else {

            $data = array();

            $data['page_name']      =       "New page";
            $data['menu_item']      =       "menu_page";
            $data['pageview']       =       "Page setup";


            $this->parser->parse('header_new.php', $data);
            $this->parser->parse('/page/add.php', $data);
        }


    }
    public function edit() {

        $page_key = $this->session->userdata('page_key');

        $name                   =       $this->input->post('name');
        $url                    =       $this->input->post('url');
        $main_keyword           =       $this->input->post('main_keyword');
        $font                   =       $this->input->post('font');
        $fontfamily             =       $this->input->post('fontfamily');
        $email                  =       $this->input->post('email');
        $phone                  =       $this->input->post('phone');

        $data = array(

            'name'              =>      $name,
            'url'               =>      $url,
            'main_keyword'      =>      $main_keyword,
            'font'              =>      $font,
            'fontfamily'        =>      $fontfamily,
            'email'             =>      $email,
            'phone'             =>      $phone,

        );



        $this->db->where('key', $page_key);
        $this->db->update('page', $data);

        $this->session->set_userdata('page_name', $name);

        redirect('/page/configure?saved');


    }
    public function edit_calltracking($page_key) {

        if ($_POST) {

            $email          =       $this->input->post('email');
            $phone          =       $this->input->post('phone');

            $data = array(

                'email'     =>      $email,
                'phone'     =>      $phone,
            );



            $this->db->where('key', $page_key);
            $this->db->update('page', $data);

            redirect('/page/edit_contact/' . $page_key . '/?details');

        } else {

            $data = array();

            //$data['page_name']      =       "Edit page";
            $data['menu_item']      =       "menu_page";

            $this->db->where('key', $page_key);
            $page = $this->db->get('page')->result_array();
            $page = $page[0];
            $data['page'] = $page;

            $title = array(

                array(
                    'title'         =>      'Pages',
                    'url'           =>      '/page'
                ),
                array(
                    'title'         =>      $page['name'],
                    'url'           =>      '/page/configure/' . $page['key'],
                ),
                array(
                    'title'         =>      'Edit call tracking',
                    'url'           =>      null,
                ),

            );

            $data['header_title']   =       $title;

            $this->db->where('key', $this->session->userdata('site_key'));
            $sites = $this->db->get('site')->result_array();
            $data['site'] = $sites[0];


            $this->db->where('key', $this->session->userdata('key'));
            $users = $this->db->get('users')->result_array();
            $data['user'] = $users[0];


            $this->parser->parse('header.php', $data);
            $this->parser->parse('/page/edit_calltracking.php', $data);
        }


    }
    public function delete($page_key, $step) {

        if ($step == 'check') {

            $data = array();

            $data['page_name']      =       "Pages";
            $data['pageview']      =       "Pages";
            $data['menu_item']      =       "menu_page";

            $this->db->where('key', $page_key);
            $pages = $this->db->get('page')->result_array();
            $page = $pages[0];
            $data['page'] = $page;

            $this->parser->parse('header_new.php', $data);
            $this->parser->parse('/page/checkdelete.php', $data);


        } else {

            $this->db->where('key', $page_key);
            $pages = $this->db->get('page')->result_array();
            $page = $pages[0];

            if ($page['twillio_key'] > '0') {

                $t = new Twillio();
                $sid                    =       $t->deleteNumber($page['twillio_key']);

            }


            $data = array('trackable_phone' => '', 'twillio_key' => '', 'status' => 'Deleted');

            $this->db->where('key', $page_key);
            $this->db->update('page', $data);

            redirect('page');

        }


    }
    public function duplicate($page_key) {

        $this->db->where('key', $page_key);
        $page = $this->db->get('page')->result_array();
        $page = $page[0];

        $new_key            =       $this->key_model->generate();

        $data = array(

            'key'           =>      $new_key,
            'name'          =>      $page['name'] . ' Copy',
            'phone'         =>      $page['phone'],
            'email'         =>      $page['email'],
            'main_keyword'  =>      $page['main_keyword'],
            'created'       =>      date('Y-m-d H:i:s'),
            'site_key'      =>      $this->session->userdata('site_key'),
            'url'           =>      $page['url'] . '-copy',
            'status'        =>      'Active',
            'font'          =>      $page['font'],
            'fontfamily'    =>      $page['fontfamily'],
            'template'      =>      $page['template'],
            'settings'      =>      $page['settings'],

        );

        $this->db->insert('page', $data);

        redirect('/page/configure/' . $new_key);

    }

    public function editor() {

        $page_key = $this->session->userdata('page_key');
        $data = array();

        $this->db->where('key', $page_key);


        $pages                          =       $this->db->get('page')->result_array();
        $page                           =       $pages[0];
        $data['page']                   =       $page;

        $this->db->where('key', $this->session->userdata('site_key'));
        $sites = $this->db->get('site')->result_array();
        $data['site'] = $sites[0];

        $data['menu_item']              =       "menu_page";


        if ($page['trackable_phone'] > '0') {

            $data['phone'] = $page['trackable_phone'];
        } else {
            $data['phone'] = $page['phone'];
        }




        $title = array(

            array(
                'title'         =>      'Pages',
                'url'           =>      '/page'
            ),
            array(
                'title'         =>      $page['name'],
                'url'           =>      null
            ),

        );

        $data['header_title']      =       $title;

        //$this->parser->parse('header.php', $data);
        $this->parser->parse('/page/editor.php', $data);


    }
    public function getfonts() {

        $data = array();

        $data[] = array('id' => 0,      'name'      =>      "Product Sans", 'style'     =>      "font-family: 'Product Sans', Arial, sans-serif;");
        $data[] = array('id' => 1,      'name'      =>      "Dosis",        'style'     =>      "font-family: 'Dosis', sans-serif;");
        $data[] = array('id' => 2,      'name'      =>      "Montserrat",        'style'     =>      "font-family: 'Montserrat', sans-serif;");
        $data[] = array('id' => 3,      'name'      =>      "Open Sans",        'style'     =>      "font-family: 'Open Sans', sans-serif;");
        $data[] = array('id' => 4,      'name'      =>      "Roboto",        'style'     =>      "font-family: 'Roboto', sans-serif;");

        return $data;

    }
    public function getInitalPageJson() {

        return '{"head_bgcolor":"#ffffff","head_fontcolor":"#171206","head_link_1":"","head_dest_1":"","head_link_2":"","head_dest_2":"","head_link_3":"","head_dest_3":"","head_phonecolor":"#333333","head_logo":"\/uploads\/pages\/gWcwh2vcJT\/addio.png","prehead_bgcolor":"#6993e3","prehead_fontcolor":"#fbfbf8","prehead_text1":"YOUR MAIN HEADLINE TO GRAB THE VISITOR\'S ATTENTION","prehead_text2":"Short, sharp and to the point to ensure they continue reading the page","banner_bgcolor":"#dc8d13","banner_text":"A brief summary of why someone should come to you and not one of your competitorsasddadasd","banner_fontcolor":"#f6fbf2","banner_bp1_title":"Dedicated Account Managers","banner_bp1_text":"All our account managers are based in our own offices","banner_bp2_title":"Trading for over 10 years","banner_bp2_text":"We\'ve got a long history in delivering the best results for our client","banner_bp3_title":"We go the extra mile","banner_bp3_text":"We do whatever is required to ensure the highest standards","banner_boxbehindtext":"No","banner_form_header":"#6993e3","banner_form_border":"#6993e3","banner_form_headline":"Get in touch today","banner_form_headline2":"For a free consultation","banner_form_pretext":"Let our team show you how we\'ll make a difference and help your business grow","banner_form_button":"Get Started","banner_form_textbox1":"Name","banner_form_textbox2":"Email","banner_form_textbox3":"Company","banner_form_textbox4":"Phone","banner_bgimage":"","subbanner_bgcolor":"#6993e3","subbanner_bannertext":"Dedicated to helping our clients grow since 2008 - Let us help you","subbanner_fontcolor":"#f9fef9","test_text1":"Increased sales leads and reduced costs across one of the most competitive industries","test_author1":"John Stevens, The Big Company, Devon","test_text2":"A full and professional service from start to finish at an affordable price and fast ROI","test_author2":"Sheila Jones, The Widget Co. Northampton","test_bgcolor":"#fcfcfc","test_fontcolor":"#131313","about_bgcolor":"#ffffff","about_fontcolor":"#161f08","about_titletext":"Since 2008, Your Company has been helping businesses grow and develop","about_description":"We are proud to say we have been a key part in helping small and medium sized businesses grow and develop a greater market share.\r\n\r\nHeadquartered in the digital heartland of England, we are surrounded by some of the best talent our industry has to offer.\r\n\r\nOur team of dedicated experts combined with our leading technology is ready to meet even the toughest challenge!","about_image":"\/uploads\/pages\/gWcwh2vcJT\/office.jpg","cta_bgcolor":"#6993e3","cta_fontcolor":"#ffffff","cta_header1":"Get in touch today for a hassle free chat about how we can help","cta_header2":"","banner_form_bgcolor":null}';

    }

















    // OLD FUNCTIONS

    public function getHeaderSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section_header             =       $this->db->get('section_header')->result_array();

        if (count($section_header) > 0) {

            $section_header             =       $section_header[0];

        } else {

            $section_header = array(

                'bgcolor'           =>      '#7cc567',
                'fontcolor'         =>      '#CCCCCC',
                'link1'             =>      '',
                'dest1'             =>      '',
                'link2'             =>      '',
                'dest2'             =>      '',
                'link3'             =>      '',
                'dest3'             =>      '',

            );
        }

        return $section_header;

    }
    public function getPreHeaderSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section             =       $this->db->get('section_preheader')->result_array();

        if (count($section) > 0) {

            $section                     =       $section[0];

        } else {

            $section = array(

                'bgcolor'               =>      '#7cc567',
                'fontcolor'             =>      '#CCCCCC',
                'headertext1'           =>      '',
                'headertext2'           =>      '',

            );
        }
        return $section;
    }
    public function getBannerSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section_banner             =       $this->db->get('section_banner')->result_array();

        if (count($section_banner) > 0) {

            $section_banner             =       $section_banner[0];

        } else {

            $section_banner = array(

                'bgcolor'               =>      '#7cc567',
                'fontcolor'             =>      '#CCCCCC',
                'bannertitle'           =>      '',
                'bannertext'            =>      '',
                'bgimage'               =>      '',
                'form_header'           =>      '#CCC',
                'form_border'           =>      '#FFFFFF',


            );
        }
        return $section_banner;
    }
    public function getSubBannerSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section             =       $this->db->get('section_subbanner')->result_array();

        if (count($section) > 0) {

            $section                     =       $section[0];

        } else {

            $section = array(

                'bgcolor'               =>      '#7cc567',
                'fontcolor'             =>      '#CCCCCC',
                'bannertext'            =>      '',

            );
        }
        return $section;
    }
    public function getTestimonialSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section             =       $this->db->get('section_testimonial')->result_array();

        if (count($section) > 0) {

            $section                     =       $section[0];

        } else {

            $section = array(

                'bgcolor'               =>      '#7cc567',
                'fontcolor'             =>      '#CCCCCC',
                'test1_text'            =>      '',
                'test1_author'            =>      '',
                'test2_text'            =>      '',
                'test2_author'            =>      '',

            );
        }
        return $section;
    }
    public function getAboutSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section             =       $this->db->get('section_about')->result_array();

        if (count($section) > 0) {

            $section                     =       $section[0];

        } else {

            $section = array(

                'bgcolor'               =>      '#7cc567',
                'fontcolor'             =>      '#CCCCCC',
                'titletext'             =>      '',
                'description'           =>      '',
                'image'                 =>      '',

            );
        }
        return $section;
    }
    public function getCtaSection($page_key) {

        $this->db->where('page_key', $page_key);
        $section             =       $this->db->get('section_cta')->result_array();

        if (count($section) > 0) {

            $section                     =       $section[0];

        } else {

            $section = array(

                'bgcolor'               =>      '#7cc567',
                'fontcolor'             =>      '#CCCCCC',
                'headertext1'           =>      '',
                'headertext2'           =>      '',

            );
        }
        return $section;
    }


    public function header_defaults($page_key) {

        $this->db->where('page.key', $page_key);
        $this->db->join('site', 'site.key = page.site_key', 'left');
        $this->db->select('site.main_url as main_url');
        $site = $this->db->get('page')->result_array();

        $url = $site[0]['main_url'];


        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#FFFFFF',
            'fontcolor'             =>              '#333333',
            'logo'                  =>              'https://logo.clearbit.com/' . $url . '?size=500',
            'phonecolor'            =>              '#333333',

        );

        $this->db->insert('section_header', $data);


    }
    public function preheader_defaults($page_key) {

        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#5599ff',
            'fontcolor'             =>              '#FFFFFF',
            'headertext1'           =>              'YOUR MAIN HEADLINE TO GRAB THE VISITOR\'S ATTENTION',
            'headertext2'           =>              'Short, sharp and to the point to ensure they continue reading the page',

        );

        $this->db->insert('section_preheader', $data);


    }
    public function banner_defaults($page_key) {

        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#487be7',
            'fontcolor'             =>              '#FFFFFF',
            'bannertitle'           =>              'Test header in here',
            'bannertext'            =>              'A brief summary of why someone should come to you and not one of your competitors',
            'bp1_title'             =>              'Dedicated Account Manager',
            'bp1_text'              =>              'All our account managers are based in our own offices',
            'bp2_title'             =>              'Trading for over 10 years',
            'bp2_text'              =>              'We\'ve got a long history in delivering the best results for our clients',
            'bp3_title'             =>              'We go the extra mile',
            'bp3_text'              =>              'We do whatever is required to ensure the highest standards',
            'boxbehindtext'         =>              'No',
            'form_header'           =>              '#487be7',
            'form_border'           =>              '#487be7',
            'form_headline'         =>              'Get in touch today',
            'form_headline2'        =>              'For a free consultation',
            'form_pretext'          =>              'Let our team show you how we\'ll make a difference and help your business grow',
            'form_button'           =>              'Get Started',
            'form_textbox_1'        =>              'Name',
            'form_textbox_2'        =>              'Email',
            'form_textbox_3'        =>              'Company',
            'form_textbox_4'        =>              'Phone',

        );

        $this->db->insert('section_banner', $data);


    }
    public function subbanner_defaults($page_key) {

        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#6599F8',
            'fontcolor'             =>              '#FFFFFF',
            'bannertext'            =>              'Dedicated to helping our clients grow since 2008 - Let us help you!',


        );

        $this->db->insert('section_subbanner', $data);


    }
    public function testimonial_defaults($page_key) {

        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#FFFFFF',
            'fontcolor'             =>              '#2D2D2D',
            'test1_text'            =>              'Increased sales leads and reduced costs across one of the most competitive industries.',
            'test1_author'          =>              'John Stevens, The Big Company, Devon',
            'test2_text'            =>              'A full and professional service from start to finish at an affordable price and fast ROI',
            'test2_author'          =>              'Sheila Jones, The Widget Co. Northampton',



        );

        $this->db->insert('section_testimonial', $data);


    }
    public function about_defaults($page_key) {

        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#f2f1f4',
            'fontcolor'             =>              '#282828',
            'titletext'             =>              'Since 2008, Your Company has been helping businesses grow and develop',
            'description'           =>              'We are proud to say we have been a key part in helping small and medium sized businesses grow and develop a greater market share.<br/><br/>Headquartered in the digital heartland of England, we are surrounded by some of the best talent our industry has to offer.<br/><br/>Our team of dedicated experts combined with our leading technology is ready to meet even the toughest challenge!',
            'image'                 =>              '/uploads/pages/jsQiyR3SDB/social-media-services-icon.svg',



        );

        $this->db->insert('section_about', $data);


    }
    public function cta_defaults($page_key) {

        $data = array(

            'key'                   =>              $this->key_model->generate(),
            'page_key'              =>              $page_key,
            'bgcolor'               =>              '#7cc567',
            'fontcolor'             =>              '#FFFFFF',
            'headertext1'           =>              'Get in touch today for a hassle free chat about how we can help',



        );

        $this->db->insert('section_cta', $data);


    }


}
