<?php
class Login extends CI_Controller {


    public function index() {


        if ($this->db->table_exists('users') )
        {

            // table exists
            $data = array();
            $this->parser->parse('/account/login.php', $data);

        }
        else
        {
            // table does not exist

            $this->load->dbforge();


            $fields = array(
                'key' => array('type' => 'VARCHAR', 'constraint' => '10', 'unique' => TRUE,),
                'email' => array('type' => 'VARCHAR', 'constraint' => '200',),
                'password' => array('type' =>'LONGTEXT',),
                'created' => array('type' => 'DATETIME',),
                'fname' => array('type' => 'VARCHAR', 'constraint' => '100',),
                'lname' => array('type' => 'VARCHAR', 'constraint' => '100',),
                'company' => array('type' => 'VARCHAR', 'constraint' => '200',),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('key', TRUE);
            $this->dbforge->create_table('users');

            $this->load->library('encrypt');
            $pwd = $this->encrypt->encode('B1ggMed1a');

            $data = array(
                'key' => 'root_key',
                'email' => 'root@root.com',
                'password' => $pwd,
                'created' => date('Y-m-d H:i:s'),
                'fname' => 'root',
                'lname' => 'Account',
            );

            $this->db->insert('users', $data);

            redirect('/login');


        }

    }

    public function dologin() {

        $this->load->library('encrypt');

        $email = $this->input->post('email');
        $pwd = $this->input->post('password');

        $this->db->where('email', $email);
        $users = $this->db->get('users')->result();
        if (count($users) > 0) {
            $e_pwd = $users[0]->password;
            $d_pwd = $this->encrypt->decode($e_pwd);

            if ($d_pwd == $pwd) {

                // SUCCESS

                $data = array();
                $data['loggedin'] = true;
                $data['email'] = $this->input->post('email');
                $data['key'] = $users[0]->key;
                $data['fname'] = $users[0]->fname;
                $data['lname'] = $users[0]->lname;
                $data['user_type'] = $users[0]->user_type;

                $this->session->set_userdata($data);


                if ($users[0]->user_type == "Admin") {

                    $clients = $this->db->get('client')->result_array();

                    $this->db->where('client_key', $clients[0]['key']);
                    $sites = $this->db->get('site')->result_array();

                    $this->db->where('status', 'Active');
                    $this->db->where('site_key', $sites[0]['key']);
                    $pages = $this->db->get('page')->result_array();

                    $this->session->set_userdata('client_key', $clients[0]['key']);
                    $this->session->set_userdata('client_name', $clients[0]['name']);
                    $this->session->set_userdata('client_hcolor', '#6F91E0');
                    $this->session->set_userdata('client_scolor', '#FFFFFF');
                    $this->session->set_userdata('client_logo', 'https://app.addio.co.uk/uploads/addio.png');
                    $this->session->set_userdata('site_key', $sites[0]['key']);
                    $this->session->set_userdata('site_name', $sites[0]['name']);
                    $this->session->set_userdata('page_key', $pages[0]['key']);
                    $this->session->set_userdata('page_name', $pages[0]['name']);

                    redirect('/analytics');

                }

                if ($users[0]->user_type == "Client") {

                    $this->db->where('key', $users[0]->client_key);
                    $clients = $this->db->get('client')->result_array();
                    $client = $clients[0];

                    $this->db->where('client_key', $users[0]->client_key);
                    $sites = $this->db->get('site')->result_array();

                    $this->db->where('status', 'Active');
                    $this->db->where('site_key', $sites[0]['key']);
                    $pages = $this->db->get('page')->result_array();

                    $this->session->set_userdata('client_key', $client['key']);
                    $this->session->set_userdata('client_name', $client['name']);
                    $this->session->set_userdata('client_hcolor', $client['header_color']);
                    $this->session->set_userdata('client_scolor', $client['side_color']);
                    $this->session->set_userdata('client_logo', $client['logo']);
                    $this->session->set_userdata('site_key', $sites[0]['key']);
                    $this->session->set_userdata('site_name', $sites[0]['name']);
                    $this->session->set_userdata('page_key', $pages[0]['key']);
                    $this->session->set_userdata('page_name', $pages[0]['name']);

                    redirect('/analytics');

                }

                if ($users[0]->user_type == "User") {

                    $this->db->where('key', $users[0]->client_key);
                    $clients = $this->db->get('client')->result_array();
                    $client = $clients[0];

                    $this->db->where('key', $users[0]->site_key);
                    $sites = $this->db->get('site')->result_array();
                    $site = $sites[0];

                    $this->db->where('status', 'Active');
                    $this->db->where('site_key', $sites[0]['key']);
                    $pages = $this->db->get('page')->result_array();

                    $this->session->set_userdata('client_key', $client['key']);
                    $this->session->set_userdata('client_name', $client['name']);
                    $this->session->set_userdata('client_hcolor', $client['header_color']);
                    $this->session->set_userdata('client_scolor', $client['side_color']);
                    $this->session->set_userdata('client_logo', $client['logo']);
                    $this->session->set_userdata('site_key', $site['key']);
                    $this->session->set_userdata('site_name', $site['name']);
                    $this->session->set_userdata('page_key', $pages[0]['key']);
                    $this->session->set_userdata('page_name', $pages[0]['name']);

                    redirect('/analytics');

                }




            } else {

                // FAILED

                redirect('/login?err');
            }
        } else {
            redirect('/login?err');
        }




    }

}
