<?php
class Calls extends Auth_Controller {


    public function index() {

        redirect('/calls/view/7');

    }

    public function view($start = null, $end = null) {

        $data = array();

        $data['page_name']      =       "Calls";
        $data['menu_item']      =       "menu_calls";
        $data['pageview']       =       "Phone leads";

        if (!$start) {

            $start              =           date('Y-m-d', strtotime('- 1 month'));
        } else {
            $start = date('Y-m-d 00:00:00', strtotime($start));
        }

        if (!$end) {

            $end                =           date('Y-m-d');
        } else {
            $end = date('Y-m-d 23:59:59', strtotime($end));
        }

        $data['start'] = $start;
        $data['end'] = $end;

        if ($this->session->userdata('page_key') !== 'all') {
            $this->db->where('key', $this->session->userdata('page_key'));

            $data['page_name']      =       "Phone Leads for " . $this->session->userdata('page_name');
        } else {
            $data['page_name']      =       "Phone Leads for all pages";
        }

        $this->db->where('site_key', $this->session->userdata('site_key'));

        $pages                  =       $this->db->get('page')->result_array();

        $return_data = array();

        $this->db->where('call.created >=', date('Y-m-d 00:00:00', strtotime($start)));
        $this->db->where('call.created <=', date('Y-m-d 23:59:59', strtotime($end)));
        $this->db->where('site_key', $pages[0]['site_key']);
        $this->db->join('page', 'page.key = call.page_key');
        $this->db->join('site', 'site.key = page.site_key');
        $this->db->order_by('call.created', 'desc');
        $this->db->select('page.name as pagename, call.call_start as start, call.call_end as end, call.caller_number as number');
        $formfills = $this->db->get('call')->result_array();
        $return_data[] = $formfills;

        $data['calls'] = $return_data;

        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/calls/overview.php', $data);


    }

}
