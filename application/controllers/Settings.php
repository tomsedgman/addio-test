<?php
class Settings extends Auth_Controller {


    public function index() {

        $data = array();

        $data['page_name']      =       "Site settings";
        $data['pageview']      =       "Settings";
        $data['menu_item']      =       "menu_settings";

        $clients = $this->db->get('client')->result_array();
        $data['clients'] = $clients;

        $this->db->where('key', $this->session->userdata('site_key'));
        $site = $this->db->get('site')->result_array();
        $data['site'] = $site[0];

        $this->db->where('key', $site[0]['user_key']);
        $owner = $this->db->get('users')->result_array();
        $data['owner'] = $owner[0];

        $this->db->where('users.site_key', $site[0]['key']);
        $this->db->where('user_type', 'Customer');
        $this->db->select('users.fname, users.lname, users.email');
        $users = $this->db->get('users')->result_array();
        $data['users']          =       $users;

        $this->db->where('site_key', $site[0]['key']);
        $invites                =       $this->db->get('invites')->result_array();
        $data['invites']        =       $invites;


        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/settings/overview.php', $data);


    }

    public function updateclient() {

        $client_key = $this->input->post('client_key');

        $data = array(

            'client_key'        =>      $client_key,

        );

        $this->db->where('key', $this->session->userdata('site_key'));
        $this->db->update('site', $data);

        redirect('/settings');

    }

    public function removeaccess($key) {


        $this->db->where('key', $key);
        $access = $this->db->get('user_access')->result_array();

        if ($access[0]['user_key'] == $this->session->userdata('key')) {

            $this->db->where('key', $key);
            $this->db->delete('user_access');

            redirect('/site');

        } else {

            $this->db->where('key', $key);
            $this->db->delete('user_access');

            redirect('/settings');

        }


    }

    public function removeinvitation($key) {


        $this->db->where('key', $key);
        $this->db->delete('invites');

        redirect('/settings');


    }

    public function adduseraccess() {


        $this->db->where                        ('key', $this->session->userdata('site_key'));
        $sites                  =               $this->db->get('site')->result_array();
        $site                   =               $sites[0];

        $email                  =               $this->input->post('email');
        $access                 =               $this->input->post('access');

        // 1. Check if the user has an account, if so add the access to their user_key

        $this->db->where('email', $email);
        $users                  =               $this->db->get('users')->result_array();

        if (count($users) > 0) {

            if ($site['user_key'] == $users[0]['key']) {

                redirect('/settings/add_access/?owner');

            } else {

                redirect('/settings/add_access/?hasaccount');

            }

        } else {

            $this->load->library('email');
            $this->email->from('noreply@addio.co.uk');
            $this->email->to($email);

            $this->email->subject('You\'ve been invited to an Addio site');
            $this->email->message($this->invited_emailtemplate($site['name'], 'https://app.addio.co.uk' . $this->session->userdata('client_logo')));

            $this->email->send();

            $data = array(
                'created'           =>          date('Y-m-d H:i:s'),
                'key'               =>          $this->key_model->generate(),
                'email'             =>          $email,
                'access'            =>          $access,
                'invite_by'         =>          $this->session->userdata('key'),
                'site_key'          =>          $site['key'],
            );

            $this->db->insert('invites', $data);

            redirect('/settings');

        }


    }
    public function add_access() {

        $data = array();

        $data['pageview']      =       "Settings";
        $data['menu_item']      =       "menu_settings";

        $this->db->where('key', $this->session->userdata('site_key'));
        $site = $this->db->get('site')->result_array();
        $data['site'] = $site[0];

        $title = array(

            array(
                'title'         =>      'Settings',
                'url'           =>      '/settings'
            ),
            array(
                'title'         =>      'Add user access',
                'url'           =>      null,
            ),

        );

        $data['header_title']   =       $title;


        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/settings/add_access.php', $data);

    }
    public function update() {

        $url = $this->input->post('url');
        $main_url = $this->input->post('main_url');
        $name = $this->input->post('name');

        $data = array(

            'url'                   =>          $url . '.addio.co.uk',
            'main_url'              =>          $main_url,
            'name'                  =>          $name,
            'privacy_policy'        =>          $this->input->post('privacy_policy'),
            'cookie_policy'         =>          $this->input->post('cookie_policy'),
            'address'               =>          $this->input->post('address'),

        );

        $this->db->where('key', $this->session->userdata('site_key'));
        $this->db->update('site', $data);

        $this->session->set_userdata('site_name', $name);

        redirect('/settings?details');
    }



    function invited_emailtemplate($sitename, $logo) {

        $html = '<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Your new account</title>
    
    ';

        $html .= $this->email_style();

        $html .= '
    
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">You\'ve been invited to an Addio site</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
            ';

        $html .= $this->invited_email_content($sitename, $logo);

        $html .= '

              
            </table>

';

        $html .= $this->email_footer();

        $html .= '
            

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>';

        return $html;

    }
    function invited_email_content($sitename, $logo) {

        return '<!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="logo" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                    <img width="100px" src="'. $logo .'"/>
                </td>
              </tr>
              <tr>
             
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Hi there,
                        </p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                            Congratulations, you have been invited to the site ' . $sitename . ' but as you don\'t have an account you\'ll need to sign up first.
                        </p>
                        <p>
                            Sign up using this email address and the site will automatically be added to your account.
                        </p>
                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
                          <tbody>
                            <tr>
                              <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> 
                                      <a href="' . site_url() . 'signup/go/' . $this->session->userdata('client_key') . '" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">
                                      Signup now
                                      </a> 
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                       If you have any problems please contact our support team who will be happy to help.</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Good luck!</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->';
    }

    function added_emailtemplate($sitename) {

        $html = '<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Your new account</title>
    
    ';

        $html .= $this->email_style();

        $html .= '
    
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">You\'ve been added to a site</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
            ';

        $html .= $this->added_email_content($sitename);

        $html .= '

              
            </table>

';

        $html .= $this->email_footer();

        $html .= '
            

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>';

        return $html;

    }
    function added_email_content($sitename) {

        return '<!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="logo" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                    <img width="100px" src="https://app.addio.co.uk/uploads/addio.png"/>
                </td>
              </tr>
              <tr>
             
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Hi there,
                        </p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Congratulations, you have been added to the site ' . $sitename . ', you can view the site the next time you log in.
                        </p>
                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
                          <tbody>
                            <tr>
                              <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> 
                                      <a href="' . site_url() . 'login" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">
                                      Login now
                                      </a> 
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                       If you have any problems please contact our support team who will be happy to help.</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        Good luck!</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->';
    }




    function email_footer() {

        return '<!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"></span>
                   
                  </td>
                </tr>
                
              </table>
            </div>
            <!-- END FOOTER -->';

    }
    function email_style() {

        return '<style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .logo {
        padding: 10px !important;
        background: transparent;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>';
    }

}
