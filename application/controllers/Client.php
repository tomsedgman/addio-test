<?php

class Client extends Auth_Controller {


    function index() {

        $this->session->set_userdata('site_key', null);
        $this->session->set_userdata('site_name', '');


        $data = array();

        $data['page_name']      =       "Clients";
        $data['menu_item']      =       "menu_client";
        $data['hide_sidebar']   =       true;


        $this->db->select('client.key as key, client.name, client.url');
        $clients                  =       $this->db->get('client')->result_array();
        $data['clients']          =       $clients;


        if (count($clients) == 0) {
            redirect('/client/add');
        } else {
            //$this->parser->parse('header.php', $data);
            $this->parser->parse('/client/list.php', $data);

        }

    }
    function settings() {


        $data = array();

        $data['pageview'] = "Client";

        $this->db->where('key', $this->session->userdata('client_key'));
        $clients = $this->db->get('client')->result_array();
        $data['client'] = $clients[0];

        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/client/settings.php', $data);

    }

    function select($key, $redirect = null) {

        $this->db->where('key', $key);
        $clients = $this->db->get('client')->result_array();

        $this->db->where('client_key', $key);
        $sites = $this->db->get('site')->result_array();

        $this->db->where('status', 'Active');
        $this->db->where('site_key', $sites[0]['key']);
        $pages = $this->db->get('page')->result_array();

        $this->session->set_userdata('client_key', $key);
        $this->session->set_userdata('client_name', $clients[0]['name']);
        $this->session->set_userdata('client_logo', $clients[0]['logo']);
        $this->session->set_userdata('client_hcolor', $clients[0]['header_color']);
        $this->session->set_userdata('client_scolor', $clients[0]['side_color']);
        $this->session->set_userdata('site_key', $sites[0]['key']);
        $this->session->set_userdata('site_name', $sites[0]['name']);
        $this->session->set_userdata('page_key', $pages[0]['key']);
        $this->session->set_userdata('page_name', $pages[0]['name']);


        if ($redirect) {
            redirect (str_replace('_', '/', $redirect));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }


    }
    function add() {

        $data = array();

        $title = array(

            array(
                'title'         =>      'Clients',
                'url'           =>      '/client'
            ),
            array(
                'title'         =>      'Add client',
                'url'           =>      null,
            ),

        );

        $data['pageview'] = "Client";

        $data['header_title']   =       $title;

        $this->parser->parse('header_new.php', $data);
        $this->parser->parse('/client/add.php', $data);
    }
    function addsite() {

        $data = array();
        $this->parser->parse('/site/addsite.php', $data);
    }
    function addnew() {

        $client_key                    =               $this->key_model->generate();

        $data = array(
            'key'               =>              $client_key,
            'name'              =>              $this->input->post('name'),
            'url'               =>              $this->input->post('url'),
            'header_color'      =>              $this->input->post('header_color'),
            'side_color'        =>              $this->input->post('side_color'),
            'created'           =>              date('Y-m-d H:i:s'),
            'status'            =>              'Active',
        );


        if ($this->input->post('logo_file') !== '') {

            if (!file_exists('./uploads/clients/' . $client_key)) {
                mkdir('./uploads/clients/' . $client_key);
            }

            $config['upload_path']          = './uploads/clients/' . $client_key;
            $config['allowed_types']        = 'gif|jpg|png|jpeg|svg';
            $config['max_size']             = 10000;
            $config['max_width']            = 3024;
            $config['max_height']           = 1768;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('logo_file'))
            {
                //$error = array('error' => $this->upload->display_errors());
                //print_r($error);
            }
            else
            {
                $data2 = array('upload_data' => $this->upload->data());
                $data['logo'] = '/uploads/clients/' . $client_key . '/' . $data2['upload_data']['file_name'];

                $data['logo']            =              '/uploads/clients/' . $client_key . '/' . $data2['upload_data']['file_name'];

            }
        }


        $this->db->insert('client', $data);

        $this->session->set_userdata('client_key', $client_key);
        $this->session->set_userdata('client_name', $this->input->post('name'));
        $this->session->set_userdata('client_access', 'Admin');

        redirect('/client');

    }

    function update() {

        $client_key                    =               $this->session->userdata('client_key');

        $data = array(

            'name'              =>              $this->input->post('name'),
            'url'               =>              $this->input->post('url'),
            'header_color'      =>              $this->input->post('header_color'),
            'side_color'        =>              $this->input->post('side_color'),
            'created'           =>              date('Y-m-d H:i:s'),
            'status'            =>              'Active',
        );


        if ($this->input->post('logo_file') !== '') {

            if (!file_exists('./uploads/clients/' . $client_key)) {
                mkdir('./uploads/clients/' . $client_key);
            }

            $config['upload_path']          = './uploads/clients/' . $client_key;
            $config['allowed_types']        = 'gif|jpg|png|jpeg|svg';
            $config['max_size']             = 10000;
            $config['max_width']            = 3024;
            $config['max_height']           = 1768;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('logo_file'))
            {
                //$error = array('error' => $this->upload->display_errors());
                //print_r($error);
            }
            else
            {
                $data2 = array('upload_data' => $this->upload->data());
                $data['logo'] = '/uploads/clients/' . $client_key . '/' . $data2['upload_data']['file_name'];

                $data['logo']            =              '/uploads/clients/' . $client_key . '/' . $data2['upload_data']['file_name'];

            }
        }


        $this->db->where('key', $this->session->userdata('client_key'));
        $this->db->update('client', $data);

        $this->session->set_userdata('client_name', $this->input->post('name'));

        redirect('/client/settings?saved');

    }
    function delete() {

        $site_key = $this->session->userdata('site_key');

        // 1. Delete advertising spend

        $this->db->where('site_key', $site_key);
        $this->db->delete('advertising_spend');

        $this->db->where('site_key', $site_key);
        $this->db->delete('advertising_spend_detail');

        $this->db->where('site_key', $site_key);
        $this->db->delete('adwords');

        // 2. Remove all the invites

        $this->db->where('site_key', $site_key);
        $this->db->delete('invites');

        // 3. Remove all the user access

        $this->db->where('site_key', $site_key);
        $this->db->delete('user_access');

        // 4. Get all the pages

        $this->db->where('site_key', $site_key);
        $pages = $this->db->get('page')->result_array();

        foreach ($pages as $p) {

            $page_key = $p['key'];

            // 5. Delete the calls
            $this->db->where('page_key', $page_key);
            $this->db->delete('call');

            // 6. Delete the email clicks
            $this->db->where('page_key', $page_key);
            $this->db->delete('click_email');

            // 7. Delete the call clicks
            $this->db->where('page_key', $page_key);
            $this->db->delete('click_phone');

            // 8. Delete the form fills
            $this->db->where('page_key', $page_key);
            $this->db->delete('formfill');

            // 9. Delete the page views
            $this->db->where('page_key', $page_key);
            $this->db->delete('page_view');

            // 10. Delete the sections
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_about');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_banner');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_cta');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_header');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_preheader');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_subbanner');
            $this->db->where('page_key', $page_key);
            $this->db->delete('section_testimonial');

            // 11. Delete the page
            $this->db->where('key', $page_key);
            $this->db->delete('page');

        }

        // 12. Delete this site
        $this->db->where('key', $site_key);
        $this->db->delete('site');


        redirect('/site');


    }

}