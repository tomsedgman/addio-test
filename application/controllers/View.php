<?php

class View extends CI_Controller {

    public function index() {

        $url        =       $_SERVER['SERVER_NAME'];

        $this->db->where('customer_url', $url);
        $user = $this->db->get('users')->result_array();

        if (count($user) > 0) {


            $user = $user[0];


        } else {

            echo "No website found";
        }

    }

    public function page($pagename) {

        $url        =       $_SERVER['SERVER_NAME'];

        $this->db->where('customer_url', $url);
        $user = $this->db->get('users')->result_array();

        if (count($user) > 0) {

            $user = $user[0];

            $this->db->where('user_key', $user['key']);
            $this->db->where('url', $pagename);
            $page   =       $this->db->get('page')->result_array();

            if (count($page) > 0) {

                $this->db->where('page_key', $page[0]['key']);
                $header = $this->db->get('section_header')->result_array();

                $this->db->where('page_key', $page[0]['key']);
                $preheader = $this->db->get('section_preheader')->result_array();

                $this->db->where('page_key', $page[0]['key']);
                $banner = $this->db->get('section_banner')->result_array();

                $this->db->where('page_key', $page[0]['key']);
                $subbanner = $this->db->get('section_subbanner')->result_array();

                if (count($header) > 0) {

                    $data = array();

                    $data['section_header']         =       $header[0];
                    $data['section_banner']         =       $banner[0];
                    $data['section_preheader']      =       $preheader[0];
                    $data['section_subbanner']      =       $subbanner[0];

                    $this->parser->parse('/publicfolder/index.php', $data);


                } else {

                    echo "No header found";
                }


            } else {

                echo "No page found";
            }


        } else {

            echo "No website found";
        }

    }


}