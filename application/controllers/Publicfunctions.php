<?php

class Publicfunctions extends CI_Controller {


    public function adspend() {

        $ads                    =               $this->db->get('advertising_spend')->result_array();

        foreach ($ads as $a) {


            $daily_budget       =               round($a['budget'] / 30, 2);
            $balance            =               $a['balance'];
            $new_balance        =               $balance - $daily_budget;

            if ($new_balance * 2 < $balance) {
                $new_balance    =               0;
                $daily_budget   =               $balance;
            }

            $data = array(

                'balance'       =>              $new_balance

            );

            if ($new_balance == 0) {

                $data['enddate'] = date('Y-m-d');

            }

            $this->db->where('key', $a['key']);
            $this->db->update('advertising_spend', $data);


            $data = array(
                'key'           =>      $this->key_model->generate(),
                'as_key'        =>      $a['key'],
                'amount'        =>      $daily_budget,
                'date'          =>      date('Y-m-d'),
                'site_key'      =>      $a['site_key'],
                'page_key'      =>      $a['page_key'],
            );

            $this->db->insert('advertising_spend_detail', $data);


        }

    }

}