<?php

class Receivecall extends CI_Controller {


    function track($page_key) {

        $this->load->model('key_model');
        $data = array(
            'key'       =>      $this->key_model->generate(),
            'page_key'  =>      $page_key,
            'created'   =>      date('Y-m-d H:i:s'),
            'post_data' =>      json_encode($_POST),
            'caller_number'     =>      $_POST['From'],
            'call_sid'          =>      $_POST['CallSid'],
            'call_start'        =>      date('Y-m-d H:i:s'),
        );

        $this->db->insert('call', $data);

        $this->db->where('key', $page_key);
        $page = $this->db->get('page')->result_array();
        $page = $page[0];

        $t = new Twillio();
        $t->call('+44' . ltrim(str_replace(' ', '', $page['phone']), '0'));

    }

    function callstatus() {

        if ($_POST['CallStatus'] == "completed") {

            $CallSid = $_POST['CallSid'];

            $data = array(

                'status_data'       =>      json_encode($_POST),
                'call_end'          =>      date('Y-m-d H:i:s'),
            );

            $this->db->where('call_sid', $CallSid);
            $this->db->update('call', $data);

        }


    }


}