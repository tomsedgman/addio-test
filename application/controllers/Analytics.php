<?php
class Analytics extends Auth_Controller {


    public function index() {

        if ($this->session->userdata('page_key') == 'all') {

            redirect('/analytics/view');

        } else {
            redirect('/analytics/viewpage');
        }

    }

    public function view($start = null, $end = null) {

        $data = array();

        $data['page_name']      =       "Analytics";
        $data['pageview']      =       "Analytics";
        $data['menu_item']      =       "menu_analytics";


        if (!$start) {

            $start              =           date('Y-m-d', strtotime('- 1 month'));
        } else {
            $start = date('Y-m-d 00:00:00', strtotime($start));
        }

        if (!$end) {

            $end                =           date('Y-m-d');
        } else {
            $end = date('Y-m-d 23:59:59', strtotime($end));
        }

        $data['start'] = $start;
        $data['end'] = $end;


        $this->db->where('status <>', 'Deleted');
        $this->db->where('site_key', $this->session->userdata('site_key'));

        $pages                  =       $this->db->get('page')->result_array();

        if ($this->session->userdata('page_key') !== 'all') {

            redirect('/analytics/viewpage');

        } else if (count($pages) > 1) {

            $pages_array            =       array();

            foreach ($pages as $p) {

                $page_key           =       $p['key'];

                $this->db->where('created >= ', $start);
                $this->db->where('created <= ', $end);
                $this->db->where('page_key', $page_key);
                $this->db->select('COUNT(ipaddress) as total');
                $total = $this->db->get('page_view')->result_array();
                $total = $total[0]['total'];

                $p['total'] = $total;

                $this->db->where('created >= ', $start);
                $this->db->where('created <= ', $end);
                $this->db->where('page_key', $page_key);
                $this->db->select('COUNT(name) as total');
                $forms = $this->db->get('formfill')->result_array();
                $forms = $forms[0]['total'];

                $p['forms'] = $forms;


                $this->db->where('created >= ', $start);
                $this->db->where('created <= ', $end);
                $this->db->where('page_key', $page_key);
                $this->db->select('COUNT(page_key) as total');
                $calls = $this->db->get('call')->result_array();
                $calls = $calls[0]['total'];

                $p['calls'] = $calls;

                $this->db->where('created >= ', $start);
                $this->db->where('created <= ', $end);
                $this->db->where('page_key', $page_key);
                $this->db->select('COUNT(page_key) as total');
                $ccalls = $this->db->get('click_phone')->result_array();
                $ccalls = $ccalls[0]['total'];

                $p['ccalls'] = $ccalls;

                $this->db->where('created >= ', $start);
                $this->db->where('created <= ', $end);
                $this->db->where('page_key', $page_key);
                $this->db->select('COUNT(page_key) as total');
                $cemail = $this->db->get('click_email')->result_array();
                $cemail = $cemail[0]['total'];

                $p['cemails'] = $cemail;

                $this->db->where('date >=', $start);
                $this->db->where('date <= ', $end);
                $this->db->where('page_key', $page_key);
                $this->db->select('SUM(amount) as amount');
                $spend = $this->db->get('advertising_spend_detail')->result_array();

                if (count($spend) > 0){

                    $amt = $spend[0]['amount'];
                } else {
                    $amt = 0;
                }


                $p['spend'] = $amt;

                array_push($pages_array, $p);

            }

            $graph = array(array('Date', 'Total'));

            $now = strtotime($end);; // or your date as well
            $your_date = strtotime($start);
            $datediff = $now - $your_date;

            $days = round($datediff / (60 * 60 * 24));


            for ($i = $days -1; $i > -1; $i--) {

                $this->db->where('page_view.created >=', date('Y-m-d 00:00:00', strtotime(substr($end, 0, 10) . ' -' . $i . ' days')));
                $this->db->where('page_view.created <=', date('Y-m-d 23:59:59', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('site_key', $pages[0]['site_key']);
                $this->db->join('page', 'page.key = page_view.page_key');
                $this->db->join('site', 'site.key = page.site_key');
                $this->db->select('COUNT(ipaddress) as total, MIN(page_view.created) as date');
                $total = $this->db->get('page_view')->result_array();
                $graph[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);
            }

            $data['graph'] = json_encode($graph);


            array_multisort(array_map(function($element) {
                return $element['total'];
            }, $pages_array), SORT_DESC, $pages_array);

            $data['pages']          =       $pages_array;

            $this->parser->parse('header_new.php', $data);
            $this->parser->parse('/analytics/overview.php', $data);

        } else {

            redirect('/page/add');

        }




    }

    public function viewpage($start = null, $end = null) {


        if ($this->session->userdata('page_key') == 'all') {

            redirect('/analytics/view');

        } else {

            $data = array();


            $data['menu_item']      =       "menu_analytics";

            if (!$start) {

                $start              =           date('Y-m-d', strtotime('- 1 month'));
            } else {
                $start = date('Y-m-d 00:00:00', strtotime($start));
            }

            if (!$end) {

                $end                =           date('Y-m-d');
            } else {
                $end = date('Y-m-d 23:59:59', strtotime($end));
            }

            $data['start'] = $start;
            $data['end'] = $end;


            $this->db->where('key', $this->session->userdata('page_key'));
            $pages                  =       $this->db->get('page')->result_array();
            $page = $pages[0];

            if ($page['trackable_phone'] > '0') {

                $data['tp'] = true;

            } else {

                $data['tp'] = false;
            }


            $title = array(

                array(
                    'title'         =>      'Analytics',
                    'url'           =>      '/analytics'
                ),
                array(
                    'title'         =>      $page['name'],
                    'url'           =>      null,
                ),

            );

            $data['pageview'] = "Analytics";

            $data['header_title']   =       $title;


            $pages_array            =       array();


            $p = array();

            $this->db->where('created >= ', $start);
            $this->db->where('created <= ', $end);
            $this->db->where('page_key', $this->session->userdata('page_key'));
            $this->db->select('COUNT(ipaddress) as total');
            $total = $this->db->get('page_view')->result_array();
            $total = $total[0]['total'];

            $data['total'] = $total;
            $p['total'] = $total;

            $this->db->where('created >= ', $start);
            $this->db->where('created <= ', $end);
            $this->db->where('page_key', $this->session->userdata('page_key'));
            $this->db->select('COUNT(name) as total');
            $forms = $this->db->get('formfill')->result_array();
            $forms = $forms[0]['total'];

            $data['forms'] = $forms;
            $p['forms'] = $forms;


            $this->db->where('created >= ', $start);
            $this->db->where('created <= ', $end);
            $this->db->where('page_key', $this->session->userdata('page_key'));
            $this->db->select('COUNT(page_key) as total');
            $calls = $this->db->get('call')->result_array();
            $calls = $calls[0]['total'];

            $data['calls'] = $calls;
            $p['calls'] = $calls;

            $this->db->where('created >= ', $start);
            $this->db->where('created <= ', $end);
            $this->db->where('page_key', $this->session->userdata('page_key'));
            $this->db->select('COUNT(page_key) as total');
            $ccalls = $this->db->get('click_phone')->result_array();
            $ccalls = $ccalls[0]['total'];

            $data['ccalls'] = $ccalls;
            $p['ccalls'] = $ccalls;

            $this->db->where('created >= ', $start);
            $this->db->where('created <= ', $end);
            $this->db->where('page_key', $this->session->userdata('page_key'));
            $this->db->select('COUNT(page_key) as total');
            $cemail = $this->db->get('click_email')->result_array();
            $cemail = $cemail[0]['total'];

            $data['cemails'] = $cemail;
            $p['cemails'] = $cemail;

            $this->db->where('date >=', $start);
            $this->db->where('date <= ', $end);
            $this->db->where('page_key', $this->session->userdata('page_key'));
            $this->db->select('SUM(amount) as amount');
            $spend = $this->db->get('advertising_spend_detail')->result_array();

            if (count($spend) > 0){

                $amt = $spend[0]['amount'];
            } else {
                $amt = 0;
            }

            $data['spend'] = $amt;
            $p['spend'] = $amt;

            array_push($pages_array, $p);


            $now = strtotime($end);; // or your date as well
            $your_date = strtotime($start);
            $datediff = $now - $your_date;

            $days = round($datediff / (60 * 60 * 24));


            // GRAPH

            $graph = array(array('Date', 'Total'));

            for ($i = $days -1; $i > -1; $i--) {

                $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('page_key', $this->session->userdata('page_key'));
                $this->db->select('COUNT(ipaddress) as total, MIN(created) as date');
                $total = $this->db->get('page_view')->result_array();
                $graph[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


            }

            $graph_form = array(array('Date', 'Total'));

            for ($i = $days; $i > -1; $i--) {

                $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('page_key', $this->session->userdata('page_key'));
                $this->db->select('COUNT(name) as total, MIN(created) as date');
                $total = $this->db->get('formfill')->result_array();
                $graph_form[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


            }

            $graph_cemail = array(array('Date', 'Total'));

            for ($i = $days; $i > -1; $i--) {

                $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('page_key', $this->session->userdata('page_key'));
                $this->db->select('COUNT(page_key) as total, MIN(created) as date');
                $total = $this->db->get('click_email')->result_array();
                $graph_cemail[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


            }

            $graph_call = array(array('Date', 'Total'));

            for ($i = $days; $i > -1; $i--) {

                $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('page_key', $this->session->userdata('page_key'));
                $this->db->select('COUNT(page_key) as total, MIN(created) as date');
                $total = $this->db->get('call')->result_array();
                $graph_call[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


            }

            $graph_ccall = array(array('Date', 'Total'));

            for ($i = $days; $i > -1; $i--) {

                $this->db->where('created >=', date('Y-m-d 00:00:00', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('created <=', date('Y-m-d 23:59:59', strtotime(substr($end, 0, 10) . '-' . $i . ' days')));
                $this->db->where('page_key', $this->session->userdata('page_key'));
                $this->db->select('COUNT(page_key) as total, MIN(created) as date');
                $total = $this->db->get('click_phone')->result_array();
                $graph_ccall[] = array(date('d M Y', strtotime('-' . $i . ' days')), (int)$total[0]['total']);


            }

            $data['graph'] = json_encode($graph);
            $data['graph_form'] = json_encode($graph_form);
            $data['graph_call'] = json_encode($graph_call);
            $data['graph_ccall'] = json_encode($graph_ccall);
            $data['graph_cemail'] = json_encode($graph_cemail);
            $data['page']          =       $page;
            $data['page_data']     =       $p;

            $this->parser->parse('header_new.php', $data);
            $this->parser->parse('/analytics/page.php', $data);

        }



    }




}
