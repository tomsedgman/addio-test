<?php
class Account extends Auth_Controller {


    public function index() {

        $data = array();

        $data['page_name']      =       "Account details";
        $data['menu_item']      =       "menu_account";

        $this->db->where('key', $this->session->userdata('key'));
        $user = $this->db->get('users')->result_array();
        $data['user'] = $user[0];


        $this->parser->parse('header.php', $data);
        $this->parser->parse('/account/account.php', $data);


    }

    public function update() {

        $email = $this->input->post('email');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $phone = $this->input->post('phone');

        $data = array(
            'email' => $email,
            'fname' => $fname,
            'lname' => $lname,
            'phone' =>  $phone,
        );

        $this->db->where('key', $this->session->userdata('key'));
        $this->db->update('users', $data);

        redirect('/account?details');
    }


    public function updatepassword() {

        $pwd = $this->input->post('password');

        $this->load->library('encrypt');
        $e_pwd = $this->encrypt->encode($pwd);

        $data = array(
            'password' => $e_pwd,
        );

        $this->db->where('key', $this->session->userdata('key'));
        $this->db->update('users', $data);

        redirect('/account?pwd');
    }

}
