<?php

class Key_model extends CI_Model {


    public function generate() {

        $x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        //0 - 61

        $y = "";

        for ($i = 0; $i < 10; $i++) {
            $y .= substr($x, rand(0, 61), 1);
        }

        return $y;

    }


}